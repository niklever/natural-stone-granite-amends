//
//  SSGalleryVC.h
//  NaturalStone
//
//  Created by Nik Lever on 28/04/2014.
//
//

#import <UIKit/UIKit.h>

@interface SSGalleryVC : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *gallery1_img;
@property (weak, nonatomic) IBOutlet UIImageView *gallery2_img;
@property (weak, nonatomic) IBOutlet UIButton *menu_btn;
@property (strong, nonatomic) SSMenuVC *menu_vc;

- (IBAction)menuPressed:(id)sender;
@end
