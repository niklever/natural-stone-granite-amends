//
//  SSProportionsVC.m
//  NaturalStone
//
//  Created by Nik Lever on 30/04/2014.
//
//

#import "SSProportionsVC.h"

@interface SSProportionsVC (){
    NSArray *mixImages;
    NSArray *dragImages;
    NSArray *labels;
    int colours[6];
    int colourCount;
    UIImageView *dragImage;
}

@end

@implementation SSProportionsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mixImages = [[NSArray alloc] initWithObjects:_mix1_img, _mix2_img, _mix3_img, _mix4_img, _mix5_img, _mix6_img, nil];
    dragImages = [[NSArray alloc] initWithObjects:_drag1_img, _drag2_img, _drag3_img, _drag4_img, _drag5_img, nil];
    labels = [[NSArray alloc] initWithObjects:_mix1_lbl, _mix2_lbl, _mix3_lbl, _mix4_lbl, _mix5_lbl, _mix6_lbl, nil];
    
    for(int i=0; i<[mixImages count]; i++){
        UIImageView *img = [mixImages objectAtIndex:i];
        img.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        img.backgroundColor = [UIColor colorWithPatternImage:img.image];
        img.image = nil;
        //img.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"pattern%d", (i+1)]]];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches]anyObject];
    
    for(int i=0; i<[dragImages count]; i++){
        if([touch view] == [dragImages objectAtIndex:i]){
            dragImage = (UIImageView*)[touch view];
            break;
        }
    }
}

-(NSArray *)getColours{
    return [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:colours[0]],
                                            [NSNumber numberWithInt:colours[1]],
                                            [NSNumber numberWithInt:colours[2]],
                                            [NSNumber numberWithInt:colours[3]],
                                            [NSNumber numberWithInt:colours[4]],
                                            [NSNumber numberWithInt:colours[5]], nil];
}

-(void)setColours:(NSArray *)_colours{
    int count=0;
    UIImageView *img;
    UILabel *label;
    
    for(int i=0; i<6; i++){
        colours[i] = [[_colours objectAtIndex:i] intValue];
        if (colours[i]>0) count++;
        img = [mixImages objectAtIndex:i];
        if (img!=nil) img.hidden = (colours[i]==0);
        label = [labels objectAtIndex:i];
        if (label!=nil){
            label.hidden = (colours[i]==0);
            if (colours[i]>0){
                label.text = [NSString stringWithFormat:@"%d%%", colours[i]];
            }
        }
    }
    
    for(int i=0; i<5; i++){
        UIImageView *drag = [dragImages objectAtIndex:i];
        drag.hidden = (i>=(count-1));
    }
    
    int x = 0;
    int idx = 0;
    colourCount = count;

    for(int i=0; i<6; i++){
        if (colours[i]>0){
            x += (colours[i]/100.0) * self.view.frame.size.width;
            if (idx<(colourCount-1)){
                UIImageView *drag = [dragImages objectAtIndex:idx];
                CGRect frame = drag.frame;
                frame.origin.x = x - frame.size.width/2;
                drag.frame = frame;
            }
            idx++;
        }
    }
    
    [self updateLayout];
}

-(void)updateLayout{
    UILabel *label;
    UIImageView *img;
    CGRect frame;
    int idx = 0;
    UIView *drag;
    CGFloat left=0, right;
    int sum = 0;
    double cols[6];
    
    for(int i=0; i<6; i++){
        label = [labels objectAtIndex:i];
        cols[i] = 0.0;
        if (!label.hidden){
            frame = label.frame;
            if (idx<(colourCount-1)){
                drag = [dragImages objectAtIndex:idx];
                right = drag.frame.origin.x + drag.frame.size.width/2;
            }else{
                right = 644;
            }
            frame = label.frame;
            frame.origin.x = (right-left)/2 + left - frame.size.width/2;
            cols[i] = ((right - left) * 100)/644.0;
            colours[i] = (int)cols[i];
            sum += colours[i];
            label.text = [NSString stringWithFormat:@"%d%%", colours[i]];
            label.frame = frame;
            idx++;
            img = [mixImages objectAtIndex:i];
            frame = img.frame;
            frame.origin.x = left;
            frame.size.width = right - left;
            img.frame = frame;
            left = right;
        }
    }
    
    while(sum!=100){
        double delta = 0.0;
        int idx = 0;
        if (sum<100){
            for(int i=0; i<6; i++){
                if (cols[i]==0.0) continue;
                double d = cols[i] - (int)cols[i];
                if (d>delta){
                    delta = d;
                    idx = i;
                }
            }
            colours[idx]++;
            sum++;
        }else{
            delta = 1.0;
            for(int i=0; i<6; i++){
                if (cols[i]==0.0) continue;
                double d = cols[i] - (int)cols[i];
                if (d<delta){
                    delta = d;
                    idx = i;
                }
            }
            colours[idx]--;
            sum--;
        }
        cols[idx] = colours[idx];
        label = [labels objectAtIndex:idx];
        label.text = [NSString stringWithFormat:@"%d%%", colours[idx]];
    }
}

- (void) touchesMoved:(NSSet *)touches withEvent: (UIEvent *)event
{
    
    UITouch *touch = [[event allTouches]anyObject];
    CGPoint pos = [touch locationInView:self.view];
    UIView *touch_view = [touch view];
    CGFloat min, max, width;
    UIView *left, *right;
    
    for(int i=0; i<(colourCount-1); i++){
        if(touch_view == (UIView*)[dragImages objectAtIndex:i]){
            CGRect frame = touch_view.frame;
            if (i==0){
                min = -5;
                if (colourCount==2){
                    max = 620;
                }else{
                    right = (UIView*)[dragImages objectAtIndex:i+1];
                    max = right.frame.origin.x - 8;
                }
            }else if (i==(colourCount-2)){
                left = (UIView*)[dragImages objectAtIndex:i-1];
                min = left.frame.origin.x + 8;
                max = 620;
            }else{
                left = (UIView*)[dragImages objectAtIndex:i-1];
                min = left.frame.origin.x + 8;
                right = (UIView*)[dragImages objectAtIndex:i+1];
                max = right.frame.origin.x - 8;
            }
            if (pos.x>=min && pos.x<=max){
                frame.origin.x = pos.x;
                touch_view.frame = frame;
            }
            break;
        }
    }

    [self updateLayout];
}

@end
