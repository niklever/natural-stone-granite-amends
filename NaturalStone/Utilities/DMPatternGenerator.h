//
//  DMPatternGenerator.h
//  NaturalStone
//
//  Created by John McKerrell on 08/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DMProject.h"

@interface DMPatternGenerator : NSObject

- (id)initWithProject:(DMProject*)project;
- (void) generatePattern;
- (CALayer*) patternPreviewWrapper;
- (NSArray*) quoteDetails;
- (NSMutableString*) patternHTML;

@end
