//
//  UITextField+Dolphin.m
//  NaturalStone
//
//  Created by John McKerrell on 15/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UITextField+Dolphin.h"

#import <QuartzCore/QuartzCore.h>

@implementation UITextField (Dolphin)

- (void) fixForDolphin {
    self.leftViewMode = UITextFieldViewModeAlways;
    self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5.0f, self.frame.size.height)];
    [self setValue:[UIColor colorWithRed:0.77 green:0.52 blue:0.23 alpha:0.5f] 
                     forKeyPath:@"_placeholderLabel.textColor"];
    self.textColor = [UIColor colorWithRed:0.77 green:0.52 blue:0.23 alpha:1.00];
    self.font = [UIFont fontWithName:kDMDefaultFormFont size:21.0f];
    self.layer.cornerRadius = 3.0f;
}

@end
