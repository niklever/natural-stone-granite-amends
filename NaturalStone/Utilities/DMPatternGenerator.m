//
//  DMPatternGenerator.m
//  NaturalStone
//
//  Created by John McKerrell on 08/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMPatternGenerator.h"

#import <QuartzCore/QuartzCore.h>

#define kDMProjectPercentagePriority @"area"
#define kDMPatternPreviewPadding 0.0f
//#define kDMPatternSizeModified 0.2f
#define kDMPatternSizeModified 0.2f
#define kDMUseCSS false

@interface DMPatternGenerator ()

@property (nonatomic,assign) NSInteger crossroadsTolerance; // how close the mortar joints of two adjacent courses are allowed to be
@property (nonatomic,assign) NSInteger courseBuildsAllowed; // how many attempts are allowed at rebuilding a course in order to avoid crossroads (crossroads are prevented on the first flag by offsetting the whole course; they are acceptable anywhere on a course that has only one flag size available; this rebuild rule concerns crossroads elsewhere in the pattern)
@property (nonatomic,assign) NSInteger maxCourseOffset; // maximum allowed rightward offset of each course, in order to avoid crossroads on the first flag
@property (nonatomic,assign) NSInteger patternWidth; // size of preview pattern
@property (nonatomic,assign) NSInteger patternHeight;
@property (nonatomic,assign) NSInteger calculatedWidth; // size that pattern ends up
@property (nonatomic,strong) NSMutableArray *gauges; // all of the paving course gauges (widths) to be used in the pattern, and relevant data about them, resulting from what the user has specified
@property (nonatomic,strong) NSMutableArray *courses; // info about each of the paving courses to be built: gauge, available flag lengths
@property (nonatomic,assign) CGFloat totalRuns; // total number of runs through the array of available course gauges (in gordsfreakypavingspeak, a 'run' is a group of consecutive courses with steadily increasing or decreasing gauge, in order to mix up the gauges evenly and minimise sudden contrast between the largest and smallest)
@property (nonatomic,assign) CGFloat totalCourses; // total number of paving courses used in the pattern
@property (nonatomic,strong) NSMutableArray *availableFlagLengthsBuffer; // for speed optimisation: when working within each course, use this buffer to remember the available flag sizes that are compatible with the current course gauge
@property (nonatomic,assign) CGFloat availableFlagLengthsTotalPercentage; // within each course, the flag size percentages defined in the project for just the compatible flag sizes, often add up to less than 100, so we need to treat them as fractions of this total rather than of 100
@property (nonatomic,strong) NSMutableArray *flagLengthsUsedBuffer; // remember all of the flag lengths used, so that in the next course we can avoid crossroads mortar joints
@property (nonatomic,strong) NSMutableArray *flagColorsUsedBuffer; // remember all of the flag colors used, so that in the next course we might (depending on the number of colors available) reduce vertical blocks of the same color
@property (nonatomic,strong) NSMutableArray *courseOffsetBuffer; // remember offset of previous course in order to avoid repeating it
@property (nonatomic,assign) NSInteger lookAheadLengthBuffer;
@property (nonatomic,assign) NSInteger lookAheadOffsetBuffer;
@property (nonatomic,strong) NSMutableArray *availableColoursBuffer; // for speed optimisation: randomisation of colours
@property (nonatomic,assign) NSInteger colorIndexBuffer; // within a course, remember color of previous flag...
@property (nonatomic,assign) NSInteger colorRepeats; // ...and how many times in a row that color has been used
@property (nonatomic,strong) NSMutableArray *colorSequence; // a pseudo-random array of colors, to ensure that they are evenly mixed
@property (nonatomic,assign) NSInteger colorSequenceIndex; // the current position in the color array as we scan along it

@property (nonatomic,strong) DMProject *project;
@property (nonatomic,strong) NSArray *sizeMixFlagSizes;

@property (nonatomic,assign) NSInteger lastGauge;
@property (nonatomic,assign) CGFloat maximumLeft;
@property (nonatomic,assign) CGFloat minimumWidth;
@property (nonatomic,strong) NSMutableArray *usedFlags;
@property (nonatomic,strong) NSMutableArray *courseFlags;
@property (nonatomic,strong) NSMutableString *patternHTML;
@property (nonatomic,strong) NSMutableString *courseHTML;
@property (nonatomic,assign) CGFloat courseWidth;

- (void)calculatePavingSizePercentages:(NSString*)percentagePriority;

@end

@implementation DMPatternGenerator

@synthesize crossroadsTolerance = __crossroadsTolerance;
@synthesize courseBuildsAllowed = __courseBuildsAllowed;
@synthesize maxCourseOffset = __maxCourseOffset;
@synthesize patternWidth = __patternWidth;
@synthesize patternHeight = __patternHeight;
@synthesize calculatedWidth = __calculatedWidth;
@synthesize gauges = __gauges;
@synthesize courses = __courses;
@synthesize totalRuns = __totalRuns;
@synthesize totalCourses = __totalCourses;
@synthesize availableFlagLengthsBuffer = __availableFlagLengthsBuffer;
@synthesize availableFlagLengthsTotalPercentage = __availableFlagLengthsTotalPercentage;
@synthesize flagLengthsUsedBuffer = __flagLengthsUsedBuffer;
@synthesize flagColorsUsedBuffer = __flagColorsUsedBuffer;
@synthesize courseOffsetBuffer = __courseOffsetBuffer;
@synthesize lookAheadLengthBuffer = __lookAheadLengthBuffer;
@synthesize lookAheadOffsetBuffer = __lookAheadOffsetBuffer;
@synthesize availableColoursBuffer = __availableColoursBuffer;
@synthesize colorIndexBuffer = __colorIndexBuffer;
@synthesize colorRepeats = __colorRepeats;
@synthesize colorSequence = __colorSequence;
@synthesize colorSequenceIndex = __colorSequenceIndex;

@synthesize project = __project;
@synthesize sizeMixFlagSizes = __sizeMixFlagSizes;

@synthesize lastGauge = __lastGauge;

@synthesize maximumLeft = __maximumLeft;
@synthesize minimumWidth = __minimumWidth;
@synthesize usedFlags = __usedFlags;
@synthesize courseFlags = __courseFlags;
@synthesize patternHTML = __patternHTML;
@synthesize courseHTML = __courseHTML;
@synthesize courseWidth = __courseWidth;

- (id)initWithProject:(DMProject*)project {
 if ((self = [super init])) {
  self.project = project;
  
  NSString *key = @"pavingSizes";
  self.crossroadsTolerance = 100;
  if ([kDMProjectFlagTypeSett isEqualToString:[self.project.sizeMix objectForKey:kDMProjectFlagTypeKey]]) {
   key = @"settSizes";
   self.crossroadsTolerance = 25;
  }
  
  //self.sizeMixFlagSizes = [self.project.sizeMix objectForKey:key];
  NSSortDescriptor *sortDescriptor; // sort flag sizes by ascending length (Gordon's debugging: not absolutely sure the sorting is necessary but this is how they were in the javascript version of the pattern generator)
  sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kDMProjectSizeMixLength ascending:YES];
  NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
  self.sizeMixFlagSizes = [[self.project.sizeMix objectForKey:key] sortedArrayUsingDescriptors:sortDescriptors];
  
  self.courseBuildsAllowed = 50; // how many attempts are allowed at rebuilding a course in order to avoid crossroads (crossroads are prevented on the first flag by offsetting the whole course; they are acceptable anywhere on a course that has only one flag size available; this rebuild rule concerns crossroads elsewhere in the pattern)
  self.maxCourseOffset = 500; // maximum allowed rightward offset of each course, in order to avoid crossroads on the first flag
  self.patternWidth = 10000; // size of preview pattern
  self.patternHeight = 7500;
  self.gauges = [NSMutableArray array]; // all of the paving course gauges (widths) to be used in the pattern, and relevant data about them, resulting from what the user has specified
  self.courses = [NSMutableArray array]; // info about each of the paving courses to be built: gauge, available flag lengths
  self.totalRuns = 0; // total number of runs through the array of available course gauges (in gordsfreakypavingspeak, a 'run' is a group of consecutive courses with steadily increasing or decreasing gauge, in order to mix up the gauges evenly and minimise sudden contrast between the largest and smallest)
  self.totalCourses = 0; // total number of paving courses used in the pattern
  self.availableFlagLengthsBuffer = nil; // for speed optimisation: when working within each course, use this buffer to remember the available flag sizes that are compatible with the current course gauge
  self.availableFlagLengthsTotalPercentage = 0; // within each course, the flag size percentages defined in the project for just the compatible flag sizes, often add up to less than 100, so we need to treat them as fractions of this total rather than of 100
  self.flagLengthsUsedBuffer = nil; // remember all of the flag lengths used, so that in the next course we can avoid crossroads mortar joints
  self.flagColorsUsedBuffer = nil; // remember all of the flag colors used, so that in the next course we might (depending on the number of colors available) reduce vertical blocks of the same color
  self.courseOffsetBuffer = nil; // remember offset of previous course in order to avoid repeating it
  self.lookAheadLengthBuffer = 0;
  self.lookAheadOffsetBuffer = -1;
  self.availableColoursBuffer = nil; // for speed optimisation: randomisation of colours
  self.colorIndexBuffer = -1; // within a course, remember color of previous flag...
  self.colorRepeats = 0; // ...and how many times in a row that color has been used
  self.colorSequence = nil; // a pseudo-random array of colors, to ensure that they are evenly mixed
  self.colorSequenceIndex = 0; // the current position in the color array as we scan along it
  self.maximumLeft = 0;
  self.minimumWidth = 0;
  self.usedFlags = [NSMutableArray array];
  self.courseFlags = [NSMutableArray array];
  self.patternHTML = [NSMutableString stringWithCapacity:0];
  self.courseHTML = [NSMutableString stringWithCapacity:0];
  self.courseWidth = 0;
 }
 return self;
}


- (void) generatePattern {
 /*for (NSInteger index=0; index<[self.project.colours count]; index++) {
  NSLog (@"project.colours %i %@", index, [self.project.colours objectAtIndex:index]);
  }*/
 
 [self clearPattern];
 
 [self calculatePavingSizePercentages:nil]; // if paving size percentages are user-specified by flag count, work out the corresponding percentages by area, or vice versa, and write them to the global project
 //console.log("percentages calculated: project == " + JSON.stringify(project));
 
 /*for (NSInteger index=0; index<[self.sizeMixFlagSizes count]; index++) {
  NSLog (@"sizeMixFlagSizes %i %@", index, [self.sizeMixFlagSizes objectAtIndex:index]);
  }*/
 
 [self calculateGauges]; // work out how many courses (of each gauge and in total), and roughly how many runs through the array of course gauges, are needed to fill the paving area
 //console.log("gauges calculated: gauges == " + JSON.stringify(gauges) + "; totalRuns == " + totalRuns + "; totalCourses == " + totalCourses);
 
 /*for (NSInteger index=0; index<[self.gauges count]; index++) {
  NSLog (@"gauges %i %@", index, [self.gauges objectAtIndex:index]);
  }
  NSLog (@"totalRuns %f", self.totalRuns);
  NSLog (@"totalCourses %f", self.totalCourses);*/
 
	[self calculateCourses]; // step through all the paving courses that are about to be built, and list the flag gauge and lengths that will be used on each course
 
	/*for (NSInteger index=0; index<[self.courses count]; index++) {
  NSLog (@"courses %i %@", index, [self.courses objectAtIndex:index]);
  }*/
 
 self.availableColoursBuffer = nil;
 
	BOOL evenRun = true; // step through the runs & courses to lay the paving
	NSInteger startGaugeIndex = 0;
	NSInteger gaugeIncrement = 1;
	NSInteger courseIndex = 0;
	BOOL lookAhead = false;
	self.lookAheadLengthBuffer = 0;
	self.lookAheadOffsetBuffer = -1;
	self.flagLengthsUsedBuffer = [NSMutableArray array]; // remember all of the flag lengths used, so that in the next course we can avoid crossroads mortar joints
	self.flagColorsUsedBuffer = [NSMutableArray array]; // remember all of the flag colors used, so that in the next course we might (depending on the number of colors available) reduce vertical blocks of the same color
	self.courseOffsetBuffer = [NSMutableArray array]; // remember offset of previous course in order to avoid repeating it
	self.colorIndexBuffer = -1; // within a course, remember color of previous flag...
	self.colorRepeats = 0; // ...and how many times in a row that color has been used
	self.colorSequence = [NSMutableArray array]; // a pseudo-random array of colors, to ensure that they are evenly mixed
	self.colorSequenceIndex = 0; // the current position in the color array as we scan along it
	for (NSInteger runIndex=0; (float)runIndex<self.totalRuns; runIndex++) {
  evenRun = (runIndex % 2) == 0;
  //		evenRun = (runIndex * 0.5 == round(runIndex * 0.5)); // if there are 3 or more gauges, odd & even runs go in opposite directions through the available course gauges, so keeping the distribution of gauges as smooth and even as possible
		if (evenRun || [self.gauges count] < 3) {
			startGaugeIndex = 0;
			gaugeIncrement = 1;
		} else {
			startGaugeIndex = [self.gauges count] - 1;
			gaugeIncrement = -1;
		}
		for (NSInteger gaugeIndex=startGaugeIndex; gaugeIndex>=0&&gaugeIndex<[self.gauges count]; gaugeIndex+=gaugeIncrement) {
			CGFloat targetCoursesUsed = [[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"coursesUsed"] floatValue] + ([[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"courses"] floatValue] / self.totalRuns);
   //console.log("runIndex == " + runIndex + "; gaugeIndex == " + gaugeIndex + "; gauges[gaugeIndex].coursesUsed == " + gauges[gaugeIndex].coursesUsed + "; targetCoursesUsed == " + targetCoursesUsed);
   
   NSLog (@"course: runIndex %i; courseIndex %i; gaugeIndex %i; coursesUsed %f; targetCoursesUsed %f", runIndex, courseIndex, gaugeIndex, [[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"coursesUsed"] floatValue], targetCoursesUsed);
   //NSLog (@"%f; %f; ", ceil(targetCoursesUsed), ceil([[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"coursesUsed"] floatValue]));
   
   if ((ceil(targetCoursesUsed) > ceil([[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"coursesUsed"] floatValue])) && ((float)courseIndex < self.totalCourses)) {
    //console.log("gauges[gaugeIndex].gauge == " + gauges[gaugeIndex].gauge);
				NSLog (@"gauge: %i", [[[self.gauges objectAtIndex:gaugeIndex] objectForKey:kDMProjectSizeMixWidth] integerValue]);
    [self.flagLengthsUsedBuffer addObject:[NSMutableArray array]];
    [self.flagColorsUsedBuffer addObject:[NSMutableArray array]];
    [self.courseOffsetBuffer addObject:[NSNumber numberWithInteger:0]];
    NSInteger courseOffset = 0;
    NSInteger lengthUsed = 0;
    NSInteger courseBuilds = 1;
    BOOL firstFlagOfCourse = true;
				if (courseIndex < [self.courses count] - 1) { // if this course has multiple flag lengths but the next doesn't, try to design this course so that it will not cause crossroads on the next
					lookAhead = (([[[self.courses objectAtIndex:courseIndex] objectForKey:@"lengths"] count] > 1) && ([[[self.courses objectAtIndex:courseIndex+1] objectForKey:@"lengths"] count] == 1));
					if (lookAhead) {
						self.lookAheadLengthBuffer = [[[[self.courses objectAtIndex:courseIndex+1] objectForKey:@"lengths"] objectAtIndex:0] integerValue];
						NSLog (@"lookAhead: %i", self.lookAheadLengthBuffer);
					}
    }
    do {
					NSInteger flagLength = [self generateRandomFlagLength:[[[self.gauges objectAtIndex:gaugeIndex] objectForKey:kDMProjectSizeMixWidth] integerValue]];
					NSInteger targetLengthUsed = lengthUsed + flagLength;
     BOOL crossroadsRight = NO;
     
     if (lengthUsed == 0) { // on first flag of course, offset the entire course to avoid crossroads at top left corner of flag
						BOOL crossroadsLeft;
						if (self.lookAheadOffsetBuffer > -1) { // if this course's offset was already calculated while setting up the last course, as a result of lookAhead, use that
							courseOffset = self.lookAheadOffsetBuffer;
							targetLengthUsed = self.lookAheadOffsetBuffer + self.lookAheadLengthBuffer;
							if (courseOffset > 0) {[self buildOffset:courseOffset];}
							[self.courseOffsetBuffer removeLastObject];
							[self.courseOffsetBuffer addObject:[NSNumber numberWithInteger:courseOffset]];
							lengthUsed += courseOffset;
							self.lookAheadOffsetBuffer = -1;
							crossroadsLeft = false;
							crossroadsRight = false;
						} else {
							crossroadsLeft = [self checkCrossroads:@"left" targetLengthUsed:targetLengthUsed - flagLength lookAhead:false];
							crossroadsRight = [self checkCrossroads:@"right" targetLengthUsed:targetLengthUsed lookAhead:false];
						}
      if (crossroadsLeft || crossroadsRight) {
       //console.log("crossroads fail: courseIndex == " + courseIndex + "; flagLength == " + flagLength + "; gauge == " + gauges[gaugeIndex].gauge);
							courseOffset = self.crossroadsTolerance;
							do {
								targetLengthUsed = courseOffset + flagLength;
								crossroadsLeft = [self checkCrossroads:@"left" targetLengthUsed:targetLengthUsed - flagLength lookAhead:false];
        //console.log("offsetting course: courseOffsetBuffer[0] == " + courseOffsetBuffer[0] + "; courseOffset == " + courseOffset + "; crossroadsLeft = " + crossroadsLeft);
								if (! crossroadsLeft) { // find a combination of course offset and flag sizes, to start each course without a crossroads at either of the top corners of the first flag
         
									BOOL correctSizeFound = true;
         crossroadsRight = [self checkCrossroads:@"right" targetLengthUsed:targetLengthUsed lookAhead:false];
									if (crossroadsRight) {
          //console.log("flag size fail: flagLength == " + flagLength);
										correctSizeFound = false;
										for (NSInteger sizeIndex=0; sizeIndex<[self.availableFlagLengthsBuffer count]; sizeIndex++) {
											if ([[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectColourPercentageKey] integerValue] > 0) {
												NSInteger targetFlagLength = [[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] integerValue];
												targetLengthUsed = courseOffset + targetFlagLength;
												crossroadsRight = [self checkCrossroads:@"right" targetLengthUsed:targetLengthUsed lookAhead:false];
												if (! crossroadsRight) {
													flagLength = targetFlagLength;
             //console.log("flag size win: flagLength == " + flagLength);
             correctSizeFound = true;
             break;
            } else {
             //console.log("flag size fail: flagLength == " + flagLength);
            }
           }
										}
									}
         if (correctSizeFound) {
										[self buildOffset:courseOffset];
          [self.courseOffsetBuffer removeLastObject];
          [self.courseOffsetBuffer addObject:[NSNumber numberWithInteger:courseOffset]];
          lengthUsed += courseOffset;
          crossroadsRight = [self checkCrossroads:@"right" targetLengthUsed:targetLengthUsed lookAhead:false]; // now the first flag is offset correctly, re-check its top right corner for crossroads
          //console.log("win");
          break;
									}
         
								}
        courseOffset += self.crossroadsTolerance;
							} while (courseOffset <= self.maxCourseOffset);
						}
     } else { // not on first flag of course; only check top right corner for crossroads
						crossroadsRight = [self checkCrossroads:@"right" targetLengthUsed:(lengthUsed + flagLength) lookAhead:lookAhead];
					}
     
     if (crossroadsRight) { // step through the available alternative flag sizes to see if there is one that would prevent the crossroads at top right corner of flag
      if (self.availableFlagLengthsBuffer) {
       //console.log("crossroads fail: flagLength == " + flagLength);
							NSInteger startSizeIndex = 0; // randomise search direction in order to reduce the effect on flag size statistics
							for (NSInteger sizeIndex=0; sizeIndex<[self.availableFlagLengthsBuffer count]; sizeIndex++) {
								if ([[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] integerValue] == flagLength) {
									startSizeIndex = sizeIndex;
									break;
								}
							}
       NSInteger sizeIncrement = arc4random() % 2 == 0 ? 1 : -1;
							for (NSInteger sizeIndex=startSizeIndex+sizeIncrement; sizeIndex>=0&&sizeIndex<[self.availableFlagLengthsBuffer count]; sizeIndex+=sizeIncrement) { // start with the next size up or down from the rejected one, and work towards the end of the array in that direction
								targetLengthUsed = lengthUsed + [[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] integerValue];
        //console.log("outward trying: sizeIncrement == " + sizeIncrement + "; sizeIndex == " + sizeIndex + "; flagLength == " + availableFlagLengthsBuffer[sizeIndex].length);
								crossroadsRight = [self checkCrossroads:@"right" targetLengthUsed:targetLengthUsed lookAhead:lookAhead];
								if ((! crossroadsRight) && ([[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageFlags] integerValue] > 0)) {
									flagLength = [[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] integerValue];
         //console.log("win");
									break;
								}
							}
							if (crossroadsRight) { // if an acceptable flag size is still not found, go to the opposite end of the array and work back towards where we started
								for (NSInteger sizeIndex=(sizeIncrement==1?0:[self.availableFlagLengthsBuffer count]-1); sizeIndex!=startSizeIndex; sizeIndex+=sizeIncrement) {
									targetLengthUsed = lengthUsed + [[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] integerValue];
         //console.log("inward trying: sizeIncrement == " + sizeIncrement + "; sizeIndex == " + sizeIndex + "; flagLength == " + availableFlagLengthsBuffer[sizeIndex].length);
									crossroadsRight = [self checkCrossroads:@"right" targetLengthUsed:targetLengthUsed  lookAhead:lookAhead];
         if ((! crossroadsRight) && ([[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageFlags] integerValue] > 0)) {
										flagLength = [[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] integerValue];
          //console.log("win");
										break;
									}
								}
							}
						}
					}
     if (! crossroadsRight) {
      crossroadsRight = [self checkCrossroads:@"right" targetLengthUsed:(lengthUsed + flagLength) lookAhead:lookAhead];
     } // final re-check for crossroads on this flag
     
     if ((! crossroadsRight) || (courseBuilds >= self.courseBuildsAllowed) || ([self.availableFlagLengthsBuffer count] < 2)) {
						NSString * crossroadsType = nil;
						if (crossroadsRight) {
							if (lookAhead) {
								crossroadsType = @"crossroads-top-or-bottom";
							} else if (courseBuilds > 1) {
								crossroadsType = @"crossroads-too-many-builds";
							} else if ([self.availableFlagLengthsBuffer count] < 2) {
								crossroadsType = @"crossroads-single-length-course";
							} else {
								crossroadsType = @"crossroads";
							}
						}
      NSInteger colorIndexUsed = [self buildFlag:(courseOffset + lengthUsed) length:flagLength gauge:[[[self.gauges objectAtIndex:gaugeIndex] objectForKey:kDMProjectSizeMixWidth] integerValue] crossroads:crossroadsType firstFlagOfCourse:firstFlagOfCourse];
//NSLog(@"colorIndexUsed: %i", colorIndexUsed);
      [[self.flagLengthsUsedBuffer lastObject] addObject:[NSNumber numberWithInteger:flagLength]];
      [[self.flagColorsUsedBuffer lastObject] addObject:[NSNumber numberWithInteger:colorIndexUsed]];
						lengthUsed += flagLength;
						firstFlagOfCourse = false;
      
						if (lengthUsed == courseOffset + flagLength) { // if this is the first flag of the course and we're looking ahead, calculate the offset for the next course so we know where all its flags will be
							if (lookAhead) {
								self.lookAheadOffsetBuffer = 0;
								while ((self.lookAheadOffsetBuffer == courseOffset) || (self.lookAheadOffsetBuffer + self.lookAheadLengthBuffer == lengthUsed)) {
									self.lookAheadOffsetBuffer += self.crossroadsTolerance;
								}
							} else {
								self.lookAheadOffsetBuffer = -1;
							}
NSLog(@"self.lookAheadOffsetBuffer: %i", self.lookAheadOffsetBuffer);
						}
						
					} else { // crossroads
      courseBuilds++;
						NSInteger lastFlagIndex = [[self.flagLengthsUsedBuffer lastObject] count] - 1;
						if (lastFlagIndex > 10) { // go back a few flags and try again
							NSInteger firstFlagIndex = lastFlagIndex - 10;
							NSLog(@"firstFlagIndex: %i; lastFlagIndex: %i", firstFlagIndex, lastFlagIndex);
							for (NSInteger flagIndex=lastFlagIndex; flagIndex>=firstFlagIndex; flagIndex--) {
								NSLog(@"looping: %i %@", flagIndex, [[self.flagLengthsUsedBuffer lastObject] lastObject]);
								lengthUsed -= [[[self.flagLengthsUsedBuffer lastObject] lastObject] integerValue];
								self.courseWidth -= [[[self.flagLengthsUsedBuffer lastObject] lastObject] integerValue] * kDMPatternSizeModified;
								[[self.flagLengthsUsedBuffer lastObject] removeLastObject];
								NSInteger imgIndex = [self.courseHTML rangeOfString:@"<img" options:NSBackwardsSearch].location;
								self.courseHTML = [NSMutableString stringWithString:[self.courseHTML substringToIndex:imgIndex]];
							}
						} else { // or if a course fails early, just scrap it and start again
							lengthUsed = 0;
							[self.flagLengthsUsedBuffer removeLastObject];
							[self.flagLengthsUsedBuffer addObject:[NSMutableArray array]];
							[self.flagColorsUsedBuffer removeLastObject];
							[self.flagColorsUsedBuffer addObject:[NSMutableArray array]];
							[self.courseOffsetBuffer removeLastObject];
							[self.courseOffsetBuffer addObject:[NSNumber numberWithInteger:0]];
							[self rebuildCourse];
							firstFlagOfCourse = true;
						}
						
					}
				} while (lengthUsed < self.patternWidth);
    
				while ([self.flagLengthsUsedBuffer count] > 1) {
     [self.flagLengthsUsedBuffer removeObjectAtIndex:0];
     [self.flagColorsUsedBuffer removeObjectAtIndex:0];
     [self.courseOffsetBuffer removeObjectAtIndex:0];
				}
    //console.log("course finished: courseIndex == " + courseIndex + "; availableFlagLengthsBuffer.length == " + availableFlagLengthsBuffer.length + "; courseBuilds == " + courseBuilds/* + "; flagLengthsUsedBuffer == " + JSON.stringify(flagLengthsUsedBuffer)*/);
				[self buildNewCourse];
				self.availableFlagLengthsBuffer = nil;
				courseIndex++;
    firstFlagOfCourse = true;
			}
   [[self.gauges objectAtIndex:gaugeIndex] setObject:[NSNumber numberWithFloat:targetCoursesUsed] forKey:@"coursesUsed"];
		}
NSLog(@"availableColoursBuffer - %@", self.availableColoursBuffer);
	}
	
	NSInteger totalFlagsUsed =0; // calculate flag size percentages actually used
 for (NSInteger sizeIndex=0; sizeIndex<[self.sizeMixFlagSizes count]; sizeIndex++) {
  totalFlagsUsed += [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:@"flagsUsed"] integerValue];
 }
 for (NSInteger sizeIndex=0; sizeIndex<[self.sizeMixFlagSizes count]; sizeIndex++) {
  [[self.sizeMixFlagSizes objectAtIndex:sizeIndex] setObject:[NSNumber numberWithFloat:(([[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:@"flagsUsed"] floatValue] / (float)totalFlagsUsed) * 100.0f )] forKey:@"percentageFlagsUsed"];
 }
 
 CGRect frame = CGRectMake(0,0,(float)self.patternWidth*kDMPatternSizeModified, (float)self.patternHeight*kDMPatternSizeModified);
 //NSLog(@"Old frame: %@", NSStringFromCGRect(frame));
 frame.size.width = self.calculatedWidth+kDMPatternPreviewPadding;
 frame.origin.x = -(self.maximumLeft);
 //NSLog(@"New frame: %@", NSStringFromCGRect(frame));
 //    self.patternPreview.frame = frame;
 //    self.patternPreviewWrapper = [CALayer layer];
 //    self.patternPreviewWrapper.frame = CGRectMake(0, 0, self.minimumWidth - self.maximumLeft, self.patternPreview.frame.size.height);
 //    [self.patternPreviewWrapper addSublayer:self.patternPreview];
	[self.patternHTML insertString:[NSString stringWithFormat:@"<!DOCTYPE HTML><html><head><meta name=\"viewport\" content=\"width=%.0f, initial-scale=%.3f, minimum-scale=0.2, maximum-scale=3\" />%@</head><body style=\"overflow:hidden;position:relative;width:%.0fpx;height:%.0fpx;background-color:#82796e;\"><div style=\"overflow:hidden;position:relative;width:%.0fpx;height:%.0fpx;\"><div style=\"position:absolute;width:%.0fpx;height:%.0fpx;left:%.0fpx;top:%.0f; -webkit-transform: scale(1,1);\">",  (self.minimumWidth-self.maximumLeft), (1024.0/(self.minimumWidth-self.maximumLeft)), [self addCSSClasses], (self.minimumWidth-self.maximumLeft), frame.size.height, (self.minimumWidth-self.maximumLeft), frame.size.height, ((float)frame.size.width*2.0f), ((float)frame.size.height*2.0f), frame.origin.x, 0.0f] atIndex:0];
    [self.patternHTML appendString:@"</div></div></body></html>"];

    //[self.patternHTML insertString:[NSString stringWithFormat:@"<!DOCTYPE HTML><html><head>%@</head><body style=\"overflow:hidden;position:relative;width:1024px;height:768px;background-color:#82796e;\"><div style=\"position:absolute;width:%.0fpx;height:%.0fpx;left:%.0fpx;top:%.0f;\">", [self addCSSClasses], (self.minimumWidth-self.maximumLeft), frame.size.height, (self.minimumWidth-self.maximumLeft), frame.size.height, ((float)frame.size.width*2.0f), ((float)frame.size.height*2.0f), frame.origin.x, 0.0f] atIndex:0];
 //[self.patternHTML appendString:@"</div></body></html>"];
 
NSLog(@"minimumWidth %f; maximumLeft %f", self.minimumWidth, self.maximumLeft);
}

-(NSString *)addCSSClasses{
    NSMutableString *css = [[NSString stringWithString:@"<style type=\"text/css\">\n"] mutableCopy];
    [css appendString:@"html, body, div, img { background:#83796f; margin:0; padding:0; line-height:1px}\n"];
    if (kDMUseCSS){
        [css appendString:[NSString stringWithFormat:@".flag100x100 { -webkit-transform: scale(%0.1f,%0.1f); margin:-40px -40px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag125x125 { -webkit-transform: scale(%0.1f,%0.1f); margin:-50px -50px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag150x150 { -webkit-transform: scale(%0.1f,%0.1f); margin:-60px -60px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag170x100 { -webkit-transform: scale(%0.1f,%0.1f); margin:-40px -68px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag180x125 { -webkit-transform: scale(%0.1f,%0.1f); margin:-50px -72px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag200x100 { -webkit-transform: scale(%0.1f,%0.1f); margin:-40px -80px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag200x150 { -webkit-transform: scale(%0.1f,%0.1f); margin:-60px -80px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag230x100 { -webkit-transform: scale(%0.1f,%0.1f); margin:-40px -92px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag240x125 { -webkit-transform: scale(%0.1f,%0.1f); margin:-50px -96px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag250x150 { -webkit-transform: scale(%0.1f,%0.1f); margin:-60px -100px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag300x100 { -webkit-transform: scale(%0.1f,%0.1f); margin:-40px -120px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag300x125 { -webkit-transform: scale(%0.1f,%0.1f); margin:-50px -120px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag300x150 { -webkit-transform: scale(%0.1f,%0.1f); margin:-60px -120px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag300x300 { -webkit-transform: scale(%0.1f,%0.1f); margin:-120px -120px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag450x450 { -webkit-transform: scale(%0.1f,%0.1f); margin:-180px -180px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag500x300 { -webkit-transform: scale(%0.1f,%0.1f); margin:-120px -200px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag600x450 { -webkit-transform: scale(%0.1f,%0.1f); margin:-180px -240px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag600x600 { -webkit-transform: scale(%0.1f,%0.1f); margin:-240px -240px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag700x300 { -webkit-transform: scale(%0.1f,%0.1f); margin:-120px -280px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag700x600 { -webkit-transform: scale(%0.1f,%0.1f); margin:-240px -280px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag750x450 { -webkit-transform: scale(%0.1f,%0.1f); margin:-180px -300px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag800x100 { -webkit-transform: scale(%0.1f,%0.1f); margin:-40px -320px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag800x600 { -webkit-transform: scale(%0.1f,%0.1f); margin:-240px -320px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag900x300 { -webkit-transform: scale(%0.1f,%0.1f); margin:-120px -360px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag900x450 { -webkit-transform: scale(%0.1f,%0.1f); margin:-180px -360px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
        [css appendString:[NSString stringWithFormat:@".flag900x600 { -webkit-transform: scale(%0.1f,%0.1f); margin:-240px -360px; }\n", kDMPatternSizeModified, kDMPatternSizeModified]];
    }
    [css appendString:@"</style>\n"];
    return css;
}
- (NSArray*) quoteDetails {
 NSMutableDictionary *detailsHash = [NSMutableDictionary dictionaryWithCapacity:10];
 for (NSString *flagKey in self.usedFlags) {
  NSNumber *count = [detailsHash objectForKey:flagKey];
  if (count) {
   count = [NSNumber numberWithInteger:([count integerValue]+1)];
  } else {
   count = [NSNumber numberWithInteger:1];
  }
  [detailsHash setObject:count forKey:flagKey];
 }
 NSMutableArray *details = [NSMutableArray arrayWithCapacity:[detailsHash count]];
 for (NSString *flagKey in detailsHash) {
  NSArray *flagDesc = [flagKey componentsSeparatedByString:@"-"];
  [details addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                      [flagDesc objectAtIndex:0], kDMProjectItemColourReferenceKey,
                      [flagDesc objectAtIndex:1], kDMProjectSizeMixLength,
                      [flagDesc objectAtIndex:2], kDMProjectSizeMixWidth,
                      [detailsHash objectForKey:flagKey], kDMProjectOrderQuantity,
                      nil]];
 }
 return [NSArray arrayWithArray:details];
}

- (void) calculatePavingSizePercentages:(NSString*)percentagePriority { // if paving size percentages are user-specified by flag count, work out the corresponding percentages by area, or vice versa, and write them to the global project
	if (percentagePriority == nil) {percentagePriority = kDMProjectPercentagePriority;} // if priority not specified in function call, use global default priority
	CGFloat subtotal = 0;
	NSMutableArray *subtotals = [NSMutableArray array];
	CGFloat total = 0;
 for (NSInteger sizeIndex=0; sizeIndex<[self.sizeMixFlagSizes count]; sizeIndex++) {
  if ([percentagePriority isEqualToString:@"flags"]) {
   subtotal = [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageFlags] floatValue] * [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixWidth] floatValue] * [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] floatValue];
  } else {
   subtotal = [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageArea] floatValue] / ( [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixWidth] floatValue] * [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] floatValue]);
  }
  [subtotals addObject:[NSNumber numberWithFloat:subtotal]];
  total += subtotal;
 }
 for (NSInteger sizeIndex=0; sizeIndex<[self.sizeMixFlagSizes count]; sizeIndex++) {
  NSMutableDictionary *size = [self.sizeMixFlagSizes objectAtIndex:sizeIndex];
  [size setObject:[NSNumber numberWithInteger:0] forKey:@"flagsUsed"];
  [size setObject:[NSNumber numberWithFloat:0] forKey:@"percentageFlagsUsed"];
  if ([percentagePriority isEqualToString:@"flags"]) {
   [size setObject:[NSNumber numberWithFloat:([[subtotals objectAtIndex:sizeIndex] floatValue] / total * 100.0f)] forKey:kDMProjectSizeMixPercentageArea];
  } else {
   [size setObject:[NSNumber numberWithFloat:floorf(([[subtotals objectAtIndex:sizeIndex] floatValue] / total * 100.0f))] forKey:kDMProjectSizeMixPercentageFlags];
  }
 }
}

- (void) calculateGauges { // work out how many courses (of each gauge and in total), and roughly how many runs through the array of course gauges, are needed to fill the paving area
	CGFloat subtotalPercentageArea = 0;
	CGFloat totalLength = 0;
 self.gauges = [NSMutableArray array];
	CGFloat flagGauge = 0;
	BOOL gaugeDefined = false;
 NSInteger gaugeIndex = 0;
 for (NSInteger sizeIndex=0; sizeIndex<[self.sizeMixFlagSizes count]; sizeIndex++) {
  subtotalPercentageArea = [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageArea] floatValue];
  flagGauge = [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixWidth] floatValue];
  gaugeDefined = false;
  for (gaugeIndex=0; gaugeIndex<[self.gauges count]; gaugeIndex++) {
   if ([[[self.gauges objectAtIndex:gaugeIndex] objectForKey:kDMProjectSizeMixWidth] floatValue] == flagGauge) {
    gaugeDefined = true;
    break;
   }
  }
  if (gaugeDefined) {
   [[self.gauges objectAtIndex:gaugeIndex] setObject:[NSNumber numberWithFloat:([[[self.gauges objectAtIndex:gaugeIndex] objectForKey:kDMProjectSizeMixPercentageArea] floatValue] + subtotalPercentageArea)] forKey:kDMProjectSizeMixPercentageArea];
  } else {
   [self.gauges addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                           [NSNumber numberWithFloat:0], kDMProjectSizeMixLength,
                           [NSNumber numberWithFloat:flagGauge], kDMProjectSizeMixWidth,
                           [NSNumber numberWithFloat:0], @"courses",
                           [NSNumber numberWithFloat:0], @"coursesUsed",
                           [NSNumber numberWithFloat:0], @"percentageCourses",
                           [NSNumber numberWithFloat:subtotalPercentageArea], kDMProjectSizeMixPercentageArea,
                           nil]];
   // length: total length of all courses of this gauge, coursesUsed: number of courses of this gauge that have been added to the pattern so far (to be used later)
  }
 }
 
	NSSortDescriptor *sortDescriptor; // sort courses by ascending gauge
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kDMProjectSizeMixWidth ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
 [self.gauges sortUsingDescriptors:sortDescriptors];
 
 for (gaugeIndex=0; gaugeIndex<[self.gauges count]; gaugeIndex++) {
  NSMutableDictionary *gauge = [self.gauges objectAtIndex:gaugeIndex];
  [gauge setObject:[NSNumber numberWithFloat:((([[gauge objectForKey:kDMProjectSizeMixPercentageArea] floatValue] * 0.01 ) * (self.patternWidth*self.patternHeight)) / [[gauge objectForKey:kDMProjectSizeMixWidth] floatValue])] forKey:kDMProjectSizeMixLength];
  totalLength += [[gauge objectForKey:kDMProjectSizeMixLength] floatValue];
	}
 for (gaugeIndex=0; gaugeIndex<[self.gauges count]; gaugeIndex++) {
  NSMutableDictionary *gauge = [self.gauges objectAtIndex:gaugeIndex];
  [gauge setObject:[NSNumber numberWithFloat:([[gauge objectForKey:kDMProjectSizeMixLength] floatValue] / self.patternHeight)] forKey:@"courses"];
  [gauge setObject:[NSNumber numberWithFloat:(([[gauge objectForKey:kDMProjectSizeMixLength] floatValue] / totalLength) * 100.0f)] forKey:@"percentageCourses"];
	}
	self.totalCourses = totalLength / self.patternWidth;
	CGFloat maxCoursesPerGauge = 0;
	for (gaugeIndex=0; gaugeIndex<[self.gauges count]; gaugeIndex++) {
  NSMutableDictionary *gauge = [self.gauges objectAtIndex:gaugeIndex];
  if ([[gauge objectForKey:@"courses"] floatValue] > maxCoursesPerGauge) {
   maxCoursesPerGauge = [[gauge objectForKey:@"courses"] floatValue];
  }
	}
	self.totalRuns = maxCoursesPerGauge;
 
}

- (void) calculateCourses { // step through all the paving courses that are about to be built, and list the flag gauge and lengths that will be used on each course
 self.courses = [NSMutableArray array];
	BOOL evenRun = true; // step through the runs & courses as if we were laying the paving
	NSInteger startGaugeIndex = 0;
	NSInteger gaugeIncrement = 1;
	NSInteger courseIndex = 0;
	for (NSInteger runIndex=0; (float)runIndex<self.totalRuns; runIndex++) {
  evenRun = (runIndex % 2) == 0;
		if (evenRun || [self.gauges count] < 3) {
			startGaugeIndex = 0;
			gaugeIncrement = 1;
		} else {
			startGaugeIndex = [self.gauges count] - 1;
			gaugeIncrement = -1;
		}
		for (NSInteger gaugeIndex=startGaugeIndex; gaugeIndex>=0&&gaugeIndex<[self.gauges count]; gaugeIndex+=gaugeIncrement) {
			CGFloat targetCoursesUsed = [[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"coursesUsed"] floatValue] + ([[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"courses"] floatValue] / self.totalRuns);
   //NSLog (@"gauge: runIndex %i; courseIndex %i; gaugeIndex %i; coursesUsed %f; targetCoursesUsed %f", runIndex, courseIndex, gaugeIndex, [[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"coursesUsed"] floatValue], targetCoursesUsed);
   //NSLog (@"%f; %f; ", ceil(targetCoursesUsed), ceil([[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"coursesUsed"] floatValue]));
   if ((ceil(targetCoursesUsed) > ceil([[[self.gauges objectAtIndex:gaugeIndex] objectForKey:@"coursesUsed"] floatValue])) && ((float)courseIndex < self.totalCourses)) {
				CGFloat flagGauge = [[[self.gauges objectAtIndex:gaugeIndex] objectForKey:kDMProjectSizeMixWidth] integerValue];
				//NSLog (@"course: %i", [[[self.gauges objectAtIndex:gaugeIndex] objectForKey:kDMProjectSizeMixWidth] integerValue]);
				[self.courses addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithFloat:flagGauge], kDMProjectSizeMixWidth,
                             [NSMutableArray array], @"lengths",
                             nil]];
				for (NSInteger sizeIndex=0; sizeIndex<[self.sizeMixFlagSizes count]; sizeIndex++) {
					if (([[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixWidth] floatValue] == flagGauge) && ([[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageFlags] floatValue] > 0)) {
						CGFloat flagLength = [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] integerValue];
						//NSLog (@"        %i", [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] integerValue]);
						[[[self.courses objectAtIndex:[self.courses count] - 1] objectForKey:@"lengths"] addObject:[NSNumber numberWithFloat:flagLength]];
					}
				}
			}
			[[self.gauges objectAtIndex:gaugeIndex] setObject:[NSNumber numberWithFloat:targetCoursesUsed] forKey:@"coursesUsed"];
		}
	}
	for (NSInteger gaugeIndex=0; gaugeIndex<[self.gauges count]; gaugeIndex++) { // reset all the coursesUsed properties in gauges to zero, before starting to build the paving, so that the paving algorithm will start again from the same zero point as this function did and thus use the same course gauges in the same order
		[[self.gauges objectAtIndex:gaugeIndex] setObject:[NSNumber numberWithFloat:0] forKey:@"coursesUsed"];
	}
}



- (NSInteger) generateRandomFlagLength:(NSInteger)flagGauge { // select a random flag length appropriate to the current course gauge
	CGFloat flagLength = 1000; // default value to prevent infinite loop while debugging
 if (! self.availableFlagLengthsBuffer) { // for speed optimisation: only search for flag sizes that are compatible with the current gauge, if this is the first flag of the course.  otherwise use the global buffer copy of this data
		self.availableFlagLengthsBuffer = [NSMutableArray array];
		self.availableFlagLengthsTotalPercentage = 0;
  for (NSInteger sizeIndex=0; sizeIndex<[self.sizeMixFlagSizes count]; sizeIndex++) {
   if (([[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixWidth] floatValue] == flagGauge) && ([[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageFlags] floatValue] > 0)) {
    self.availableFlagLengthsTotalPercentage += [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageFlags] floatValue]; // the percentages might not add up to 100 because we are only dealing with flags that fit the current course gauge, so we need to remember the total, and treat the percentages as fractions of this
    [self.availableFlagLengthsBuffer addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                [[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength], kDMProjectSizeMixLength,
                                                [[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageFlags], kDMProjectSizeMixPercentageFlags,
                                                [NSNumber numberWithFloat:self.availableFlagLengthsTotalPercentage], @"percentileFlags",
                                                nil]];
    if ([[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixPercentageFlags] floatValue] > 0) {
     flagLength = [[[self.sizeMixFlagSizes objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] floatValue];
    }
    // default value for when not using random
   }
  }
	}
 if ([self.availableFlagLengthsBuffer count] >= 2) { // for speed optimisation: only run the randomiser if there are at least 2 flag sizes available
		flagLength = [[[self.availableFlagLengthsBuffer objectAtIndex:0] objectForKey:kDMProjectSizeMixLength] floatValue];
  NSInteger skyhook = arc4random() % (int)self.availableFlagLengthsTotalPercentage; // pick random flag size
		for (NSInteger sizeIndex=0; sizeIndex<[self.availableFlagLengthsBuffer count]; sizeIndex++) {
			if ([[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:@"percentileFlags"] integerValue] >= skyhook) {
				flagLength = [[[self.availableFlagLengthsBuffer objectAtIndex:sizeIndex] objectForKey:kDMProjectSizeMixLength] integerValue];
				break;
			}
		}
	} else if ([self.availableFlagLengthsBuffer count] == 1) {
  flagLength = [[[self.availableFlagLengthsBuffer objectAtIndex:0] objectForKey:kDMProjectSizeMixLength] floatValue];
	}
	return flagLength;
}

-(BOOL) checkCrossroads:(NSString*)side targetLengthUsed:(NSInteger)targetLengthUsed lookAhead:(BOOL)lookAhead {
 BOOL crossroads = false;
	if ([self.flagLengthsUsedBuffer count] > 1) {
		NSInteger lengthUsedInPreviousCourse = [[self.courseOffsetBuffer objectAtIndex:0] integerValue];
		for (NSInteger flagIndex=0; flagIndex<[[self.flagLengthsUsedBuffer objectAtIndex:0] count]; flagIndex++) {
   if ([side isEqualToString:@"right"]) { // checking for crossroads at top right corner of flag
    lengthUsedInPreviousCourse += [[[self.flagLengthsUsedBuffer objectAtIndex:0] objectAtIndex:flagIndex] integerValue];
   }
   if (abs(lengthUsedInPreviousCourse - targetLengthUsed) < self.crossroadsTolerance) {
				crossroads = true;
    //                NSLog(@"crossroads");
				break;
			} else if (lengthUsedInPreviousCourse > targetLengthUsed) { // for speed optimisation: searched past any possible crossroads so stop now
				break;
			}
			if ([side isEqualToString:@"left"]) { // top left corner
    lengthUsedInPreviousCourse += [[[self.flagLengthsUsedBuffer objectAtIndex:0] objectAtIndex:flagIndex] integerValue];
   }
		}
	}
	if ((! crossroads) && lookAhead) { // if looking ahead, check bottom corner for crossroads too
		NSInteger lengthUsedInPreviousCourse = self.lookAheadOffsetBuffer;
		while (lengthUsedInPreviousCourse < self.patternWidth) {
			if ([side isEqualToString:@"right"]) { // checking for crossroads at bottom right corner of flag
    lengthUsedInPreviousCourse += self.lookAheadLengthBuffer;
   }
   if (abs(lengthUsedInPreviousCourse - targetLengthUsed) < self.crossroadsTolerance) {
				crossroads = true;
				//                NSLog(@"crossroads");
				break;
			} else if (lengthUsedInPreviousCourse > targetLengthUsed) { // for speed optimisation: searched past any possible crossroads so stop now
				break;
			}
			if ([side isEqualToString:@"left"]) { // bottom left corner
    lengthUsedInPreviousCourse += self.lookAheadLengthBuffer;
   }
		}
	}
 return crossroads;
}

-(BOOL) checkColorOverlap:(NSInteger)flagLeft flagLength:(NSInteger)flagLength targetColorIndex:(NSInteger)targetColorIndex {
 BOOL colorOverlap = false;
 NSInteger previousCourseFlagLeft;
 NSInteger previousCourseFlagRight;
 if ([self.flagLengthsUsedBuffer count] > 1) {
		NSInteger lengthUsedInPreviousCourse = [[self.courseOffsetBuffer objectAtIndex:0] integerValue];
		for (NSInteger flagIndex=0; flagIndex<[[self.flagLengthsUsedBuffer objectAtIndex:0] count]; flagIndex++) {
   previousCourseFlagLeft = lengthUsedInPreviousCourse;
   lengthUsedInPreviousCourse += [[[self.flagLengthsUsedBuffer objectAtIndex:0] objectAtIndex:flagIndex] integerValue];
   previousCourseFlagRight = lengthUsedInPreviousCourse;
//NSLog(@"checkColorOverlap: previousCourseFlagLeft %i; previousCourseFlagRight %i", previousCourseFlagLeft, previousCourseFlagRight);
//NSLog(@"checkColorOverlap: flagLeft %i; flagLength %i", flagLeft, flagLength);
   if (previousCourseFlagLeft < flagLeft + flagLength) {
//NSLog(@"checkColorOverlap c");
				if ((previousCourseFlagRight > flagLeft + (flagLength * 0.5)) && (previousCourseFlagLeft < flagLeft + (flagLength * 0.5))) {
//NSLog(@"checkColorOverlap d");
					if ([self.flagColorsUsedBuffer[0][flagIndex] integerValue] == targetColorIndex) {
//NSLog(@"checkColorOverlap e");
						colorOverlap = true;
						break;
					}
				}
			} else { // for speed optimisation: searched past any possible overlaps so stop now
				break;
			}
		}
	}
//if (colorOverlap) {NSLog(@"colorOverlappings");}
 return colorOverlap;
}

- (void) buildOffset:(NSInteger)length {
	//	$("div.pattern-preview").append("<image src=\"images/textures/offset.png\" width=\"" + (length * kDMPatternSizeModified) + "\"  height=\"1\">");
 self.maximumLeft = MAX(self.maximumLeft, (CGFloat)length);
 [self.courseHTML appendFormat:@"<img src=\"offset.png\" width=\"%.0f\" height=\"1\">", ((float)length*kDMPatternSizeModified)];
 self.courseWidth += (float)length*kDMPatternSizeModified;
}

- (NSInteger) buildFlag:(NSInteger)flagLeft length:(NSInteger)length gauge:(NSInteger)gauge crossroads:(NSString*)crossroads firstFlagOfCourse:(BOOL)firstFlagOfCourse {
	if (! self.availableColoursBuffer) { // for speed optimisation: cache color percentages (enabled colors only) as percentiles to aid random selection
  [self generateAvailableColorsBuffer];
 }

 NSInteger colorIndexTarget = 0; // default color, eg. if there is only 1 color available
 BOOL repeatOK = true;
 BOOL checkColorOverlapResult = false;
 
	if ([self.availableColoursBuffer count] >= 3) { // 3-5 colors available; use getSequenceColor() for the most even pseudo-random color distribution
		colorIndexTarget = [self getSequenceColor];
		if (self.colorIndexBuffer != -1) {
			if (self.colorRepeats >= [[[self.availableColoursBuffer objectAtIndex:self.colorIndexBuffer] objectForKey:@"maxRepeats"] integerValue]) {
		 	repeatOK = false;
		 }
		}
	} else if ([self.availableColoursBuffer count] == 2) { // 2 colors available; use getRandomColor()
		colorIndexTarget = [self getRandomColor];
		if (self.colorIndexBuffer != -1) {
			if (self.colorRepeats >= [[[self.availableColoursBuffer objectAtIndex:self.colorIndexBuffer] objectForKey:@"maxRepeats"] integerValue]) {
		 	repeatOK = false;
		 }
		}
	}
 
	if ([self.availableColoursBuffer count] >= 2) {
  
  checkColorOverlapResult = false;
  
		if (firstFlagOfCourse) {
			if ([self.availableColoursBuffer count] >= 3) {
				if ([self.flagLengthsUsedBuffer count] > 1) {
					CGFloat checkColorOverlapThreshold = ([[[self.availableColoursBuffer objectAtIndex:self.colorIndexBuffer] objectForKey:kDMProjectColourPercentageKey] integerValue] - 5) * 2; // the higher the percentage of this color, the less likely we are to do the color overlap check. this prevents high-percentage colors from being under-represented in the final pattern
					if (arc4random() % 100 > checkColorOverlapThreshold) {
						checkColorOverlapResult = (colorIndexTarget == [[[self.flagColorsUsedBuffer objectAtIndex:0] objectAtIndex:0] integerValue]);
						if (checkColorOverlapResult) {
       
       NSMutableArray *disallowedColors = [NSMutableArray arrayWithObjects:[NSNumber numberWithInteger:self.colorIndexBuffer], nil];
							colorIndexTarget = [self getRandomColor:disallowedColors];
						}
					}
				}
			}
			self.colorIndexBuffer = colorIndexTarget;
			self.colorRepeats = 1;
   
		} else if (repeatOK) {
   
			if ([self.availableColoursBuffer count] >= 2) {
    CGFloat thresholdScale = ([self.availableColoursBuffer count] >= 2 ? 2 : 0.5); // if only 2 colors, increase probability of overlap check
    CGFloat checkColorOverlapThreshold = ([[[self.availableColoursBuffer objectAtIndex:colorIndexTarget] objectForKey:kDMProjectColourPercentageKey] integerValue] - 5) * thresholdScale; // the higher the percentage of this color, the less likely we are to do the color overlap check. this prevents high-percentage colors from being under-represented in the final pattern
				if (arc4random() % 100 > checkColorOverlapThreshold) {
					checkColorOverlapResult = [self checkColorOverlap:flagLeft flagLength:length targetColorIndex:colorIndexTarget];
					if (checkColorOverlapResult) {
						if ([self.availableColoursBuffer count] >= 3) {
       NSMutableArray *disallowedColors = [NSMutableArray arrayWithObjects:[NSNumber numberWithInteger:self.colorIndexBuffer], [NSNumber numberWithInteger:colorIndexTarget], nil];
							colorIndexTarget = [self getRandomColor:disallowedColors];
						} else { // only 2 colors available; just use whichever one is not colorIndexTarget
						 colorIndexTarget = 1 - colorIndexTarget;
						}
					}
				}
	 	}
   
			if (colorIndexTarget == self.colorIndexBuffer) {
				self.colorRepeats++;
			} else {
				self.colorIndexBuffer = colorIndexTarget;
				self.colorRepeats = 1;
			}
   
		} else {
   NSMutableArray *disallowedColors = [NSMutableArray arrayWithObjects:[NSNumber numberWithInteger:self.colorIndexBuffer], nil];
   colorIndexTarget = [self getRandomColor:disallowedColors];
   
			if ([self.availableColoursBuffer count] >= 2) {
    CGFloat thresholdScale = ([self.availableColoursBuffer count] >= 2 ? 2 : 0.5); // if only 2 colors, increase probability of overlap check
    CGFloat checkColorOverlapThreshold = ([[[self.availableColoursBuffer objectAtIndex:colorIndexTarget] objectForKey:kDMProjectColourPercentageKey] integerValue] - 5) * thresholdScale; // the higher the percentage of this color, the less likely we are to do the color overlap check. this prevents high-percentage colors from being under-represented in the final pattern
//NSLog(@"checkColorOverlapThreshold %f", checkColorOverlapThreshold);
				if (arc4random() % 100 > checkColorOverlapThreshold) {
					checkColorOverlapResult = [self checkColorOverlap:flagLeft flagLength:length targetColorIndex:colorIndexTarget];
					if (checkColorOverlapResult) {
						if ([self.availableColoursBuffer count] >= 3) {
       NSMutableArray *disallowedColors = [NSMutableArray arrayWithObjects:[NSNumber numberWithInteger:self.colorIndexBuffer], [NSNumber numberWithInteger:colorIndexTarget], nil];
							colorIndexTarget = [self getRandomColor:disallowedColors];
						} else { // only 2 colors available; just use whichever one is not colorIndexTarget
						 colorIndexTarget = 1 - colorIndexTarget;
						}
					}
				}
	 	}
   
			self.colorIndexBuffer = colorIndexTarget;
			self.colorRepeats = 1;
		}
	} else { // only one color
	 self.colorIndexBuffer = colorIndexTarget;
	 self.colorRepeats = 1;
	}
 
//NSLog(@"colorIndexTarget %i", colorIndexTarget);
	NSString *ref = [[self.availableColoursBuffer objectAtIndex:colorIndexTarget] objectForKey:kDMProjectColourReferenceKey];

 //NSLog(@"ref %@", ref);
 //	ref = @"gra902"; // debug
 [self.courseFlags addObject:[NSString stringWithFormat:@"%@-%i-%i", ref, length, gauge]];
 NSString *flagImageName = [NSString stringWithFormat:@"%@-%ix%i.jpg", ref, length, gauge];
   
 //NSString *flagImage = [NSString stringWithFormat:@"%@:%ix%i.jpg", ref, length, gauge];
	/*if (crossroads) {
  flagImageName = [NSString stringWithFormat:@"%@.png", crossroads];
  //NSLog(@"crossroads - %@", flagImageName);
  }*/
 UIImage *flagImage = [UIImage imageNamed:flagImageName];
 if (!flagImage) {
  //NSLog(@"FLAG IMAGE NOT FOUND %@", flagImageName);
 }
 
 // Bump the origin along by one
 //	$("div.pattern-preview").append("<image src=\"images/textures/" + flagImage + "\" width=\"" + (length * kDMPatternSizeModified) + "\"  height=\"" + (gauge * kDMPatternSizeModified) + "\">");
    if (kDMUseCSS){
         NSString *flagClassName = [NSString stringWithFormat:@"flag%ix%i", length, gauge];
        [self.courseHTML appendFormat:@"<img src=\"%@\" class=\"%@\">", flagImageName, flagClassName];
    }else{
        [self.courseHTML appendFormat:@"<img src=\"%@\" width=\"%.0f\" height=\"%.0f\">", flagImageName, ((CGFloat)length * kDMPatternSizeModified), ((CGFloat)gauge * kDMPatternSizeModified)];
    }
 self.courseWidth += (CGFloat)length*kDMPatternSizeModified;
 
 for (NSInteger sizeIndex=0; sizeIndex<[self.sizeMixFlagSizes count]; sizeIndex++) { // record flag sizes used in order to check percentage accuracies
  NSMutableDictionary *flagSize = [self.sizeMixFlagSizes objectAtIndex:sizeIndex];
  if ([[flagSize objectForKey:kDMProjectSizeMixLength] integerValue] == length) {
   if ([[flagSize objectForKey:kDMProjectSizeMixWidth] integerValue] == gauge) {
    [flagSize setObject:[NSNumber numberWithInteger:([[flagSize objectForKey:@"flagsUsed"] integerValue]+1)] forKey:@"flagsUsed"];
    break;
   }
  }
 }
 self.lastGauge = gauge;
 NSInteger flagsUsed = [[[self.availableColoursBuffer objectAtIndex:colorIndexTarget] objectForKey:@"flagsUsed"] integerValue];
 [[self.availableColoursBuffer objectAtIndex:colorIndexTarget] setObject:[NSNumber numberWithInteger:flagsUsed + 1] forKey:@"flagsUsed"];
 return colorIndexTarget;
}

- (void) generateAvailableColorsBuffer { // for speed optimisation: cache color percentages (enabled colors only) as percentiles to aid random selection
	self.availableColoursBuffer = [NSMutableArray array];
	NSInteger availableColorsMinIndex = nil;
	NSInteger availableColorsMinPercentage = 100;
	NSInteger availableColorsTotalPercentage = 0;
	for (NSInteger colorIndex=0; colorIndex<[self.project.colours count]; colorIndex++) {
		//if ([self.project.colours objectAtIndex:colorIndex].enabled) {
  if ([[[self.project.colours objectAtIndex:colorIndex] objectForKey:kDMProjectColourPercentageKey] integerValue] < availableColorsMinPercentage) {
   availableColorsMinIndex = [self.availableColoursBuffer count];
   availableColorsMinPercentage = [[[self.project.colours objectAtIndex:colorIndex] objectForKey:kDMProjectColourPercentageKey] integerValue];
  }
  availableColorsTotalPercentage += [[[self.project.colours objectAtIndex:colorIndex] objectForKey:kDMProjectColourPercentageKey] integerValue];
  [self.availableColoursBuffer addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [[self.project.colours objectAtIndex:colorIndex] objectForKey:kDMProjectColourReferenceKey], kDMProjectColourReferenceKey,
                                          [[self.project.colours objectAtIndex:colorIndex] objectForKey:kDMProjectColourPercentageKey], kDMProjectColourPercentageKey,
                                          [NSNumber numberWithInteger:availableColorsTotalPercentage], @"percentile",
                                          [NSNumber numberWithInt:1], @"maxRepeats",
                                          [NSNumber numberWithInt:0], @"flagsUsed",
                                          nil]];
		//}
	}
 
	NSInteger maxRepeatsTotal = 0;
 NSInteger maxRepeatsTarget;
	for (NSInteger colorIndex=0; colorIndex<[self.availableColoursBuffer count]; colorIndex++) {
		if (colorIndex == availableColorsMinIndex) {
			maxRepeatsTotal++;
		} else {
   maxRepeatsTarget = ceil((CGFloat)[[[self.availableColoursBuffer objectAtIndex:colorIndex] objectForKey:kDMProjectColourPercentageKey] floatValue] / availableColorsMinPercentage);
   [[self.availableColoursBuffer objectAtIndex:colorIndex] setObject:[NSNumber numberWithInteger:maxRepeatsTarget] forKey:@"maxRepeats"]; //[[self.availableColoursBuffer objectAtIndex:colorIndex] objectForKey:@"maxRepeats"] = maxRepeatsTarget;
			maxRepeatsTotal += maxRepeatsTarget;
		}
	}
 if ([self.availableColoursBuffer count] == 3) { // special case: with 3 equal 33% colors, the last percentage gets rounded up to 34 to make the total 100.  this makes the last maxRepeats higher than the others, so we force it back down to 1
  if (([[[self.availableColoursBuffer objectAtIndex:0] objectForKey:kDMProjectColourPercentageKey] integerValue] == 33) && ([[[self.availableColoursBuffer objectAtIndex:1] objectForKey:kDMProjectColourPercentageKey] integerValue] == 33) && ([[[self.availableColoursBuffer objectAtIndex:2] objectForKey:kDMProjectColourPercentageKey] integerValue] == 34)) {
   [[self.availableColoursBuffer objectAtIndex:2] setObject:[NSNumber numberWithInteger:1] forKey:@"maxRepeats"];
   maxRepeatsTotal = 3;
  }
 }
	if (maxRepeatsTotal == [self.availableColoursBuffer count]) { // if all colors have maxRepeats=1, change them to 2 for more pattern variety
		for (NSInteger colorIndex=0; colorIndex<[self.availableColoursBuffer count]; colorIndex++) {
			[[self.availableColoursBuffer objectAtIndex:colorIndex] setObject:[NSNumber numberWithInteger:2] forKey:@"maxRepeats"]; //[[self.availableColoursBuffer objectAtIndex:colorIndex] objectForKey:@"maxRepeats"] = 2;
		}
	}
NSLog(@"availableColoursBuffer - %@", self.availableColoursBuffer);
}

- (NSInteger) getSequenceColor { // get pseudo-random color from an array, to ensure that colors are evenly mixed
//NSLog(@"getSequenceColor");
//NSLog(@"self.colorSequence - %@", self.colorSequence);
//NSLog(@"! self.colorSequence count - %i", [self.colorSequence count]);
	if (([self.colorSequence count] == 0) || (arc4random() % 100 >= 88)) { // build the array if it does not exist yet; also rebuild it occasionally to break up any diagonal banding
		self.colorSequence = [NSMutableArray array];
		NSMutableArray *counters = [NSMutableArray array];
		NSInteger sequenceLength = 0;
		for (NSInteger colorIndex=0; colorIndex<[self.availableColoursBuffer count]; colorIndex++) {
   [counters addObject:[NSNumber numberWithFloat:0]];
			sequenceLength += [[[self.availableColoursBuffer objectAtIndex:colorIndex] objectForKey:@"maxRepeats"] integerValue];
		}
		sequenceLength *= 8;
		if (sequenceLength < 30) {sequenceLength *= 2;}
//NSLog(@"getSequenceColor sequenceLength %i", sequenceLength);
  CGFloat counterTarget = 0;
		do {
   for (NSInteger colorIndex=0; colorIndex<[self.availableColoursBuffer count]; colorIndex++) {
//NSLog(@"colorIndex %i", colorIndex);
    counterTarget  = [[counters objectAtIndex:colorIndex] floatValue] + ([[[self.availableColoursBuffer objectAtIndex:colorIndex] objectForKey:kDMProjectColourPercentageKey] floatValue] / 100);
//NSLog(@"[[counters objectAtIndex:colorIndex] floatValue] %f", [[counters objectAtIndex:colorIndex] floatValue]);
//NSLog(@"[[[self.availableColoursBuffer objectAtIndex:colorIndex] objectForKey:percentage] floatValue] %f", [[[self.availableColoursBuffer objectAtIndex:colorIndex] objectForKey:kDMProjectColourPercentageKey] floatValue] / 100);
    NSInteger flagsToAdd = floor(counterTarget) - floor([[counters objectAtIndex:colorIndex] floatValue]);
//NSLog(@"flagsToAdd %i", flagsToAdd);
    for (NSInteger flagIndex=0; flagIndex<flagsToAdd; flagIndex++) {
     [self.colorSequence addObject:[NSNumber numberWithInteger:colorIndex]];
    }
    [counters replaceObjectAtIndex:colorIndex withObject:[NSNumber numberWithFloat:counterTarget]];
   }
		} while ([self.colorSequence count] < sequenceLength);
  
  for (NSInteger copyToIndex=[self.availableColoursBuffer count]-1; copyToIndex>0; copyToIndex--) { // randomize sequence order
   NSInteger copyFromIndex = floor(arc4random() % (copyToIndex + 1));
   NSInteger copyBuffer = [[self.colorSequence objectAtIndex:copyToIndex] integerValue];
   [self.colorSequence replaceObjectAtIndex:copyToIndex withObject:[NSNumber numberWithInteger:[[self.colorSequence objectAtIndex:copyFromIndex] integerValue]]];
   [self.colorSequence replaceObjectAtIndex:copyFromIndex withObject:[NSNumber numberWithInteger:copyBuffer]];
  }
  
  NSInteger repeatBlocks = [self countRepeatBlocks:self.colorSequence];
NSLog(@"before: self.colorSequence - %@", self.colorSequence);
NSLog(@"before: repeatBlocks - %ld", (long)repeatBlocks);
  
  if ([self.availableColoursBuffer count] >= 3) { // try to break up blocks (repeats of the same color)
NSLog(@"breaking up blocks");
			NSInteger buffer = -1;
			NSInteger blockStartTarget = -1;
			NSInteger blockStart = -1;
			NSInteger blockEnd = -1;
			NSMutableArray *blocks = [NSMutableArray array];
			for (NSInteger sequenceIndex=0; sequenceIndex<[self.colorSequence count]; sequenceIndex++) {
				if ([[self.colorSequence objectAtIndex:sequenceIndex] integerValue] != buffer) {
					blockStartTarget = sequenceIndex;
					if (blockEnd > blockStart) {
      [blocks addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                         [NSNumber numberWithInteger:blockStart], @"start",
                         [NSNumber numberWithInteger:blockEnd], @"end",
                         nil]];
					}
					blockStart = blockStartTarget;
				} else if (sequenceIndex == [self.colorSequence count] - 1) {
					blockEnd = sequenceIndex;
					if (blockEnd > blockStart) {
      [blocks addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                         [NSNumber numberWithInteger:blockStart], @"start",
                         [NSNumber numberWithInteger:blockEnd], @"end",
                         nil]];
					}
				} else {
					blockEnd = sequenceIndex;
				}
				buffer = [[self.colorSequence objectAtIndex:sequenceIndex] integerValue];
			}
//NSLog(@"blocks - %@", blocks);
   
			for (NSInteger blockIndex=0; blockIndex<[blocks count]; blockIndex++) {
//NSLog(@"x blockIndex - %i", blockIndex);
//NSLog(@"x block start - %i", [[[blocks objectAtIndex:blockIndex] objectForKey:@"start"] integerValue]);
				if ([[[blocks objectAtIndex:blockIndex] objectForKey:@"start"] integerValue] > 0) {
					repeatBlocks = [self countRepeatBlocks:self.colorSequence];//countRepeatBlocks(self.colorSequence);
//NSLog(@"x non-start repeatBlocks - %i", repeatBlocks);
     NSMutableArray *colorSequenceSwapped = [[NSMutableArray alloc] initWithArray:self.colorSequence copyItems:YES]; // duplicate array
					NSInteger copyBuffer = [[colorSequenceSwapped objectAtIndex:[[[blocks objectAtIndex:blockIndex] objectForKey:@"start"] integerValue]] integerValue];
     NSInteger copyInteger = [[colorSequenceSwapped objectAtIndex:[[[blocks objectAtIndex:blockIndex] objectForKey:@"start"] integerValue] - 1] integerValue];
     [colorSequenceSwapped replaceObjectAtIndex:[[[blocks objectAtIndex:blockIndex] objectForKey:@"start"] integerValue] withObject:[NSNumber numberWithInteger:copyInteger]];
     [colorSequenceSwapped replaceObjectAtIndex:[[[blocks objectAtIndex:blockIndex] objectForKey:@"start"] integerValue] - 1 withObject:[NSNumber numberWithInteger:copyBuffer]];
					NSInteger repeatBlocksSwapped = [self countRepeatBlocks:colorSequenceSwapped];//countRepeatBlocks(colorSequenceSwapped);
//NSLog(@"x non-start repeatBlocksSwapped - %i", repeatBlocksSwapped);
					if (repeatBlocksSwapped > repeatBlocks) {
      self.colorSequence = [[NSMutableArray alloc] initWithArray:colorSequenceSwapped copyItems:YES]; // duplicate array
     }
				}
				if ([[[blocks objectAtIndex:blockIndex] objectForKey:@"end"] integerValue] < [self.colorSequence count] - 1) {
//NSLog(@"x non-end repeatBlocks - %i", repeatBlocks);
					repeatBlocks = [self countRepeatBlocks:self.colorSequence];//countRepeatBlocks(self.colorSequence);
     NSMutableArray *colorSequenceSwapped = [[NSMutableArray alloc] initWithArray:self.colorSequence copyItems:YES]; // duplicate array
					NSInteger copyBuffer = [[colorSequenceSwapped objectAtIndex:[[[blocks objectAtIndex:blockIndex] objectForKey:@"end"] integerValue]] integerValue];
     NSInteger copyInteger = [[colorSequenceSwapped objectAtIndex:[[[blocks objectAtIndex:blockIndex] objectForKey:@"end"] integerValue] + 1] integerValue];
     [colorSequenceSwapped replaceObjectAtIndex:[[[blocks objectAtIndex:blockIndex] objectForKey:@"end"] integerValue] withObject:[NSNumber numberWithInteger:copyInteger]];
     [colorSequenceSwapped replaceObjectAtIndex:[[[blocks objectAtIndex:blockIndex] objectForKey:@"end"] integerValue] + 1 withObject:[NSNumber numberWithInteger:copyBuffer]];
					NSInteger repeatBlocksSwapped = [self countRepeatBlocks:colorSequenceSwapped];//countRepeatBlocks(colorSequenceSwapped);
//NSLog(@"x non-end repeatBlocksSwapped - %i", repeatBlocksSwapped);
					if (repeatBlocksSwapped > repeatBlocks) {
      self.colorSequence = [[NSMutableArray alloc] initWithArray:colorSequenceSwapped copyItems:YES]; // duplicate array
     }
				}
			}
NSLog(@"after: self.colorSequence - %@", self.colorSequence);
NSLog(@"after: repeatBlocks - %ld", (long)repeatBlocks);
		}
  
		self.colorSequenceIndex = floor(arc4random() % [self.colorSequence count]);
	}
	self.colorSequenceIndex++;
	if (self.colorSequenceIndex >= [self.colorSequence count]) {self.colorSequenceIndex = 0;}
	return [[self.colorSequence objectAtIndex:self.colorSequenceIndex] integerValue];
}

- (NSInteger) countRepeatBlocks:(NSMutableArray*)sequence {
//NSLog(@"countRepeatBlocks");
	NSInteger repeatBlocks = 0;
	NSInteger buffer = nil;
	for (NSInteger sequenceIndex=0; sequenceIndex<[sequence count]; sequenceIndex++) {
		if ([[sequence objectAtIndex:sequenceIndex] integerValue] != buffer) {
			/*repeatBlocks = [repeatBlocks integerValue] + 1;*/repeatBlocks++;
		}
		buffer = [[sequence objectAtIndex:sequenceIndex] integerValue];
	}
	return repeatBlocks;
}

- (NSInteger) getRandomColor {
//NSLog(@"getRandomColor");
	NSInteger colorIndexTarget = 0;
	BOOL colorAvailable = true;
	NSInteger skyhook;
 do {
  skyhook = arc4random() % 100;
  for (NSInteger colorIndex=0; colorIndex<[self.availableColoursBuffer count]; colorIndex++) {
   if ([[[self.availableColoursBuffer objectAtIndex:colorIndex] objectForKey:@"percentile"] integerValue] > skyhook) {
    colorIndexTarget = colorIndex;
    colorAvailable = true;
    break;
   }
  }
 } while (! colorAvailable);
	return colorIndexTarget;
}

- (NSInteger) getRandomColor:(NSMutableArray*)unavailableColors { // if the function is sent an array of color indices, these colors will be disallowed from the random selection
//NSLog(@"getRandomColor with unavailableColors %@", unavailableColors);
	NSInteger colorIndexTarget = 0;
	BOOL colorAvailable = true;
	NSInteger skyhook;
 do {
//NSLog(@"do");
  skyhook = arc4random() % 100;
//NSLog(@"skyhook %i", skyhook);
  for (NSInteger colorIndex=0; colorIndex<[self.availableColoursBuffer count]; colorIndex++) {
//NSLog(@"colorIndex %i", colorIndex);
   if ([[[self.availableColoursBuffer objectAtIndex:colorIndex] objectForKey:@"percentile"] integerValue] > skyhook) {
    colorIndexTarget = colorIndex;
//NSLog(@"colorIndexTarget %i", colorIndexTarget);
    colorAvailable = true;
    if ([unavailableColors count] > 0) {
     for (NSInteger unavailableColorIndex=0; unavailableColorIndex<[unavailableColors count]; unavailableColorIndex++) {
//NSLog(@"unavailableColorIndex %i; unavailableColor %i", unavailableColorIndex, [[unavailableColors objectAtIndex:unavailableColorIndex] integerValue]);
      if (colorIndexTarget == [[unavailableColors objectAtIndex:unavailableColorIndex] integerValue]) {
       colorAvailable = false;
       break;
      }
     }
    }
    break;
   }
  }
 } while (! colorAvailable);
	return colorIndexTarget;
}

- (void) clearPattern {
 //	$("div.pattern-preview").html("");
 self.maximumLeft = 0;
 self.minimumWidth = 0;
 self.courseWidth = 0.0f;
 [self.usedFlags  removeAllObjects];
 [self.courseFlags removeAllObjects];
 [self.patternHTML setString:@""];
 [self.courseHTML setString:@"<div>"];
}

- (void) buildNewCourse {
 //	$("div.pattern-preview").append("<br />");
 self.calculatedWidth = MAX(self.calculatedWidth, self.courseWidth);
 if (self.minimumWidth == 0) {
  self.minimumWidth = self.courseWidth;
 } else {
  self.minimumWidth = MIN(self.minimumWidth, self.courseWidth);
 }
 [self.usedFlags addObjectsFromArray:self.courseFlags];
 [self.courseFlags removeAllObjects];
 [self.patternHTML appendString:self.courseHTML];
 [self.patternHTML appendFormat:@"</div>\n"];
 [self.courseHTML setString:@"<div>"];
 self.courseWidth = 0;
}

- (void) rebuildCourse { // remove all flags on the current course; start again
	NSLog(@"rebuildCourse");
 //	while ($("div.pattern-preview").children().last().is("img")) {
 //		$("div.pattern-preview").children().last().remove();
 //	}
 [self.courseFlags removeAllObjects];
 [self.courseHTML setString:@"<div>"];
 self.courseWidth = 0.0f;
}



@end
