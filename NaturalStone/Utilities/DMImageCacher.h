//
//  DMImageCacher.h
//  NaturalStone
//
//  Created by John McKerrell on 14/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ASIFormDataRequest.h"

#define kDMImageSyncCompletedNotification @"kDMImageSyncCompletedNotification"
//#define kDMImageUploadURL @"http://richardlaptop.local/naturalstone-svc/ProjectImage.ashx"
//#define kDMHTMLUploadURL @"http://richardlaptop.local/naturalstone-svc/projectHtml.ashx"
#define kDMImageUploadURL @"http://www.marshalls.co.uk/NaturalStoneSvc/ProjectImage.ashx"
#define kDMHTMLUploadURL @"http://www.marshalls.co.uk/NaturalStoneSvc/projectHtml.ashx"

@interface DMImageCacher : NSObject <ASIHTTPRequestDelegate>

- (BOOL)syncing;
- (void)beginSync;

+ (DMImageCacher*)sharedInstance;

@end
