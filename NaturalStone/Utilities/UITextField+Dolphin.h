//
//  UITextField+Dolphin.h
//  NaturalStone
//
//  Created by John McKerrell on 15/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Dolphin)

- (void) fixForDolphin;

@end
