
//
//  DMImageCacher.m
//  NaturalStone
//
//  Created by John McKerrell on 14/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMImageCacher.h"

#import "JSONKit.h"
#import "NSString+Base64.h"

static DMImageCacher *sharedInstance = nil;


@interface DMImageCacher ()

// Handled images are either uploads (thumbnail won't be set until next sync) or requests which fail
@property (nonatomic,strong) NSMutableSet *handledImages;
@property (nonatomic,assign) BOOL syncing;
@property (nonatomic,strong) DMProject *activeProject;
@property (nonatomic,strong) NSString* activeRequestType;
@property (nonatomic,assign) BOOL activeRequestIsDownload;
@property (nonatomic,strong) NSArray *projectLibrary;
@property (nonatomic,strong) NSString *authToken;

- (void)syncNextFile;

@end

@implementation DMImageCacher

@synthesize handledImages = __handledImages;
@synthesize syncing = __syncing;
@synthesize activeProject = __activeProject;
@synthesize activeRequestType = __activeRequestType;
@synthesize activeRequestIsDownload = __activeRequestIsDownload;

- (void)beginSync {
    @synchronized(self) {
        if (self.syncing) {
            return;
        }
        self.syncing = YES;
    }
    self.handledImages = [NSMutableSet set];
    self.projectLibrary = kDMNavigationControllerRef.projectLibrary;
    self.authToken = [kDMNavigationControllerRef authToken];
    [self syncNextFile];
}

- (void)syncNextFile {
    if ([NSThread isMainThread]) {
        [self performSelectorInBackground:@selector(syncNextFile) withObject:nil];
        return;
    }
    @autoreleasepool {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSArray *projectLibrary = self.projectLibrary;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        for (DMProject *project in projectLibrary) {
            //NSLog(@"clientprojectid=%@", [project.properties objectForKey:kDMProjectPropertyClientID]);
            NSString *filename = [project pathForImage:kDMProjectImageLarge];
            if ([self.handledImages containsObject:filename]) {
                // Ignore images that have been handled already
            } else if (![project projectImageHasBeenSynced:kDMProjectImageLarge] && project.synced) {
                // We have a file locally and we have an ID
                // Upload this file
                self.activeProject = project;
                self.activeRequestType = kDMProjectImageLarge;
                self.activeRequestIsDownload = NO;
                
                NSData *imageData = [NSData dataWithContentsOfFile:filename];
                NSData *htmlData;
                if ([project.type isEqualToString:@"Stonespar" ]){
                    NSString *str = @"<Html><Header></Header><Body>Helllo</Body></Html>";
                    htmlData = [str dataUsingEncoding:NSUTF8StringEncoding];
                }else{
                    htmlData = [NSData dataWithContentsOfFile:[project pathForImage:kDMProjectImageHTML]];
                }
                NSDictionary *jsonDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [project.properties objectForKey:kDMProjectPropertyClientID], kDMProjectPropertyClientID,
                                          [NSString encodeBase64WithData:imageData],
                                          kDMProjectImageFileContentKey,
                                          [NSString encodeBase64WithData:htmlData],
                                          kDMProjectHTMLFileContentKey,
                                          nil];
                NSMutableDictionary *headers = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                self.authToken, @"Authentication-Token",
                                                @"text/json", @"Accept",
                                                @"text/json", @"Content-Type",
                                                nil];
                ASIFormDataRequest *uploadRequest = [[ASIFormDataRequest alloc]
                                                     initWithURL:[NSURL
                                                                  URLWithString:[NSString stringWithFormat:@"%@%@",
                                                                                 [userDefaults objectForKey:(NSString*)kDefaultAPIBaseKey],
                                                                                 @"ProjectFiles"]]];
                uploadRequest.requestMethod = @"POST";
                [uploadRequest setRequestHeaders:headers];
                [uploadRequest setPostBody:[[jsonDict JSONData] mutableCopy]];
                //            [uploadRequest addRequestHeader:@"clientProjectId" value:];
                //            [uploadRequest setPostBody:imageData];
                uploadRequest.delegate = self;
                [uploadRequest startAsynchronous];
                 uploadRequest = nil;
                
                return;
                /*
                 } else if (![project projectImageHasBeenSynced:kDMProjectImageHTML] && project.synced) {
                 // We have a file locally and we have an ID
                 // Upload this file
                 self.activeProject = project;
                 self.activeRequestType = kDMProjectImageHTML;
                 self.activeRequestIsDownload = NO;
                 
                 filename = [project pathForImage:kDMProjectImageHTML];
                 //            NSMutableData *imageData = [NSMutableData dataWithContentsOfFile:filename];
                 ASIFormDataRequest *uploadRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:kDMHTMLUploadURL]];
                 uploadRequest.requestMethod = @"POST";
                 [uploadRequest setPostValue:[NSString stringWithFormat:@"%@",[self.activeProject.properties objectForKey:kDMProjectPropertyClientID]] forKey:@"clientprojectid"];
                 [uploadRequest setFile:filename withFileName:[filename lastPathComponent] andContentType:@"text/html" forKey:@"patternPreviewHTML"];
                 //            [uploadRequest addRequestHeader:@"clientProjectId" value:];
                 //            [uploadRequest setPostBody:imageData];
                 uploadRequest.delegate = self;
                 [uploadRequest startAsynchronous];
                 [uploadRequest release]; uploadRequest = nil;
                 
                 return;
                 */
                //        } else if (![fileManager fileExistsAtPath:filename] && [project.properties objectForKey:kDMProjectPropertyLargeURL] != [NSNull null] && [[project.properties objectForKey:kDMProjectPropertyLargeURL] length] > 0) {
                //            // We have no file locally and the server has assigned it a URL
                //            // Download the file
                //            self.activeProject = project;
                //
                //            self.activeRequestType = kDMProjectImageLarge;
                //            self.activeRequestIsDownload = YES;
                //
                //            ASIHTTPRequest *downloadRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:[project.properties objectForKey:kDMProjectPropertyLargeURL]]];
                //            downloadRequest.delegate = self;
                //            [downloadRequest startAsynchronous];
                //            [downloadRequest release]; downloadRequest = nil;
                //
                //            return;
                
            } else if (![fileManager fileExistsAtPath:[project pathForImage:kDMProjectImageThumb]] && [project.properties objectForKey:kDMProjectPropertyThumbnailURL] != [NSNull null] && [[project.properties objectForKey:kDMProjectPropertyThumbnailURL] length] > 0) {
                // We have no thumbnail locally and the server has assigned it a URL
                // Download the file
                self.activeProject = project;
                
                self.activeRequestType = kDMProjectImageThumb;
                self.activeRequestIsDownload = YES;
                
                ASIHTTPRequest *downloadRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:[project.properties objectForKey:kDMProjectPropertyThumbnailURL]]];
                downloadRequest.delegate = self;
                [downloadRequest startAsynchronous];
                 downloadRequest = nil;
                
                return;
            } else if (![fileManager fileExistsAtPath:[project pathForImage:kDMProjectImageHTML]] && [project.properties objectForKey:kDMProjectPropertyHTMLURL] != [NSNull null] && [[project.properties objectForKey:kDMProjectPropertyHTMLURL] length] > 0) {
                // We have no thumbnail locally and the server has assigned it a URL
                // Download the file
                self.activeProject = project;
                
                self.activeRequestType = kDMProjectImageHTML;
                self.activeRequestIsDownload = YES;
                
                ASIHTTPRequest *downloadRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:[project.properties objectForKey:kDMProjectPropertyHTMLURL]]];
                downloadRequest.delegate = self;
                [downloadRequest startAsynchronous];
                 downloadRequest = nil;
                
                return;
            }
        }
        
        // If we've got this far we're all done
        @synchronized(self) {
            self.syncing = NO;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kDMImageSyncCompletedNotification object:self];
        });
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    NSLog(@"finished");
    if (request.responseStatusCode >= 200 && request.responseStatusCode <= 300) {
        if (!self.activeRequestIsDownload) {
            [[NSFileManager defaultManager] removeItemAtPath:[self.activeProject pathForUnsentImageMarker:self.activeRequestType] error:nil];
            [self.handledImages addObject:[self.activeProject pathForImage:NO]];
        } else {
            NSString *filename = [self.activeProject pathForImage:self.activeRequestType];
            [request.responseData writeToFile:filename atomically:YES];
        }
        self.activeProject = nil;
        [self syncNextFile];
    } else {
        NSLog(@"responseString=%@", request.responseString);
        [self requestFailed:request];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSLog(@"failed");
    [self.handledImages addObject:[self.activeProject pathForImage:kDMProjectImageLarge]];
    
    [self syncNextFile];
}

#pragma mark - Singleton methods

+ (DMImageCacher*)sharedInstance
{
    @synchronized(self)
    {
        if (sharedInstance == nil)
            sharedInstance = [[DMImageCacher alloc] init];
    }
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;  // assignment and return on first allocation
        }
    }
    return nil; // on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

/*- (id)retain {
    return self;
}

- (unsigned)retainCount {
    return UINT_MAX;  // denotes an object that cannot be released
}

- (void)release {
    //do nothing
}

- (id)autorelease {
    return self;
}*/

@end
