//
//  DMProject.h
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kDMProjectPropertyTitle @"Title"
#define kDMProjectPropertyLocation @"Location"
#define kDMProjectPropertyDate @"ProjectDate"
#define kDMProjectPropertyClientID @"ClientId"
#define kDMProjectPropertyUser @"OwnerDeviceId"
#define kDMProjectPropertyLargeURL @"LargeImageUrl"
#define kDMProjectPropertyThumbnailURL @"ThumbnailImageUrl"
#define kDMProjectPropertyHTMLURL @"HtmlUrl"
#define kDMProjectPropertyColours @"ProjectColours"
#define kDMProjectPropertySizeMixKey @"ProjectSizeMixes"
#define kDMProjectTypeName @"ProjectTypeName"
#define kDMProjectPatternId @"PatternId"
#define kDMProjectQuotesKey @"ProjectItems"
#define kDMProjectPropertyArea @"Area"
#define kDMProjectPropertyThickness @"Thickness"

#define kDMProjectHTMLFileContentKey @"HtmlFileContent"
#define kDMProjectImageFileContentKey @"ImageFileContent"

#define kDMProjectColourReferenceKey @"Reference"
#define kDMProjectColourPercentageKey @"Percentage"
#define kDMProjectItemColourReferenceKey @"ColourReference"

#define kDMProjectSizeMixPercentageArea @"AreaPercentage"
#define kDMProjectSizeMixPercentageFlags @"FlagPercentage"
#define kDMProjectSizeMixWidth @"Width"
#define kDMProjectSizeMixLength @"Length"

#define kDMProjectOrderQuantity @"Quantity"

#define kDMProjectMaxColours 5

#define kDMProjectFlagTypeKey @"FlagType"
#define kDMProjectFlagTypeSett @"Setts"
#define kDMProjectFlagTypePaving @"Paving"

#define kDMProjectImageLarge @"full"
#define kDMProjectImageThumb @"thumb"
#define kDMProjectImageHTML @"html"

@interface DMProject : NSObject

@property (nonatomic, strong) NSMutableDictionary *properties;
@property (nonatomic, strong) NSMutableArray *colours;
@property (nonatomic, strong) NSMutableDictionary *sizeMix;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSMutableArray *quotes;
@property (nonatomic,assign) BOOL saved;
@property (nonatomic,assign) BOOL synced;

+ (NSArray*)coloursData;
+ (NSDictionary*)coloursDataAsDictionary;
+ (NSDictionary*)sizesData;

+ (DMProject*)projectFromJSONDictionary:(NSDictionary*)jsonDictionary;

- (BOOL)isTitleAndLocationValid;
- (NSDictionary*)fullDictionaryForJSON;
- (NSDictionary*)fullDictionaryForStonesparJSON;

- (NSString*)pathForImage:(NSString*)type;
- (UIImage*)largeImage;
- (UIImage*)thumbnailImage;
- (void)deleteImages;
- (void)patternPreview;
- (NSString*)patternHTML;
- (BOOL)projectImageHasBeenSynced:(NSString*)type;
- (NSString*)pathForUnsentImageMarker:(NSString*) type;
- (BOOL)projectHasImages;
- (void)setProjectImage:(UIImage*)image;
- (void)swapOldColours;

@end
