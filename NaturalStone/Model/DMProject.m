//
//  DMProject.m
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMProject.h"

#import "JSONKit.h"
#import "DMPatternGenerator.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Resize.h"

@implementation DMProject

@synthesize properties = __properties;
@synthesize colours = __colours;
@synthesize sizeMix = __sizeMix;
@synthesize quotes = __quotes;
@synthesize saved = __saved;
@synthesize type;

+ (NSArray*)coloursData {
    return [[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Colours" ofType:@"json"]] objectFromJSONData];
}
+ (NSDictionary*)coloursDataAsDictionary {
    NSArray *coloursData = [DMProject coloursData];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:[coloursData count]];
    for (NSDictionary *colourInfo in coloursData) {
        [dictionary setObject:colourInfo forKey:[colourInfo objectForKey:@"ref"]];
    }
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
+ (NSDictionary*)sizesData {
    return [[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Sizes" ofType:@"json"]] objectFromJSONData];
}

+ (DMProject*)projectFromJSONDictionary:(NSDictionary*)jsonDictionary {
    NSMutableDictionary *dict = [jsonDictionary mutableCopy];
    NSString *str = [dict objectForKey:kDMProjectTypeName];
    DMProject *project = [[DMProject alloc] init];
    
    if (str == nil || [str isEqualToString:@"Granite"]){
        [project.sizeMix setObject:[dict objectForKey:kDMProjectFlagTypeKey] forKey:kDMProjectFlagTypeKey];
        [dict removeObjectForKey:kDMProjectFlagTypeKey];
        if ([kDMProjectFlagTypePaving isEqualToString:[project.sizeMix objectForKey:kDMProjectFlagTypeKey]]) {
            [project.sizeMix setObject:[dict objectForKey:kDMProjectPropertySizeMixKey] forKey:@"pavingSizes"];
        } else {
            [project.sizeMix setObject:[dict objectForKey:kDMProjectPropertySizeMixKey] forKey:@"settSizes"];
        }
        [dict removeObjectForKey:kDMProjectPropertySizeMixKey];
        project.quotes = [dict objectForKey:kDMProjectQuotesKey];
        [dict removeObjectForKey:kDMProjectQuotesKey];
    }else if ([str isEqualToString:@"Stonespar"]){
        NSLog(@"DMProject projectFromJSONDictionary Stonespar project");
    };
    
    project.colours = [dict objectForKey:kDMProjectPropertyColours];
    [dict removeObjectForKey:kDMProjectPropertyColours];
    
    project.type = [dict objectForKey:kDMProjectTypeName];
    [dict removeObjectForKey:kDMProjectTypeName];
    
    project.synced = YES;
    NSNumber *unsynced = [dict objectForKey:@"unsynced"];
    if (unsynced) {
        if ([unsynced boolValue]) {
            project.synced = NO;
        }
        [dict removeObjectForKey:@"unsynced"];
    }
    
    project.properties = dict;
    project.saved = YES;
    dict = nil;
    
    return project;
}

- (id)init {
    if ((self = [super init])) {
        // Create universally unique identifier (object)
        CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
        
        // Get the string representation of CFUUID object.
        NSString *uuidStr = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject));
        
        // If needed, here is how to get a representation in bytes, returned as a structure
        // typedef struct {
        //   UInt8 byte0;
        //   UInt8 byte1;
        //   ...
        //   UInt8 byte15;
        // } CFUUIDBytes;
        
        CFRelease(uuidObject);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterFullStyle];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        NSString *ownerDeviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        //NSString *ownerDeviceId = [[NSUUID UUID] UUIDString];
//        dateFormatter.dateStyle = NSDateFormatterShortStyle;
//        [dateFormatter setDateFormat:@"dd/MM/yyyy"];// 10/04/2012
        NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
         dateFormatter = nil;
        self.properties = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           uuidStr, kDMProjectPropertyClientID,
                           dateString, kDMProjectPropertyDate,
                           [NSNumber numberWithFloat:100.0f], kDMProjectPropertyArea,
                           [NSNumber numberWithFloat:30.0f], kDMProjectPropertyThickness,
                           ownerDeviceId, kDMProjectPropertyUser,
                           nil];
        self.colours = [NSMutableArray arrayWithCapacity:kDMProjectMaxColours];
        self.sizeMix = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                        kDMProjectFlagTypeSett, kDMProjectFlagTypeKey,
                        [NSMutableDictionary dictionary], @"settSizes",
                        [NSMutableDictionary dictionary], @"pavingSizes",
                        nil];
        self.quotes = [NSMutableArray arrayWithCapacity:5];
    }
    return self;
}

- (BOOL)isTitleAndLocationValid {
    return ([[self.properties objectForKey:kDMProjectPropertyTitle] length] && [[self.properties objectForKey:kDMProjectPropertyLocation] length]);
}

- (NSArray*)addType:(NSString*)type forDictionariesInArray:(NSArray*)existingArray {
    return existingArray;
}
/**
 * This should return a dictionary in the format required by the API.
 */
- (NSDictionary*)fullDictionaryForJSON {
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionaryWithDictionary:self.properties];
    [jsonDict setObject:[self.sizeMix objectForKey:kDMProjectFlagTypeKey] forKey:kDMProjectFlagTypeKey];

    NSArray *existingSizeMix = [self.sizeMix objectForKey:@"settSizes"];
    if ([kDMProjectFlagTypePaving isEqualToString:[jsonDict objectForKey:kDMProjectFlagTypeKey]]) {
        existingSizeMix = [self.sizeMix objectForKey:@"pavingSizes"];
    }
    
    NSMutableArray *sizeMixes = [NSMutableArray arrayWithCapacity:[existingSizeMix count]];
    for (__strong NSMutableDictionary *sizeMix in existingSizeMix) {
        sizeMix = [sizeMix mutableCopy];
        [sizeMix removeObjectForKey:@"flagsUsed"];
        [sizeMix removeObjectForKey:@"percentageFlagsUsed"];
        [sizeMix setObject:[NSNumber numberWithInteger:[[sizeMix objectForKey:kDMProjectSizeMixPercentageArea] integerValue]] forKey:kDMProjectSizeMixPercentageArea];
        [sizeMixes addObject:sizeMix];
         sizeMix = nil;
    }
    
    [jsonDict setObject:[self addType:@"SizeMix:#MarshallsWcf" forDictionariesInArray:[NSArray arrayWithArray:sizeMixes]] forKey:kDMProjectPropertySizeMixKey];
    
    [jsonDict setObject:[self addType:@"Colours:#MarshallsWcf" forDictionariesInArray:self.colours] forKey:kDMProjectPropertyColours];

    [jsonDict setObject:[self addType:@"Quote:#MarshallsWcf" forDictionariesInArray:self.quotes] forKey:kDMProjectQuotesKey];
    
    return jsonDict;
}

/**
 * This should return a dictionary in the format required by the API.
 */
- (NSDictionary*)fullDictionaryForStonesparJSON {
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionaryWithDictionary:self.properties];
    [jsonDict setObject:@"Paving" forKey:kDMProjectFlagTypeKey];
    [jsonDict setObject:@"Stonespar" forKey:kDMProjectTypeName];
    [jsonDict setObject:[self addType:@"ProjectColours" forDictionariesInArray:self.colours] forKey:kDMProjectPropertyColours];

    return jsonDict;
}


- (DMProject*)mutableCopy {
    DMProject *newProject = [[DMProject alloc] init];
    NSMutableDictionary *existingProperties = [self.properties mutableCopy];
    [existingProperties removeObjectForKey:kDMProjectPropertyClientID];
    [existingProperties removeObjectForKey:kDMProjectPropertyUser];
    [existingProperties removeObjectForKey:kDMProjectPropertyDate];
    [existingProperties removeObjectForKey:kDMProjectPropertyLargeURL];
    [existingProperties removeObjectForKey:kDMProjectPropertyThumbnailURL];
    [newProject.properties addEntriesFromDictionary:existingProperties];
    for (NSMutableDictionary *colour in self.colours) {
        [newProject.colours addObject:[colour mutableCopy]];
    }
    for (NSString *key in self.sizeMix) {
        id value = [self.sizeMix objectForKey:key];
        if ([value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSDictionary class]]) {
            [newProject.sizeMix setObject:[value mutableCopy] forKey:key];
        } else {
            [newProject.sizeMix setObject:value forKey:key];
        }
    }
    // New quotes will depend on the new pattern
//    [newProject.quotes setArray:[[self.quotes mutableCopy] autorelease]];
    
    return newProject;
}

-(void) swapOldColours{
    NSArray *projectColours = DMProject.coloursData;
    NSMutableArray *newColours = [self.colours mutableCopy];
    bool changed = false;
    for (int i=0; i<[self.colours count]; i++) {
        NSMutableDictionary* colour = [[self.colours objectAtIndex:i] mutableCopy];
        NSString *ref = [colour objectForKey:@"Reference"];
        if ([ref hasPrefix:@"gra9"]){
            for (NSDictionary *projectColour in projectColours) {
                NSString *ref1 = [projectColour objectForKey:@"ref"];
                NSNumber *num = [projectColour objectForKey:@"discontinued"];
                if (ref1!=NULL && ([ref1 isEqualToString:ref] && num==NULL)){
                    NSString *name = [projectColour objectForKey:@"name"];
                    [colour setObject:[name lowercaseString] forKey:@"Reference"];
                    [newColours replaceObjectAtIndex:i withObject:colour];
                    changed = true;
                    break;
                }
            }
        }
    }
    if (changed) self.colours = newColours;
}

- (NSString*)pathForImage:(NSString*)type {
    NSString *privateDocumentsPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Library"] stringByAppendingPathComponent:@"Private Documents"];
    NSString *imagesPath = [privateDocumentsPath stringByAppendingPathComponent:@"PreviewImages"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:imagesPath]) {
        [fileManager createDirectoryAtPath:imagesPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *imagePath = [imagesPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@.%@", 
                                                                      [self.properties objectForKey:kDMProjectPropertyClientID], 
                                                                      type,
                                                                      ([type isEqualToString:kDMProjectImageHTML]?@"html":@"png")
                                                                      ]];
    return imagePath;
}

- (NSString*)pathForUnsentImageMarker:(NSString*) type {
    return [[self pathForImage:type] stringByAppendingPathExtension:@"unsent"];
}

- (NSString*)patternHTML {
    NSString *filename = [self pathForImage:kDMProjectImageHTML];
    return [NSString stringWithContentsOfFile:filename encoding:NSUTF8StringEncoding error:nil];
}

- (BOOL)projectImageHasBeenSynced:(NSString*)type {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return ![fileManager fileExistsAtPath:[self pathForUnsentImageMarker:type]];
}

- (UIImage*)getImage:(BOOL)thumbnail {
    return [UIImage imageWithContentsOfFile:[self pathForImage:kDMProjectImageThumb]];
}

- (UIImage*)largeImage {
    return [self getImage:NO];
}

- (UIImage*)thumbnailImage {
    return [self getImage:YES];
}

- (void)deleteImages {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:[self pathForImage:kDMProjectImageLarge] error:nil];
    [fileManager removeItemAtPath:[self pathForImage:kDMProjectImageThumb] error:nil];
    [fileManager removeItemAtPath:[self pathForImage:kDMProjectImageHTML] error:nil];
}

- (void)patternPreview {
    NSString *type = [self.properties objectForKey:kDMProjectTypeName];
    if ([type isEqualToString:@"Stonespar"]) return;
    
    DMPatternGenerator *patternGenerator = [[DMPatternGenerator alloc] initWithProject:self];
    
    [patternGenerator generatePattern];
    
    NSMutableArray *quotes = [NSMutableArray array];
    
    // Get the total paving area in square metres [self.properties objectForKey:kDMProjectPropertyArea]
    CGFloat area = [[self.properties objectForKey:kDMProjectPropertyArea] floatValue];
    NSArray *existingSizeMix = [self.sizeMix objectForKey:@"settSizes"];
    if ([kDMProjectFlagTypePaving isEqualToString:[self.sizeMix objectForKey:kDMProjectFlagTypeKey]]) {
        existingSizeMix = [self.sizeMix objectForKey:@"pavingSizes"];
    }
    // For each flag size in self.sizeMix
    for (NSDictionary *flagSize in existingSizeMix) {
        //multiply its percentArea by the total paving area then divide by 100.  Then divide this by the flag area in square metres (width * length / 1000000) to get the number of flags of this size.
        CGFloat flagArea = ( ([[flagSize objectForKey:kDMProjectSizeMixLength] floatValue] * [[flagSize objectForKey:kDMProjectSizeMixWidth] floatValue]) / 1000000.0f );
        CGFloat numFlags = ceilf(( ([[flagSize objectForKey:kDMProjectSizeMixPercentageArea] floatValue] * area) / 100.0f ) / flagArea);
        CGFloat totalNumFlags = numFlags;
        
        // For each flag size, take the total number of flags just calculated, and share it out between the colours in self.colours, according to the percentage of each colour.
        for (int i = 0, l = [self.colours count], last = l-1; i < l; ++i) {
            NSDictionary *colour = [self.colours objectAtIndex:i];
            CGFloat numColourFlags = numFlags;
            // The last colour will just use the remainder left in numFlags, previous colours will be calculated
            if (i < last) {
                numColourFlags = roundf(([[colour objectForKey:kDMProjectColourPercentageKey] floatValue] * totalNumFlags) / 100.0f);
                numFlags -= numColourFlags;
            }
            // All you need to do is get this data into self.quotes in the same way as the old data was.
            [quotes addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                               [colour objectForKey:kDMProjectColourReferenceKey], kDMProjectItemColourReferenceKey,
                               [flagSize objectForKey:kDMProjectSizeMixLength], kDMProjectSizeMixLength,
                               [flagSize objectForKey:kDMProjectSizeMixWidth], kDMProjectSizeMixWidth,
                               [NSNumber numberWithFloat:numColourFlags], kDMProjectOrderQuantity,
                               nil]];
        }
    }
    
//    [self.quotes setArray:[patternGenerator quoteDetails]];
    self.quotes = [NSArray arrayWithArray:quotes];
        
//    CALayer *patternPreview = [patternGenerator patternPreviewWrapper];
//    UIGraphicsBeginImageContext(CGSizeMake(256, 256));
//    [patternPreview renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *thumbNailImage = UIGraphicsGetImageFromCurrentImageContext();
//    
//    UIGraphicsEndImageContext();
//
//    NSData *thumbnailData = UIImagePNGRepresentation(thumbNailImage);
//    [thumbnailData writeToFile:[self pathForImage:YES] atomically:YES];
//    
//    UIGraphicsBeginImageContext([patternPreview frame].size);
//    [patternPreview renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
//    
//    UIGraphicsEndImageContext();
//
//    NSData *imageData = UIImagePNGRepresentation(outputImage);
//    [imageData writeToFile:[self pathForImage:NO] atomically:YES];
//    
//    [patternGenerator release]; patternGenerator = nil;
//    
    
    NSString *filename = [self pathForImage:kDMProjectImageHTML];
    [[patternGenerator patternHTML] writeToFile:filename atomically:YES encoding:NSUTF8StringEncoding error:nil];

    // Save a dummy file to say that the file hasn't been pushed to the server
    [[NSData data] writeToFile:[self pathForUnsentImageMarker:kDMProjectImageHTML] atomically:YES];
    
     patternGenerator = nil;

}

- (BOOL)projectHasImages {
    return [[NSFileManager defaultManager] fileExistsAtPath:[self pathForImage:kDMProjectImageThumb]];
}

- (void)setProjectImage:(UIImage*)image {
    UIImage *largeImage = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFill
                                                      bounds:CGSizeMake(516, 411)
                                        interpolationQuality:kCGInterpolationHigh];
    NSData *imageData = UIImagePNGRepresentation(largeImage);
    [imageData writeToFile:[self pathForImage:kDMProjectImageLarge] atomically:YES];
    
    UIImage *resized = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFill 
                                                   bounds:CGSizeMake(256.0f, 256.0f) 
                                     interpolationQuality:kCGInterpolationHigh];
    
    imageData = UIImagePNGRepresentation(resized);
    [imageData writeToFile:[self pathForImage:kDMProjectImageThumb] atomically:YES];
    
    // Save a dummy file to say that the file hasn't been pushed to the server
    [[NSData data] writeToFile:[self pathForUnsentImageMarker:kDMProjectImageLarge] atomically:YES];

}

-(NSString*)description {
    NSString *description = [super description];
    return [NSString stringWithFormat:@"%@ Title=\"%@\" Location=\"%@\" synced=%@ saved=%@",
            [description substringToIndex:([description length]-1)],
            [self.properties objectForKey:kDMProjectPropertyTitle],
            [self.properties objectForKey:kDMProjectPropertyLocation],
            self.synced ? @"YES" : @"NO",
            self.saved ? @"YES" : @"NO"
            ];
}

@end
