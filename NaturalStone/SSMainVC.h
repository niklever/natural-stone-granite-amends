//
//  SSMainVC.h
//  NaturalStone
//
//  Created by Nik Lever on 28/04/2014.
//
//

#import <UIKit/UIKit.h>

@class SSProportionsVC;
@class DMNavigationController;

@interface SSMainVC : UIViewController<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *left_btn;
@property (weak, nonatomic) IBOutlet UILabel *title_lbl;
@property (weak, nonatomic) IBOutlet UICollectionView *data_cv;
@property (weak, nonatomic) IBOutlet UIView *panel_bg;
@property (strong, nonatomic) SSMenuVC *menu_vc;
@property (weak, nonatomic) IBOutlet UIButton *createmix_btn;
@property (strong, nonatomic) SSProportionsVC *proportions_vc;
@property (weak, nonatomic) DMNavigationController *delegate;

- (IBAction)leftPressed:(id)sender;
- (IBAction)createMixPressed:(id)sender;

@end
