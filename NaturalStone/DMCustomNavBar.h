//
//  DMCustomNavBar.h
//  NaturalStone
//
//  Created by John McKerrell on 12/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    DMNavigationNoButtons = 0,
    DMNavigationHomeButton = 1,
    DMNavigationBackButton = 2,
    DMNavigationForwardButton = 4,
    DMNavigationSaveButton = 8,
    DMNavigationSearchButton = 16,
    DMNavigationExportButton = 32,
    DMNavigationLibraryButton = 64,
    DMNavigationProjectFilter = 128,
} DMNavigationButtons;

typedef enum {
    DMTransitionLeftToRight,
    DMTransitionRightToLeft
} DMTransitions;

@class DMCustomNavBar;

@protocol DMCustomNavBarSearchDelegate <NSObject>

- (void)customNavBar:(DMCustomNavBar*)navBar searchTextUpdated:(NSString*)searchText;
- (void)customNavBarSearchCancelled:(DMCustomNavBar*)navBar;
- (void)customNavBarFilterByType:(NSString *)type;

@end

@interface DMCustomNavBar : UIView

- (void)setButtonsState:(DMNavigationButtons)buttonsState;
- (void)setButtonsState:(DMNavigationButtons)buttonsState animated:(BOOL)animated;
- (void)hideNavigationBarAnimated:(BOOL)animated;
- (void)showNavigationBarAnimated:(BOOL)animated;
- (void)hideNavigationBar;
- (void)showNavigationBar;
- (void)updateForViewController:(UIViewController*)viewController transitionDirection:(DMTransitions)transitionDirection buttonStates:(DMNavigationButtons)buttonStates;

@property (nonatomic,weak) id<DMCustomNavBarSearchDelegate> searchDelegate;
@property (nonatomic,strong) UISegmentedControl *projectFilter;


@end
