//
//  SSProportionsBVC.h
//  NaturalStone
//
//  Created by Nik Lever on 30/04/2014.
//
//

#import <UIKit/UIKit.h>

@interface SSProportionsBVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *mix1_img;
@property (weak, nonatomic) IBOutlet UIImageView *mix2_img;
@property (weak, nonatomic) IBOutlet UIImageView *mix3_img;
@property (weak, nonatomic) IBOutlet UIImageView *mix4_img;
@property (weak, nonatomic) IBOutlet UIImageView *mix5_img;
@property (weak, nonatomic) IBOutlet UIImageView *mix6_img;
@property (weak, nonatomic) IBOutlet UIImageView *drag1_img;
@property (weak, nonatomic) IBOutlet UIImageView *drag2_img;
@property (weak, nonatomic) IBOutlet UIImageView *drag3_img;
@property (weak, nonatomic) IBOutlet UIImageView *drag4_img;
@property (weak, nonatomic) IBOutlet UIImageView *drag5_img;
@property (weak, nonatomic) IBOutlet UILabel *mix1_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mix2_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mix3_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mix4_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mix5_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mix6_lbl;
@property (readwrite) BOOL modified;

-(void)setColours:(NSArray *)_colours;
-(NSArray *)getColours;

@end
