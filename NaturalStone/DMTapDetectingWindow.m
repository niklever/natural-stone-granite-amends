//
//  DMTapDetectingWindow.m
//  NaturalStone
//
//  Created by John McKerrell on 18/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMTapDetectingWindow.h"

@interface DMTapDetectingWindow ()

@property (nonatomic,assign) BOOL touchMoved;

@end

@implementation DMTapDetectingWindow

@synthesize viewToObserve = __viewToObserve;
@synthesize controllerThatObserves = __controllerThatObserves;
@synthesize touchMoved = __touchMoved;

- (void)dealloc {
    self.viewToObserve = nil;
    self.controllerThatObserves = nil;
    
}

- (void)sendEvent:(UIEvent *)event {
    [super sendEvent:event];
    if (self.viewToObserve == nil || self.controllerThatObserves == nil)
        return;
    NSSet *touches = [event allTouches];
    UITouch *touch = touches.anyObject;
    if ([touch.view isDescendantOfView:self.viewToObserve] == NO)
        return;
    if (touch.phase == UITouchPhaseBegan) {
        self.touchMoved = NO;
    } else if (touch.phase == UITouchPhaseMoved) {
        self.touchMoved = YES;
        [self.controllerThatObserves userDidMoveWebView];
    } else if (touch.phase == UITouchPhaseEnded) {
        if (!self.touchMoved) {
            [self.controllerThatObserves userDidTapWebView];
        }
    }
}
@end
