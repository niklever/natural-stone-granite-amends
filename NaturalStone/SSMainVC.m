//
//  SSMainVC.m
//  NaturalStone
//
//  Created by Nik Lever on 28/04/2014.
//
//

#import "SSMainVC.h"
#import "SSMenuVC.h"
#import "SSPatternCell.h"
#import "SSMixesCell.h"
#import "SSColourCell.h"
#import "SSProportionsVC.h"
#import "DMNavigationController.h"
#import <QuartzCore/QuartzCore.h>

enum { _SP_MIX=1, _SP_COLOURS, _SP_PROPORTIONS, _SP_PATTERN };

@interface SSMainVC (){
    int step;
    int patternIdx;
    int mixIdx;
    bool colours[6];
    int premix_colours[6][6];
}

@end

@implementation SSMainVC

@synthesize panel_bg;
@synthesize title_lbl;
@synthesize data_cv;
@synthesize left_btn;
@synthesize menu_vc;
@synthesize createmix_btn;
@synthesize proportions_vc;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        step = _SP_MIX;
        patternIdx = -1;
        mixIdx = -1;
        for(int i=0; i<6; i++) colours[i] = false;
    }
    
    for(int i=0; i<6; i++){
        for(int j=0; j<6; j++) premix_colours[i][j] = 0;
    }
    
    premix_colours[0][0] = 50;
    premix_colours[0][1] = 25;
    premix_colours[0][3] = 25;
    premix_colours[1][0] = 50;
    premix_colours[1][1] = 25;
    premix_colours[1][2] = 25;
    premix_colours[2][1] = 30;
    premix_colours[2][2] = 60;
    premix_colours[2][4] = 10;
    premix_colours[3][1] = 45;
    premix_colours[3][3] = 45;
    premix_colours[3][4] = 10;
    premix_colours[4][1] = 34;
    premix_colours[4][3] = 50;
    premix_colours[4][5] = 16;
    premix_colours[5][3] = 11;
    premix_colours[5][4] = 24;
    premix_colours[5][5] = 65;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [panel_bg.layer setCornerRadius:10.0f];
    [title_lbl setText:@"Step 1 of 2 Choose a laying pattern"];
    
    [self.data_cv registerNib:[UINib nibWithNibName:@"SSPatternCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"PatternCell"];
    [self.data_cv registerNib:[UINib nibWithNibName:@"SSMixesCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"MixCell"];
    [self.data_cv registerNib:[UINib nibWithNibName:@"SSColourCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"ColourCell"];
    self.data_cv.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    self.createmix_btn.hidden = YES;
    
    self.proportions_vc = [[SSProportionsVC alloc] initWithNibName:@"SSProportionsVC" bundle:nil];
    CGRect frame = self.proportions_vc.view.frame;
    frame.origin.x = 184;
    frame.origin.y = 174;
    self.proportions_vc.view.frame = frame;
    [self.view addSubview:self.proportions_vc.view];
    self.proportions_vc.view.hidden = YES;
    
    [self showChooseMix];
    menu_vc.view.hidden = YES;
    //[self.patterns_cv registerClass:[SSPatternCell class] forCellWithReuseIdentifier:@"PatternCell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)leftPressed:(id)sender {
    switch(step){
        case _SP_MIX:
            menu_vc.view.hidden = NO;
            [kDMNavigationControllerRef.navController popViewControllerAnimated:YES];
            kDMNavigationControllerRef.activeVC = @"";
            [menu_vc showMenu:nil];
            break;
        case _SP_PATTERN:
            if (mixIdx==6){
                [self showProportions];
            }else{
                [self showChooseMix];
            }
            break;
        case _SP_COLOURS:
            [self showChooseMix];
            break;
        case _SP_PROPORTIONS:
            [self showChooseColours];
            break;
    }
}

- (IBAction)createMixPressed:(id)sender {
    if (step==_SP_COLOURS){
        NSMutableArray *cols = [[NSMutableArray alloc] init];
        int count=0;
        for(int i=0; i<6; i++) if (colours[i]) count++;
        if (count>0){
            int prop = 100/count;
            for(int i=0; i<6; i++){
                if (colours[i]){
                    [cols addObject:[NSNumber numberWithInt: prop]];
                }else{
                    [cols addObject:[NSNumber numberWithInt: 0]];
                }
            }
            [proportions_vc setColours:cols];
            [self showProportions];
        }
    }else{
        [self showChoosePattern];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    // _data is a class member variable that contains one array per section.
    return 1;
}

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    //NSArray* sectionArray = [_data objectAtIndex:section];
    int count = 0;
    switch(step){
        case _SP_PATTERN:
            count = 18;
            break;
        case _SP_MIX:
            count = 7;
            break;
        case _SP_COLOURS:
            count = 6;
            break;
    }
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SSPatternCell *cella;
    SSMixesCell *cellb;
    SSColourCell *cellc;
    NSArray *labels;
    int index;
    NSString *label;
    NSString *imageName;
    
    switch(step){
        case _SP_PATTERN:
            cella = [collectionView dequeueReusableCellWithReuseIdentifier:@"PatternCell"
                                                                                   forIndexPath:indexPath];

            labels = [[NSArray alloc] initWithObjects:@"Stretcher Bond", @"Stitch Bond", @"Elongated Stitch Bond", @"Third Bond - Left Hand", @"Third Bond - Right Hand",
                                                               @"Vertical Herringbone", @"Horizontal Herringbone", @"Herringbone - Left Hand", @"Herringbone - Right Hand", @"Vertical Double Herringbone",
                                                               @"Horizontal Double Herringbone", @"Left Hand Double Herringbone", @"Right Hand Double Herringbone", @"Vertical Triple Herringbone", @"Horizontal Triple Herringbone",
                                                               @"Left Hand Triple Herringbone", @"Right Hand Tripe Herringbone", @"Erratic Herringbone", nil];
            index = indexPath.row;
            
            label = [labels objectAtIndex:index];
            imageName = [NSString stringWithFormat:@"pattern%d.png", (index+1)];
            //NSLog(@"collectionView %@ %@", label, imageName);
            
            [[cella pattern_lbl] setText:label];
            [[cella pattern_img] setImage:[UIImage imageNamed:imageName]];
            
            return cella;
            break;
        case _SP_MIX:
            cellb = [collectionView dequeueReusableCellWithReuseIdentifier:@"MixCell"
                                                                        forIndexPath:indexPath];
        
            labels = [[NSArray alloc] initWithObjects:@"Vasanta", @"Grishma", @"Sharad", @"Hemant", @"Dava", @"Nisha", @"Create your own", nil];
            index = indexPath.row;
            
            label = [labels objectAtIndex:index];
            imageName = [NSString stringWithFormat:@"mix%d.png", (index+1)];
            //NSLog(@"collectionView %@ %@", label, imageName);
            
            [[cellb mix_lbl] setText:label];
            [[cellb mix_img] setImage:[UIImage imageNamed:imageName]];
            
            return cellb;
            break;
        case _SP_COLOURS:
            cellc = [collectionView dequeueReusableCellWithReuseIdentifier:@"ColourCell"
                                                              forIndexPath:indexPath];
            
            labels = [[NSArray alloc] initWithObjects:@"Chaler Beige", @"Jayrum Light", @"Ranya Brown", @"Salva Grey", @"Aruva Red", @"Black", nil];
            index = indexPath.row;
            
            label = [labels objectAtIndex:index];
            imageName = [NSString stringWithFormat:@"colour%d.png", (index+1)];
            NSLog(@"collectionView %@ %@", label, imageName);
            
            [[cellc colour_lbl] setText:label];
            [[cellc colour_img] setImage:[UIImage imageNamed:imageName]];
            cellc.selected_img.hidden = !colours[indexPath.row];
            
            return cellc;
            break;
    }

    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SSColourCell *cell;
    NSMutableDictionary *dict;
    
    switch(step){
        case _SP_PATTERN:
            patternIdx = indexPath.row;
            dict = [[NSMutableDictionary alloc] init];
            [dict setObject:[NSNumber numberWithInt:patternIdx] forKey:@"pattern"];
            [dict setObject:[NSNumber numberWithInt:mixIdx] forKey:@"mix"];
            if (mixIdx==6){
                [dict setObject:[proportions_vc getColours] forKey:@"colours"];
            }else{
                NSArray *colours = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:premix_colours[patternIdx][0]],
                                    [NSNumber numberWithInt:premix_colours[patternIdx][1]],
                                    [NSNumber numberWithInt:premix_colours[patternIdx][2]],
                                    [NSNumber numberWithInt:premix_colours[patternIdx][3]],
                                    [NSNumber numberWithInt:premix_colours[patternIdx][4]],
                                    [NSNumber numberWithInt:premix_colours[patternIdx][5]], nil];
                [dict setObject:colours forKey:@"colours"];
            }
            delegate.stonespar_dct = dict;
            [delegate showStonesparRender];
            break;
        case _SP_MIX:
            mixIdx = indexPath.row;
            if (mixIdx==6){
                [self showChooseColours];
            }else{
                //Show pattern;
                [self showChoosePattern];
            }
            break;
        case _SP_COLOURS:
            if (indexPath.row>=0 && indexPath.row<6){
                colours[indexPath.row] = !colours[indexPath.row];
                cell = (SSColourCell *)[collectionView cellForItemAtIndexPath:indexPath];
                cell.selected_img.hidden = !colours[indexPath.row];
            }
            break;
    }
}

-(void)showChoosePattern{
    step = _SP_PATTERN;
    title_lbl.text = @"Step 2 of 2 Choose a laying pattern";
    [left_btn setImage:[UIImage imageNamed:@"bwn_back_btn"] forState:UIControlStateNormal];
    CGRect frame = CGRectMake(125, 54, 662, 613);
    data_cv.frame = frame;
    [data_cv reloadData];
    data_cv.hidden = NO;
    proportions_vc.view.hidden = YES;
    self.createmix_btn.hidden = YES;
}

-(void)showChooseMix{
    step = _SP_MIX;
    title_lbl.text = @"Step 1 of 2 Choose a mix";
    [left_btn setImage:[UIImage imageNamed:@"bwn_close_btn"] forState:UIControlStateNormal];
    CGRect frame = CGRectMake(20, 103, 874, 485);
    data_cv.frame = frame;
    [data_cv reloadData];
    self.createmix_btn.hidden = YES;
}

-(void)showChooseColours{
    step = _SP_COLOURS;
    title_lbl.text = @"Choose upto 6 colours";
    CGRect frame = CGRectMake(70, 115, 774, 411);
    data_cv.frame = frame;
    data_cv.hidden = NO;
    self.proportions_vc.view.hidden = YES;
    [data_cv reloadData];
    [createmix_btn setImage:[UIImage imageNamed:@"btn_create_mix"] forState:UIControlStateNormal];
    self.createmix_btn.hidden = NO;
}

-(void)showProportions{
    step = _SP_PROPORTIONS;
    title_lbl.text = @"Edit Proportions";
    data_cv.hidden = YES;
    self.proportions_vc.view.hidden = NO;
    [createmix_btn setImage:[UIImage imageNamed:@"btn_view_mix"] forState:UIControlStateNormal];
    self.createmix_btn.hidden = NO;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize size;
    
    switch(step){
        case _SP_PATTERN:
            size = CGSizeMake(200.0, 160.0);
            break;
        case _SP_MIX:
            size = CGSizeMake(200.0, 195.0);
            break;
        case _SP_COLOURS:
            size = CGSizeMake(129.0, 411.0);
            break;
    }
    
    return size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    CGFloat flt;
    
    switch(step){
        case _SP_PATTERN:
            flt = 25.0f;
            break;
        case _SP_MIX:
            flt = 75.0f;
            break;
        case _SP_COLOURS:
            flt = 0.0f;
            break;
    }
    
    return flt;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    CGFloat flt;
    
    switch(step){
        case _SP_PATTERN:
            flt = 25.0f;
            break;
        case _SP_MIX:
            flt = 15.0f;
            break;
        case _SP_COLOURS:
            flt = 0.0f;
            break;
    }
    
    return flt;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

@end
