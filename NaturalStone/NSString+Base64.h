//
//  NSString+Base64.h
//  NaturalStone
//
//  Created by John McKerrell on 07/12/2012.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Base64)

+ (NSString *)encodeBase64WithString:(NSString *)strData;
+ (NSString *)encodeBase64WithData:(NSData *)objData;
+ (NSData *)decodeBase64WithString:(NSString *)strBase64;

@end
