//
//  SSColourCCell.h
//  NaturalStone
//
//  Created by Nik Lever on 30/04/2014.
//
//

#import <UIKit/UIKit.h>

@interface SSColourCCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *colour_img;
@property (weak, nonatomic) IBOutlet UILabel *colour_lbl;
@property (weak, nonatomic) IBOutlet UIImageView *selected_img;

@end
