//
//  SSMixesCell.h
//  NaturalStone
//
//  Created by Nik Lever on 29/04/2014.
//
//

#import <UIKit/UIKit.h>

@interface SSMixesCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mix_img;
@property (weak, nonatomic) IBOutlet UILabel *mix_lbl;

@end
