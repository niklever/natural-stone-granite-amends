//
//  SSRenderVC.h
//  NaturalStone
//
//  Created by Nik Lever on 30/04/2014.
//
//

#import <UIKit/UIKit.h>

@class SSMenuVC;
@class DMNavigationController;
@class SSProportionsBVC;

@interface SSRenderVC : UIViewController<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UIGestureRecognizerDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *render_img;
@property (weak, nonatomic) IBOutlet UIButton *menu_btn;
@property (weak, nonatomic) IBOutlet UIButton *play_btn;
@property (weak, nonatomic) IBOutlet UIButton *action_btn;
@property (strong, nonatomic) IBOutlet UIView *editbar_vw;
@property (weak, nonatomic) IBOutlet UIView *patternbar_vw;
@property (weak, nonatomic) SSMenuVC *menu_vc;
@property (weak, nonatomic) NSDictionary *data;
@property (weak, nonatomic) DMNavigationController *delegate;
@property (weak, nonatomic) IBOutlet UILabel *title_lbl;
@property (weak, nonatomic) IBOutlet UICollectionView *btns_cv;
@property (weak, nonatomic) IBOutlet UIImageView *overlay_img;
@property (weak, nonatomic) IBOutlet UIView *proportionsbar_vw;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *btns_layout;
@property (strong, nonatomic) SSProportionsBVC *proportions_vc;
@property (weak, nonatomic) IBOutlet UIView *actions_vw;
@property (weak, nonatomic) IBOutlet UIView *project_pnl;
@property (weak, nonatomic) IBOutlet UITextField *projectName_txt;
@property (weak, nonatomic) IBOutlet UITextField *projectLocation_txt;
@property (weak, nonatomic) IBOutlet UITextField *customersEmail_txt;
@property (weak, nonatomic) IBOutlet UITextField *marshallsEmail_txt;
@property (weak, nonatomic) IBOutlet UITextView *message_txt;
@property (weak, nonatomic) IBOutlet UILabel *message_lbl;
@property (weak, nonatomic) IBOutlet UISwitch *quote_swt;
@property (weak, nonatomic) IBOutlet UIView *email_pnl;
@property (weak, nonatomic) IBOutlet UIWebView *render_wv;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity_vw;
@property (weak, nonatomic) IBOutlet UILabel *area_lbl;
@property (weak, nonatomic) IBOutlet UITextField *area_txt;
@property (strong, nonatomic) DMProject *project;
@property (strong, nonatomic) NSDictionary *projectInfo;
@property (weak, nonatomic) IBOutlet UIButton *addproject_btn;
@property (weak, nonatomic) IBOutlet UIButton *email_btn;
@property (weak, nonatomic) IBOutlet UIView *bg_vw;
@property (weak, nonatomic) IBOutlet UILabel *project_pnl_title_lbl;
@property (weak, nonatomic) IBOutlet UIButton *addtolibrary_btn;
@property (weak, nonatomic) IBOutlet UIButton *toolbar_btn;

- (IBAction)changePatternPressed:(id)sender;
- (IBAction)editProportionsPressed:(id)sender;
- (IBAction)changeColoursPressed:(id)sender;
- (IBAction)menuPressed:(id)sender;
- (IBAction)playPressed:(id)sender;
- (IBAction)actionsPressed:(id)sender;
- (IBAction)proportionsDonePressed:(id)sender;
- (IBAction)addProjectPressed:(id)sender;
- (IBAction)sendEmailPressed:(id)sender;
- (IBAction)cancelAddProjectPressed:(id)sender;
- (IBAction)addToLibraryPressed:(id)sender;
- (IBAction)sendEmailBPressed:(id)sender;
- (IBAction)closeEmailPressed:(id)sender;
- (IBAction)quoteChanged:(id)sender;
- (id)initWithProject:(DMProject *)_project;
- (IBAction)toolbarBtnPressed:(id)sender;

@end
