//
//  DMAppDelegate.h
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DMTapDetectingWindow.h"

const NSString *kDefaultAPIBaseKey;
const NSString *kDefaultAPIPassword;
const NSTimeInterval kDefaultBackgroundLongInactiveWarningTime;
const NSTimeInterval kDefaultBackgroundInactiveWarningTime;
const NSTimeInterval kDefaultEnterBackgroundWarningTime;

@class DMNavigationController;
@class SSMenuVC;
@class SSGalleryVC;
@class SSMainVC;

@interface DMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) DMTapDetectingWindow *window;
@property (strong, nonatomic) DMNavigationController *viewController;
@property (assign, nonatomic) UIBackgroundTaskIdentifier backgroundTask;
@property (strong, nonatomic) id imageSyncObserver;
@property (nonatomic, strong) SSMenuVC *ss_menu;
@property (nonatomic, strong) SSGalleryVC *ss_gallery;
@property (nonatomic, strong) SSMainVC *ss_main;

- (void)changeVC:(NSString *)vcName;

@end
