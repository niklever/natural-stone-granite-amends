//
//  DMTapDetectingWindow.h
//  NaturalStone
//
//  Created by John McKerrell on 18/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DMTapDetectingWindowDelegate

- (void)userDidTapWebView;
- (void)userDidMoveWebView;

@end

@interface DMTapDetectingWindow : UIWindow

@property (nonatomic, weak) UIView *viewToObserve;
@property (nonatomic, weak) id <DMTapDetectingWindowDelegate> controllerThatObserves;

@end
