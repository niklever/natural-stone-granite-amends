//
//  SSMixesCell.m
//  NaturalStone
//
//  Created by Nik Lever on 29/04/2014.
//
//

#import "SSMixesCell.h"

@implementation SSMixesCell

@synthesize mix_img;
@synthesize mix_lbl;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
