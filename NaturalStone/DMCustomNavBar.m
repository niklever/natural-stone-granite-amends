//
//  DMCustomNavBar.m
//  NaturalStone
//
//  Created by John McKerrell on 12/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMCustomNavBar.h"

@interface DMCustomNavBar ()

@property (nonatomic,strong) UIButton *homeButton;
@property (nonatomic,strong) UIButton *backButton;
@property (nonatomic,strong) UIButton *libraryButton;
@property (nonatomic,strong) UIButton *forwardButton;
@property (nonatomic,strong) UIButton *saveButton;
@property (nonatomic,strong) UILabel *mainLabel;
@property (nonatomic,strong) UILabel *departingLabel;
@property (nonatomic,strong) UIView *searchView;
@property (nonatomic,strong) UIButton *searchButton;
@property (nonatomic,strong) UIButton *searchCancelButton;
@property (nonatomic,strong) UITextField *searchTextField;
@property (nonatomic,strong) UIView *exportActionsView;
@property (nonatomic,strong) UIButton *exportActionsButton;
@property (nonatomic,strong) UIButton *sendButton;
@property (nonatomic,strong) UIButton *quoteButton;
@property (nonatomic,strong) UIButton *cancelExportButton;
@property (nonatomic,strong) UIView *labelsContainer;
@property (nonatomic,assign) BOOL searchEnabled;

- (void)enableSearch;
- (void)cancelSearch;
- (void)searchTextUpdated;

@end

@implementation DMCustomNavBar

@synthesize homeButton = __homeButton;
@synthesize backButton = __backButton;
@synthesize libraryButton = __libraryButton;
@synthesize forwardButton = __forwardButton;
@synthesize saveButton = __saveButton;
@synthesize mainLabel = __mainLabel;
@synthesize departingLabel = __departingLabel;
@synthesize searchDelegate = __searchDelegate;
@synthesize searchView = __searchView;
@synthesize searchButton = __searchButton;
@synthesize searchCancelButton = __searchCancelButton;
@synthesize searchTextField = __searchTextField;
@synthesize exportActionsView = __exportActionsView;
@synthesize exportActionsButton = __exportActionsButton;
@synthesize sendButton = __sendButton;
@synthesize quoteButton = __quoteButton;
@synthesize cancelExportButton = __cancelExportButton;
@synthesize labelsContainer = __labelsContainer;
@synthesize searchEnabled = __searchEnabled;
@synthesize projectFilter;

- (id)initWithFrame:(CGRect)frame
{
    frame.origin.y = 0;
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        self.homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.homeButton.frame = CGRectMake(0, 0, 56, 77);
        [self.homeButton setBackgroundImage:[UIImage imageNamed:@"home-btn.png"] forState:UIControlStateNormal];
        [self.homeButton setBackgroundImage:[UIImage imageNamed:@"home-btn-on.png"] forState:UIControlStateHighlighted];
        [self.homeButton addTarget:kDMNavigationControllerRef action:@selector(goHome) forControlEvents:UIControlEventTouchUpInside];
        
        self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.backButton.frame = CGRectMake(61, 0, 119, 76);
        [self.backButton setBackgroundImage:[UIImage imageNamed:@"back-btn.png"] forState:UIControlStateNormal];
        [self.backButton setBackgroundImage:[UIImage imageNamed:@"back-btn-on.png"] forState:UIControlStateHighlighted];
        [self.backButton addTarget:kDMNavigationControllerRef action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];

        self.libraryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.libraryButton.frame = CGRectMake(61, 0, 134, 76);
        [self.libraryButton setBackgroundImage:[UIImage imageNamed:@"library-btn.png"] forState:UIControlStateNormal];
        [self.libraryButton setBackgroundImage:[UIImage imageNamed:@"library-btn-on.png"] forState:UIControlStateHighlighted];
        [self.libraryButton addTarget:kDMNavigationControllerRef action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];

        self.forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.forwardButton.frame = CGRectMake(911, 0, 117, 76);
        [self.forwardButton setBackgroundImage:[UIImage imageNamed:@"next-btn.png"] forState:UIControlStateNormal];
        [self.forwardButton setBackgroundImage:[UIImage imageNamed:@"next-btn-on.png"] forState:UIControlStateHighlighted];
        [self.forwardButton addTarget:kDMNavigationControllerRef action:@selector(goForwards) forControlEvents:UIControlEventTouchUpInside];
        
        self.saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.saveButton.frame = CGRectMake(907, 0, 117, 76);
        [self.saveButton setBackgroundImage:[UIImage imageNamed:@"finish-btn.png"] forState:UIControlStateNormal];
        [self.saveButton setBackgroundImage:[UIImage imageNamed:@"finish-btn-on.png"] forState:UIControlStateHighlighted];
        [self.saveButton addTarget:kDMNavigationControllerRef action:@selector(saveAndCloseProject) forControlEvents:UIControlEventTouchUpInside];

        self.labelsContainer = [[UIView alloc] initWithFrame:CGRectMake(0,0,1024,77)];
        self.labelsContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"header.png"]];
        
        self.mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(164,20,860,53)];
        self.mainLabel.backgroundColor = [UIColor clearColor];
        self.mainLabel.textAlignment = UITextAlignmentCenter;
        self.mainLabel.font = [UIFont fontWithName:kDMDefaultFont size:26.0f];
        self.mainLabel.textColor = [UIColor whiteColor];
        self.departingLabel = [[UILabel alloc] initWithFrame:CGRectMake(164,20,860,53)];
        self.departingLabel.backgroundColor = [UIColor clearColor];
        self.departingLabel.textAlignment = UITextAlignmentCenter;
        self.departingLabel.font = [UIFont fontWithName:kDMDefaultFont size:25.0f];
        self.departingLabel.textColor = [UIColor whiteColor];
        [self.labelsContainer addSubview:self.mainLabel];
        [self.labelsContainer addSubview:self.departingLabel];
        self.labelsContainer.clipsToBounds = YES;
        
        [self addSubview:self.labelsContainer];
        
        self.searchView = [[UIView alloc] initWithFrame:CGRectMake(1024, 0, 1024, 77)];
        self.searchView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"header.png"]];
        self.searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.searchButton.frame = CGRectMake(61,20,54,44);
        [self.searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
        [self.searchButton addTarget:self action:@selector(enableSearch) forControlEvents:UIControlEventTouchUpInside];
        
        self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(120,20,400,38)];
        self.searchTextField.borderStyle = UITextBorderStyleNone;
        [self.searchTextField fixForDolphin];
        self.searchTextField.clearButtonMode = UITextFieldViewModeAlways;
        self.searchTextField.backgroundColor = [UIColor colorWithRed:0.72 green:0.50 blue:0.25 alpha:1.00];
        self.searchTextField.textColor = [UIColor colorWithRed:0.95 green:0.92 blue:0.88 alpha:1.00];
        self.searchTextField.placeholder = NSLocalizedString(@"SEARCH_PLACEHOLDER", @"");
        [self.searchTextField setValue:[UIColor colorWithRed:0.95 green:0.92 blue:0.88 alpha:0.5f] 
            forKeyPath:@"_placeholderLabel.textColor"];

        [self.searchTextField addTarget:self action:@selector(searchTextUpdated) forControlEvents:UIControlEventEditingChanged];
        [self.searchView addSubview:self.searchTextField];
        [self.searchView addSubview:self.searchButton];
        
        self.searchCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.searchCancelButton.frame = CGRectMake(0, -77, 56, 77);
        [self.searchCancelButton setBackgroundImage:[UIImage imageNamed:@"close-btn.png"] forState:UIControlStateNormal];
        [self.searchCancelButton setBackgroundImage:[UIImage imageNamed:@"close-btn-on.png"] forState:UIControlStateHighlighted];
        [self.searchCancelButton addTarget:self action:@selector(cancelSearch) forControlEvents:UIControlEventTouchUpInside];

        self.projectFilter = [[UISegmentedControl alloc] initWithItems:[[NSArray alloc] initWithObjects:@"All", @"Granite", @"Stonespar", nil]];
        self.projectFilter.segmentedControlStyle = UISegmentedControlStyleBar;
        self.projectFilter.tintColor = [UIColor whiteColor];
        self.projectFilter.selectedSegmentIndex = 0;
        self.projectFilter.frame = CGRectMake(80, 35, 250, 30);
        [self.projectFilter addTarget:self action:@selector(filterChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:self.projectFilter];
        self.projectFilter.hidden = YES;

        self.exportActionsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.exportActionsButton.frame = CGRectMake(850, 0, 67, 76);
        [self.exportActionsButton setImage:[UIImage imageNamed:@"export.png"] forState:UIControlStateNormal];
        [self.exportActionsButton setImage:[UIImage imageNamed:@"export-on.png"] forState:UIControlStateHighlighted];
        [self addSubview:self.exportActionsButton];
        [self.exportActionsButton addTarget:kDMNavigationControllerRef action:@selector(sendEmail) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.homeButton];
        [self addSubview:self.backButton];
        [self addSubview:self.libraryButton];
        [self addSubview:self.forwardButton];
        [self addSubview:self.saveButton];

        [self addSubview:self.searchView];
        [self addSubview:self.searchCancelButton];
        
        [self setButtonsState:DMNavigationNoButtons animated:NO];

    }
    return self;
}

-(void) filterChanged:(id)sender{
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    NSString *type = [segmentedControl titleForSegmentAtIndex: [segmentedControl selectedSegmentIndex]];
    NSLog(@"Filter changed to %@", type);
    [self.searchDelegate customNavBarFilterByType:type];
}

- (void)dealloc {
    self.searchDelegate = nil;
    
}

- (void)updateForViewController:(UIViewController*)viewController transitionDirection:(DMTransitions)transitionDirection buttonStates:(DMNavigationButtons)buttonStates {
    // Switch the labels
    UILabel *extra = self.departingLabel;
    self.departingLabel = self.mainLabel;
    self.mainLabel = extra;
    self.mainLabel.text = viewController.title;
    CGRect mainFrame = self.mainLabel.frame;
    CGRect departingFrame = self.departingLabel.frame;
    if (transitionDirection == DMTransitionLeftToRight) {
        mainFrame.origin.x = - mainFrame.size.width;
        self.mainLabel.frame = mainFrame;
        mainFrame.origin.x = 82;
        departingFrame = self.departingLabel.frame;
        departingFrame.origin.x = departingFrame.size.width;
    } else {
        mainFrame.origin.x = mainFrame.size.width;
        self.mainLabel.frame = mainFrame;
        mainFrame.origin.x = 82;
        departingFrame = self.departingLabel.frame;
        departingFrame.origin.x = -departingFrame.size.width;
    }
    [UIView animateWithDuration:0.375f animations:^{
        self.mainLabel.frame = mainFrame;
        self.departingLabel.frame = departingFrame;
    }];
    [self setButtonsState:buttonStates];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)setButtonsState:(DMNavigationButtons)buttonsState {
    [self setButtonsState:buttonsState animated:YES];
}

- (void)setButtonsState:(DMNavigationButtons)buttonsState animated:(BOOL)animated {
    [UIView animateWithDuration:(animated?0.375f:0.0f) animations:^{
        CGRect frame = self.homeButton.frame;
        if ((buttonsState & DMNavigationHomeButton) == DMNavigationHomeButton) {
            frame.origin.y = 0;
        } else {
            frame.origin.y = -frame.size.height;
        }
        self.homeButton.frame = frame;
        
        frame = self.backButton.frame;
        if ((buttonsState & DMNavigationBackButton) == DMNavigationBackButton) {
            frame.origin.y = 0;
        } else {
            frame.origin.y = -frame.size.height;
        }
        self.backButton.frame = frame;    

        frame = self.libraryButton.frame;
        if ((buttonsState & DMNavigationLibraryButton) == DMNavigationLibraryButton) {
            frame.origin.y = 0;
        } else {
            frame.origin.y = -frame.size.height;
        }
        self.libraryButton.frame = frame;    

        frame = self.forwardButton.frame;
        if ((buttonsState & DMNavigationForwardButton) == DMNavigationForwardButton) {
            frame.origin.y = 0;
        } else {
            frame.origin.y = -frame.size.height;
        }
        self.forwardButton.frame = frame;  
        
        frame = self.saveButton.frame;
        if ((buttonsState & DMNavigationSaveButton) == DMNavigationSaveButton) {
            frame.origin.y = 0;
        } else {
            frame.origin.y = -frame.size.height;
        }
        self.saveButton.frame = frame;  

        frame = self.searchView.frame;
        if ((buttonsState & DMNavigationSearchButton) == DMNavigationSearchButton) {
            frame.origin.x = 908;
        } else {
            frame.origin.x = 1024;
        }
        self.searchView.frame = frame;  
        if ((buttonsState & DMNavigationSearchButton) == DMNavigationSearchButton && self.searchEnabled) {
            [self enableSearch];
        } else {
            self.labelsContainer.alpha = 1.0;
        }
        
        self.projectFilter.hidden = !(buttonsState & DMNavigationProjectFilter);
//        frame = self.exportActionsView.frame;
//        if ((buttonsState & DMNavigationExportButton) == DMNavigationExportButton) {
//            frame.origin.y = 0;
//        } else {
//            frame.origin.y = -57;
//        }
//        self.exportActionsView.frame = frame;  

        frame = self.exportActionsButton.frame;
        if ((buttonsState & DMNavigationExportButton) == DMNavigationExportButton) {
            frame.origin.y = 0;
        } else {
            frame.origin.y = -77;
        }
        self.exportActionsButton.frame = frame;  

    }];
}

- (void)hideNavigationBarAnimated:(BOOL)animated {
    [UIView animateWithDuration:(animated?0.375f:0.0f) animations:^{
        CGRect frame = self.frame;
        frame.origin.y = -frame.size.height;
        self.frame = frame;
    }];
}

- (void)showNavigationBarAnimated:(BOOL)animated {
    [UIView animateWithDuration:(animated?0.375f:0.0f) animations:^{
        CGRect frame = self.frame;
        frame.origin.y = 0;
        self.frame = frame;
    }];
}

- (void)hideNavigationBar {
    [self hideNavigationBarAnimated:YES];
}

- (void)showNavigationBar {
    [self showNavigationBarAnimated:YES];
}

- (void)enableSearch {
    self.searchEnabled = YES;
    [self.searchTextField becomeFirstResponder];
    if ([self.searchTextField.text length]) {
        [self.searchDelegate customNavBar:self searchTextUpdated:self.searchTextField.text];
    }
    [UIView animateWithDuration:0.375f animations:^{
        CGRect frame = self.searchView.frame;
        frame.origin.x = 0;
        self.searchView.frame = frame;
        
        frame = self.searchCancelButton.frame;
        frame.origin.y = 0;
        self.searchCancelButton.frame = frame;
        self.homeButton.alpha = 0.0;
        self.labelsContainer.alpha = 0.0;
    }];
}
- (void)cancelSearch {
    self.searchEnabled = NO;
    [self.searchTextField resignFirstResponder];
    [self.searchDelegate customNavBarSearchCancelled:self];
    [UIView animateWithDuration:0.375f animations:^{
        CGRect frame = self.searchView.frame;
        frame.origin.x = 908;
        self.searchView.frame = frame;

        frame = self.searchCancelButton.frame;
        frame.origin.y = -77;
        self.searchCancelButton.frame = frame;
        
        self.homeButton.alpha = 1.0;
        self.labelsContainer.alpha = 1.0;
    }];
}

- (void)showExportOptions {
    [UIView animateWithDuration:0.375f animations:^{
        CGRect frame = self.exportActionsView.frame;
        frame.origin.x = 535;
        self.exportActionsView.frame = frame;
        
        self.exportActionsButton.alpha = 0.0;
    }];
}

- (void)hideExportOptions {
    [UIView animateWithDuration:0.375f animations:^{
        CGRect frame = self.exportActionsView.frame;
        frame.origin.x = 794;
        self.exportActionsView.frame = frame;
        
        self.exportActionsButton.alpha = 1.0;
    }];
}

- (void)searchTextUpdated {
    [self.searchDelegate customNavBar:self searchTextUpdated:self.searchTextField.text];
}
@end
