//
//  NSDate+parseXMLDate.m
//  7digital
//
//  Created by John McKerrell on 31/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSDate+parseXMLDate.h"

@implementation NSDate (parseXMLDate)

+ (NSDate*)parseXMLDate:(NSString*)releaseDateStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //2008-01-04T00:00:00Z
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];

    NSRange dateColonRange = { 0, 1 };
    dateColonRange.location = [releaseDateStr length]-3;
    if ([[releaseDateStr substringFromIndex:([releaseDateStr length]-1)] isEqualToString:@"Z"]) {
        releaseDateStr = [releaseDateStr stringByReplacingOccurrencesOfString:@"Z" withString:@"+0000"];
//    } else if ([[releaseDateStr substringWithRange:dateColonRange] isEqualToString:@":"]) {
//        releaseDateStr = [releaseDateStr stringByReplacingOccurrencesOfString:@":" withString:@"" options:0 range:dateColonRange];
    }
    return [dateFormatter dateFromString:releaseDateStr];
}

@end
