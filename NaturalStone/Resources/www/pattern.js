var patternID = 0;
var colours;
var coloursAvailable;
var colourNames = ["chaler-beige", "jayrum-light", "ranya-brown", "salva-grey", "aruva-red", "holkar-black"];
var coloursUsed;
var maxRepeats;
var ready = false;
var flagIDs;
var flagIDIndex = 0;
var prevFlagID = 0;
var courses;
var flagStatus;

$(document).ready(function(){
    ready = true;
    if (patternID!=0) updatePattern(patternID);
});

function updatePattern(idx, col1, col2, col3, col4, col5, col6){
console.log("updatePattern patternID:" + patternID);
    patternID = idx;
    if (col1!=null){
		colours = new Array(col1, col2, col3, col4, col5, col6);
	}else{
		colours = new Array(17, 16, 17, 17, 16, 17);
	}
	initMaxRepeats();
	flagIDs = null;
    
    if (ready){
        switch(patternID){
            case 1:
            $("#preview").html(getPattern1());
            break;
            case 2:
            $("#preview").html(getPattern2());
            break;
            case 3:
            $("#preview").html(getPattern3());
            break;
            case 4:
            $("#preview").html(getPattern4());
            break;
            case 5:
            $("#preview").html(getPattern5());
            break;
            case 6:
            $("#preview").html(getPattern6());
            break;
            case 7:
            $("#preview").html(getPattern7());
            break;
            case 8:
            $("#preview").html(getPattern8());
            break;
            case 9:
            $("#preview").html(getPattern9());
            break;
            case 10:
            $("#preview").html(getPattern10());
            break;
            case 11:
            $("#preview").html(getPattern11());
            break;
            case 12:
            $("#preview").html(getPattern12());
            break;
            case 13:
            $("#preview").html(getPattern13());
            break;
            case 14:
            $("#preview").html(getPattern14());
            break;
            case 15:
            $("#preview").html(getPattern15());
            break;
            case 16:
            $("#preview").html(getPattern16());
            break;
            case 17:
            $("#preview").html(getPattern17());
            break;
            case 18:
            $("#preview").html(getPattern18());
            break;
        }
console.log("coloursUsed: " + getColourPercentages(coloursUsed));
    }
}

function initFlagIDs() {
	flagIDs = [];
	var counters = [];
	var sequenceLength = 0;
 coloursAvailable = 0;
	for (var colourIndex=0; colourIndex<colours.length; colourIndex++) {
		counters.push(0);
		sequenceLength += maxRepeats[colourIndex];
		if (colours[colourIndex] > 0) {coloursAvailable++;}
	}
	sequenceLength *= 8;
	if (sequenceLength < 30) {sequenceLength *= 2;}
	do {
				for (var colourIndex=0; colourIndex<colours.length; colourIndex++) {
					if (colours[colourIndex] > 0) {
						var counterTarget = counters[colourIndex] + (colours[colourIndex] / 100);
						var flagsToAdd = Math.floor(counterTarget) - Math.floor(counters[colourIndex]);
						for (var flagIndex=0; flagIndex<flagsToAdd; flagIndex++) {
							flagIDs.push(colourIndex + 1);
						}
						counters[colourIndex] = counterTarget;
					}
				}
	} while (flagIDs.length < sequenceLength);

 if (coloursAvailable >= 2) {

		for (var copyToIndex=flagIDs.length-1; copyToIndex>0; copyToIndex--) { // randomize sequence order
			var copyFromIndex = Math.floor(Math.random() * (copyToIndex + 1));
			var copyBuffer = flagIDs[copyToIndex];
			flagIDs[copyToIndex] = flagIDs[copyFromIndex];
			flagIDs[copyFromIndex] = copyBuffer;
		}

		var blockCount = countBlocks(flagIDs);
//console.log("flagIDs == " + JSON.stringify(flagIDs));
//console.log("blockCount == " + blockCount);

		var blocks = findBlocks(flagIDs); // try to break up blocks (repeats of the same color)
//console.log("blocks == " + JSON.stringify(blocks));

		for (var blockIndex=0; blockIndex<blocks.length; blockIndex++) {
			if (blocks[blockIndex].start > 0) {
//console.log("start: blockIndex == " + blockIndex);
				blockCount = countBlocks(flagIDs);
				var flagIDsSwapped = flagIDs.slice(0); // duplicate array
				var copyBuffer = flagIDsSwapped[blocks[blockIndex].start];
				flagIDsSwapped[blocks[blockIndex].start] = flagIDsSwapped[blocks[blockIndex].start - 1];
				flagIDsSwapped[blocks[blockIndex].start - 1] = copyBuffer;
				var blockCountSwapped = countBlocks(flagIDsSwapped);
//console.log("start: flagIDsSwapped == " + JSON.stringify(flagIDsSwapped));
//console.log("start: blockCount == " + blockCount);
//console.log("start: blockCountSwapped == " + blockCountSwapped);
				if (blockCountSwapped > blockCount) {flagIDs = flagIDsSwapped.slice(0); flagIDsSwapped = null;} // duplicate array
			}
			if (blocks[blockIndex].end < flagIDs.length - 1) {
//console.log("end: blockIndex == " + blockIndex);
				blockCount = countBlocks(flagIDs);
				var flagIDsSwapped = flagIDs.slice(0); // duplicate array
				var copyBuffer = flagIDsSwapped[blocks[blockIndex].end];
				flagIDsSwapped[blocks[blockIndex].end] = flagIDsSwapped[blocks[blockIndex].end + 1];
				flagIDsSwapped[blocks[blockIndex].end + 1] = copyBuffer;
				var blockCountSwapped = countBlocks(flagIDsSwapped);
//console.log("end: flagIDsSwapped == " + JSON.stringify(flagIDsSwapped));
//console.log("end: blockCount == " + blockCount);
//console.log("end: blockCountSwapped == " + blockCountSwapped);
				if (blockCountSwapped > blockCount) {flagIDs = flagIDsSwapped.slice(0); flagIDsSwapped = null;} // duplicate array
			}
		}
//console.log("finished breaking up: flagIDs == " + JSON.stringify(flagIDs));
//console.log("finished breaking up: blockCount == " + countBlocks(flagIDs));

	}
 if (coloursAvailable >= 3) {

		blocks = findBlocks(flagIDs, true); // now find excessively long blocks, and move their flags elsewhere, ideally into new blocks
//console.log("blocks == " + JSON.stringify(blocks));
		for (var blockIndex=0; blockIndex<blocks.length; blockIndex++) {
			var id = flagIDs[blocks[blockIndex].start];
			var blockStart = blocks[blockIndex].start;
			var blockLength = blocks[blockIndex].end - blocks[blockIndex].start + 1;
			if (blockLength > maxRepeats[id - 1] * 1.1) {
//console.log("block " + blockIndex + " too long; length: " + blockLength + "; colour: " + id);
				var flagsToShift = Math.floor(blockLength - (maxRepeats[id - 1] * 1.1));
//console.log("flagsToShift == " + flagsToShift);
				var flagsShifted = false;
				for (var shiftIndex=0; shiftIndex<flagsToShift; shiftIndex++) {
//console.log("shiftIndex == " + shiftIndex);
					var shiftOK = false;
					do { // start searching from a random point (outside the current block) for a place to shift this flag to
						var offset = Math.floor(Math.random() * flagIDs.length);
					} while ((offset >= blockStart) && (offset <= blockStart + blockLength - 1));
					var shiftDestination = undefined;
					for (var flagIDIndex=0; flagIDIndex<flagIDs.length; flagIDIndex++) { // first try to create a new block of this colour to take the flag we're moving
						var offsetFlagIDIndex = flagIDIndex + offset;
						if (offsetFlagIDIndex >= flagIDs.length) {offsetFlagIDIndex -= flagIDs.length;}
						if (offsetFlagIDIndex > 0) {
							if ((flagIDs[offsetFlagIDIndex] != id) && (flagIDs[offsetFlagIDIndex - 1] != id)) {
								shiftOK = true;
								shiftDestination = offsetFlagIDIndex;
								break;
							}
						} else {
							if ((flagIDs[offsetFlagIDIndex] != id) && (flagIDs[flagIDs.length - 1] != id)) {
								shiftOK = true;
								shiftDestination = offsetFlagIDIndex;
								break;
							}
						}
					}
					if (! shiftOK) { // if necessary, move the flag into another (hopefully shorter) block of the same colour
						for (var flagIDIndex=0; flagIDIndex<flagIDs.length; flagIDIndex++) {
							var offsetFlagIDIndex = flagIDIndex + offset;
							if (offsetFlagIDIndex >= flagIDs.length) {offsetFlagIDIndex -= flagIDs.length;}
							if (offsetFlagIDIndex > 0) {
								if ((flagIDs[offsetFlagIDIndex] != id) || (flagIDs[offsetFlagIDIndex - 1] != id)) {
									shiftOK = true;
									shiftDestination = offsetFlagIDIndex;
									break;
								}
							} else {
								if ((flagIDs[offsetFlagIDIndex] != id) || (flagIDs[flagIDs.length - 1] != id)) {
									shiftOK = true;
									shiftDestination = offsetFlagIDIndex;
									break;
								}
							}
						}
					}
					if (shiftOK) {
//console.log("shiftDestination == " + shiftDestination);
//console.log("before: flagIDs == " + JSON.stringify(flagIDs));
//console.log("before: blocks.length == " + blocks.length);
      if (blockIndex >= blocks.length) {break; break;} // emergency bailout
						if (shiftDestination < blocks[blockIndex].start) { // moving to left: take the rightmost flag in the block
							flagIDs.splice(blocks[blockIndex].start + blockLength - 1, 1);
							flagIDs.splice(shiftDestination, 0, id);
							blocks[blockIndex].start++;
						} else { // moving to right: take the leftmost flag in the block
							flagIDs.splice(blocks[blockIndex].start, 1);
							flagIDs.splice(shiftDestination - 1, 0, id);
							blocks[blockIndex].end--;
						}
//console.log(" after: flagIDs == " + JSON.stringify(flagIDs));
						flagsShifted = true;
						blockLength--;
					}
				}
				if (flagsShifted) {blocks = findBlocks(flagIDs, true);}
//console.log(" after: blocks.length == " + blocks.length);
			}
		}

	} else if (coloursAvailable == 2) {

//console.log("before: flagIDs == " + JSON.stringify(flagIDs));
//console.log("before: blockCount == " + countBlocks(flagIDs));
		blocks = findBlocks(flagIDs, true); // now find the longest and shortest blocks, and move flags between them to even them out a bit
//console.log("before: blocks == " + JSON.stringify(blocks));

		for (var blockIndex=0; blockIndex<blocks.length; blockIndex++) {
			var id = flagIDs[blocks[blockIndex].start];
			var blockStart = blocks[blockIndex].start;
			var blockLength = blocks[blockIndex].end - blocks[blockIndex].start + 1;
			if (blockLength > maxRepeats[id - 1] * 1.1) {
//console.log("block " + blockIndex + " too long; length: " + blockLength + "; colour: " + id);
	  	var flagsToShift = Math.floor(blockLength - (maxRepeats[id - 1] * 1.1));
//console.log("flagsToShift == " + flagsToShift);
				var flagsShifted = false;
				for (var shiftIndex=0; shiftIndex<flagsToShift; shiftIndex++) {
//console.log("shiftIndex == " + shiftIndex);
					var shiftOK = false;
					var shiftDestination = undefined;
					for (var searchBlockIndex=0; searchBlockIndex<blocks.length; searchBlockIndex++) {
						if ((searchBlockIndex != blockIndex) && (flagIDs[blocks[searchBlockIndex].start] == id)) {
							var searchBlockLength = blocks[searchBlockIndex].end - blocks[searchBlockIndex].start + 1;
							if (searchBlockLength < maxRepeats[id - 1]) {
//console.log("found block " + searchBlockIndex + "; length " + searchBlockLength + "; spaces available " + (maxRepeats[id - 1] - searchBlockLength));
        if (Math.random() >= 0.5) {
									shiftOK = true;
									shiftDestination = blocks[searchBlockIndex].start;
									break;
								}
							}
						}
					}
					if (shiftOK) {
//console.log("shiftDestination == " + shiftDestination);
//console.log("before: flagIDs == " + JSON.stringify(flagIDs));
//console.log("before: blocks.length == " + blocks.length);
      if (blockIndex >= blocks.length) {break; break;} // emergency bailout
						if (shiftDestination < blocks[blockIndex].start) { // moving to left: take the rightmost flag in the block
							flagIDs.splice(blocks[blockIndex].start + blockLength - 1, 1);
							flagIDs.splice(shiftDestination, 0, id);
							blocks[blockIndex].start++;
						} else { // moving to right: take the leftmost flag in the block
							flagIDs.splice(blocks[blockIndex].start, 1);
							flagIDs.splice(shiftDestination - 1, 0, id);
							blocks[blockIndex].end--;
						}
//console.log(" after: flagIDs == " + JSON.stringify(flagIDs));
						flagsShifted = true;
						blockLength--;
					}
				if (flagsShifted) {blocks = findBlocks(flagIDs, true);}
//console.log(" after: blocks.length == " + blocks.length);
				}
			}
		}
	}

 if (coloursAvailable == 1) {
		for (var colourIndex=0; colourIndex<colours.length; colourIndex++) {
			if (colours[colourIndex] > 0) {
				flagIDs.push(colourIndex + 1);
		  break;
			}
		}
	}

//console.log("final: flagIDs == " + JSON.stringify(flagIDs));
//console.log("final: blockCount == " + countBlocks(flagIDs));

	flagIDIndex = Math.floor(Math.random() * flagIDs.length);
}

function findBlocks(sequence, includeSingleFlags) {
		var buffer = undefined;
		var blockStartTarget = -1;
		var blockStart = -1;
		var blockEnd = -1;
		var blocks = [];
		for (var sequenceIndex=0; sequenceIndex<sequence.length; sequenceIndex++) {
			if (sequence[sequenceIndex] != buffer) {
				blockStartTarget = sequenceIndex;
				if ((blockStart > -1) && ((blockEnd > blockStart) || includeSingleFlags)) {
					if (includeSingleFlags && (blockEnd < blockStart)) {blockEnd = blockStart;}
					blocks.push({start:blockStart, end:blockEnd});
				}
				blockStart = blockStartTarget;
			} else if (sequenceIndex == sequence.length - 1) {
				blockEnd = sequenceIndex;
				if ((blockStart > -1) && ((blockEnd > blockStart) || includeSingleFlags)) {
					if (includeSingleFlags && (blockEnd < blockStart)) {blockEnd = blockStart;}
					blocks.push({start:blockStart, end:blockEnd});
				}
			} else {
				blockEnd = sequenceIndex;
			}
			buffer = sequence[sequenceIndex];
		}
		return blocks;
}

function countBlocks(sequence) {
	var blockCount = 0;
	var buffer = undefined;
	for (var sequenceIndex=0; sequenceIndex<sequence.length; sequenceIndex++) {
		if (sequence[sequenceIndex] != buffer) {
			blockCount++;
		}
		buffer = sequence[sequenceIndex];
	}
	return blockCount;
}

function getFlagID(verticalRepeatCheckOffsets) {
 flagStatus = null;
	if ((flagIDs == null) || (Math.random() > 0.97)) { // rebuild the array every ~30 flags to prevent the diagonal banding that would happen if the number of flags on a course were closely related to the length of the array
	 initFlagIDs();
	}
	flagIDIndex++;
	if (flagIDIndex >= flagIDs.length) {flagIDIndex = 0;}
	var id = flagIDs[flagIDIndex];

 if (coloursAvailable >= 2) {

		var horizontalRepeats = 1;
		for (var flagIndex=courses[courses.length-1].length-1; flagIndex>=0; flagIndex--) {
			if (courses[courses.length - 1][flagIndex] == id) {
				horizontalRepeats++;
			} else {
				break;
			}
		}

		var verticalRepeats = 1;
		if (verticalRepeatCheckOffsets && (courses.length >= 2)) {
			for (var offsetIndex=0; offsetIndex<verticalRepeatCheckOffsets.length; offsetIndex++) {
				if (courses[courses.length - 2][courses[courses.length-1].length + verticalRepeatCheckOffsets[offsetIndex]] == id) {verticalRepeats++;}
			}
		}

		var colourChangeProbability = 0;
		if ((horizontalRepeats > maxRepeats[id-1] * 1.5) && (verticalRepeats > 2)) {
			colourChangeProbability = 0.8;
			//flagStatus = horizontalRepeats + ":" + verticalRepeats + " both"; // debug flag label
		} else if (horizontalRepeats > maxRepeats[id-1] * 1.5) {
			colourChangeProbability = 0.8;
			if (patternID == 18) {colourChangeProbability = 1;}
			//flagStatus = horizontalRepeats + ":" + verticalRepeats + " horizontal"; // debug flag label
		} else if ((horizontalRepeats > 1) && (verticalRepeats > 2)) {
			colourChangeProbability = 0.15;
			//flagStatus = horizontalRepeats + ":" + verticalRepeats + " vertical"; // debug flag label
		} else if ((horizontalRepeats == 1) && (verticalRepeats == 2) && (maxRepeats[id-1] == 1)) {
			colourChangeProbability = 0.3;
			//flagStatus = horizontalRepeats + ":" + verticalRepeats + " diagonal"; // debug flag label
		}
		if ((patternID == 18) && ((courses.length == 3) || (courses.length == 8) || (courses.length == 13) || (courses.length == 18)) && (verticalRepeats >= 2) && (maxRepeats[id-1] == 1)) {
			colourChangeProbability = 1;
			//flagStatus = horizontalRepeats + ":" + verticalRepeats + " erratic"; // debug flag label
		}

		if (Math.random() < colourChangeProbability) {
			while (flagIDs[flagIDIndex] == id) {
				flagIDIndex++;
				if (flagIDIndex >= flagIDs.length) {flagIDIndex = 0;}
			}
			id = flagIDs[flagIDIndex];
		} else {
			flagStatus = null;
		}

  if (patternID <= 3) {
			if (courses.length >= 5) {
				var maxMaxRepeats = 0
				for (var colourIndex=0; colourIndex<colours.length; colourIndex++) {
					if (maxRepeats[colourIndex] > maxMaxRepeats) {maxMaxRepeats = maxRepeats[colourIndex];}
				}
				if ((courses[courses.length - 3][courses[courses.length-1].length] == id) && (courses[courses.length - 5][courses[courses.length-1].length] == id) && (maxRepeats[id-1] < maxMaxRepeats)) {
					while (flagIDs[flagIDIndex] == id) {
						flagIDIndex++;
						if (flagIDIndex >= flagIDs.length) {flagIDIndex = 0;}
					}
					id = flagIDs[flagIDIndex];
					//flagStatus = horizontalRepeats + ":" + verticalRepeats + " toast rack"; // debug flag label
				}
			}
	 }

	}

	coloursUsed[id-1]++;
 //flagStatus = "[" + (courses.length - 1) + "][" + (courses[courses.length-1].length) + "] " + horizontalRepeats + ":" + verticalRepeats + ":" + maxRepeats[id-1]; // debug flag label: [course][flag], horizontalRepeats, verticalRepeats, maxRepeats
 //flagStatus = "[" + (courses.length - 1) + "][" + (courses[courses.length-1].length) + "] " + verticalRepeatCheckOffsets; // debug flag label: [course][flag], relative indices of which flag(s) on previous course to check for verticalRepeats
 prevFlagID = id;
 courses[courses.length-1].push(id);
 return colourNames[id-1] + "-" + Math.ceil(Math.random() * 4);
}

function initCourse(firstCourse){
	if (firstCourse) {
		courses = [];
	}
	courses.push([]);
}

function initMaxRepeats() {
	coloursUsed = [];
	maxRepeats = [];
	var availableColoursMinIndex = undefined;
	var availableColoursMinPercentage = 100;
	var enabledColourCount = 0;
	for (var colourIndex=0; colourIndex<colours.length; colourIndex++) {
	 coloursUsed.push(0);
	 maxRepeats.push(0);
  if (colours[colourIndex] > 0) {
   if (colours[colourIndex] < availableColoursMinPercentage) {
				availableColoursMinIndex = colourIndex;
				availableColoursMinPercentage = colours[colourIndex];
			}
			enabledColourCount++;
		}
	}
	var maxRepeatsTotal = 0;
	for (var colourIndex=0; colourIndex<colours.length; colourIndex++) {
  if (colours[colourIndex] > 0) {
			if (colourIndex == availableColoursMinIndex) {
				maxRepeats[colourIndex] = 1;
				maxRepeatsTotal++;
			} else {
				var maxRepeatsTarget = Math.ceil(colours[colourIndex] / availableColoursMinPercentage);
				if (maxRepeatsTarget > 10) {maxRepeatsTarget = 10;}
				maxRepeats[colourIndex] = maxRepeatsTarget;
				maxRepeatsTotal += maxRepeatsTarget;
			}
		}
	}
	if (maxRepeatsTotal == enabledColourCount) { // if all colours have maxRepeats=1, change them to 2 for more pattern variety
	 for (var colourIndex=0; colourIndex<colours.length; colourIndex++) {
			if (colours[colourIndex] > 0) {
				maxRepeats[colourIndex] = 2;
			}
		}
	}
console.log("colours: " + colours);
console.log("maxRepeats: " + maxRepeats);
}

function getColourPercentages(colourArray) { // convert an array of colours by number of flags used, to percentages
	duplicateColorArray = [];
	total = 0;
	for (var colourIndex=0; colourIndex<colourArray.length; colourIndex++) {
		duplicateColorArray.push(colourArray[colourIndex]);
		total += colourArray[colourIndex];
	}
	for (var colourIndex=0; colourIndex<colourArray.length; colourIndex++) {
		duplicateColorArray[colourIndex] = Math.round((colourArray[colourIndex] / total) * 100);
	}
	return duplicateColorArray;
}



function getPattern1(){
	str = "";
	offset = 0;
	initCourse(true);
	for(var top=0; top<850; top+=20){
		offset = (offset==0) ? -46 : 0;
		for(var left = -50; left<1124; left+=92){
			str += '<img src="flag-' + getFlagID(offset == 0 ? [0, 1] : [-1, 0]) + '.jpg" class="rotate0" width="92px" height="20px" style="left:' + (left + offset) + 'px; top:' + top + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate0" style="position:absolute; left:' + (left + offset) + 'px; top:' + top + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
		}
		initCourse();
	}
	return str;
}

function getPattern2(){
	str = "";
	offset = 0;
	initCourse(true);
	for(var top=0; top<850; top+=20){
		offset = (offset==0) ? -16 : 0;
		for(var left = -50; left<1124; left+=92){
			str += '<img src="flag-' + getFlagID([0]) + '.jpg" class="rotate0" width="92px" height="20px" style="left:' + (left + offset) + 'px; top:' + top + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate0" style="position:absolute; left:' + (left + offset) + 'px; top:' + top + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
		}
		initCourse();
	}
	return str;
}

function getPattern3(){
	str = "";
	offsets = new Array(0, -16, -32, -16);
	offsetID = 0;
	initCourse(true);
	for(var top=0; top<850; top+=20){
		offset = offsets[offsetID];
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		for(var left = -50; left<1124; left+=92){
			str += '<img src="flag-' + getFlagID([0]) + '.jpg" class="rotate0" width="92px" height="20px" style="left:' + (left + offset) + 'px; top:' + top + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate0" style="position:absolute; left:' + (left + offset) + 'px; top:' + top + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
		}
		initCourse();
	}
	return str;
}

function getPattern4(){
	str = "";
	offsets = new Array(0, -32, -64);
	offsetID = 0;
	initCourse(true);
	for(var top=0; top<850; top+=20){
		offset = offsets[offsetID];
		offsetID++;
		if (offsetID>=offsets.length) {offsetID=0;}
		for(var left = -50; left<1124; left+=92){
			str += '<img src="flag-' + getFlagID(offsetID == 1 ? [1] : [0]) + '.jpg" class="rotate0" width="92px" height="20px" style="left:' + (left + offset) + 'px; top:' + top + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate0" style="position:absolute; left:' + (left + offset) + 'px; top:' + top + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
		}
		initCourse();
	}
	return str;
}

function getPattern5(){
	str = "";
	offsets = new Array(0, 32, 64);
	offsetID = 0;
	initCourse(true);
	for(var top=0; top<850; top+=20){
		offset = offsets[offsetID];
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;	
		for(var left = -100; left<1124; left+=92){
			str += '<img src="flag-' + getFlagID(offsetID == 1 ? [-1] : [0]) + '.jpg" class="rotate0" width="92px" height="20px" style="left:' + (left + offset) + 'px; top:' + top + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate0" style="position:absolute; left:' + (left + offset) + 'px; top:' + top + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
		}
		initCourse();
	}
	return str;
}

/*function getPattern6(){
	var str = "";
	var offsets = new Array({left:0, top:0}, {left:0, top:-14.094995});
	initCourse(true);
	var rotation = 45;	
	var offsetID = 0;
	for(var left = -100; left<1124; left+=65.053823){
			var offset = offsets[offsetID];
			offsetID++;
			if (offsetID>=offsets.length) offsetID=0;
		for(var top=-50; top<850; top+=28.189990){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([0, 1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([-1, 0]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>'; // the combination of rotateY(180deg), negative translateX and rotateZ(45deg) flips the flag and rotates it 135deg, keeping the highlight edges at the top
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}
		}
		rotation = (rotation==45) ? 135 : 45;
		initCourse();
	}
	return str;
}*/
function getPattern6(){
	var str = "";
	var offsets = new Array({left:0, top:0}, {left:0, top:-14});
	initCourse(true);
	var rotation = 45;
	var offsetID = 0;
	for(var left = -100; left<1124; left+=64){
			var offset = offsets[offsetID];
			offsetID++;
			if (offsetID>=offsets.length) offsetID=0;
		for(var top=-50; top<850; top+=28){
			str += '<img src="flag-' + getFlagID(offsetID == 0 ? [-1, 0] : [0, 1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
		}
		rotation = (rotation==45) ? 135 : 45;
		initCourse();
	}
	return str;
}

/*function getPattern7(){
	var str = "";
	var rotation = 45;	
	offsets = new Array(0, 14.094995);
	var offsetID = 0;
	initCourse(true);
	for(var top=-50; top<850; top+=65.053823){
		var offset = offsets[offsetID];	
		for(var left = -100; left<1124; left+=28.189990){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([-1, 0]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset) + 'px) translateY(' + top + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset) + 'px) translateY(' + top + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([0, 1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset)) + 'px) translateY(' + top + 'px) rotateZ(' + 45 + 'deg);"/>'; // the combination of rotateY(180deg), negative translateX and rotateZ(45deg) flips the flag and rotates it 135deg, keeping the highlight edges at the top
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset)) + 'px) translateY(' + top + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		initCourse();
	}
	return str;
}*/
function getPattern7(){
	var str = "";
	var rotation = 45;	
	offsets = new Array(0, 14);
	var offsetID = 0;
	initCourse(true);
	for(var top=-50; top<850; top+=64){
	//for(var top=100; top<250; top+=65){
		var offset = offsets[offsetID];	
		for(var left = -100; left<1124; left+=28){
			str += '<img src="flag-' + getFlagID(offset == 0 ? [-1, 0] : [0, 1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset) + 'px; top:' + top + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset) + 'px; top:' + top + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		//if (offsetID==0) top += 0;
		initCourse();
	}
	return str;
}

function getPattern8(){
	var str = "";
	var rotation = 90;	
	offsets = new Array({left:0, top:46}, { left:16, top:76});
	var offsetID = 0;
	initCourse(true);
	for(var top=0; top<1250; top+=46){
		var offset = { left:offsets[offsetID].left, top:offsets[offsetID].top };	
		for(var left = -780; left<1024; left+=20){
			str += '<img src="flag-' + getFlagID(offsetID == 1 ? [-2, -1] : [1, 2]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left +  offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			offset.top -= 20;
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==0) ? 90 : 0;
		if (rotation==90){
			offsets[0].left += 92;
			offsets[1].left += 92;
		}
		initCourse();
	}
	return str;
}

function getPattern9(){
	var str = "";
	var rotation = 90;	
	offsets = new Array({left:56, top:56}, {left:0, top:46});
	var offsetID = 0;
	initCourse(true);
	for(var top=-900; top<0; top+=46){
		var offset = { left:offsets[offsetID].left, top:offsets[offsetID].top };	
		for(var left = 40; left<1324; left+=20){
			str += '<img src="flag-' + getFlagID(offsetID == 1 ? [-1, 0] : [0, 1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left +  offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			offset.top += 20;
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==0) ? 90 : 0;
		if (rotation==90){
			offsets[0].left -= 92;
			offsets[1].left -= 92;
		}
		initCourse();
	}
	return str;
}

/*function getPattern10(){
	var str = "";
	var rotation = 45;	
	offsets = new Array({left:0, top:0}, { left:-14.094995, top:28.189990});
	var offsetID = 0;
	initCourse(true);
	for(var left = -50; left<1024; left+=65.053823){
		var offset = offsets[offsetID];	
		for(var top=-100; top<780; top+=56.379981){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left - 14.094995) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left - 14.094995) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>'; // the combination of rotateY(180deg), negative translateX and rotateZ(45deg) flips the flag and rotates it 135deg, keeping the highlight edges at the top
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 14.094995)) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 14.094995)) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		initCourse();
	}
	return str;
}*/
function getPattern10(){
	var str = "";
	var rotation = 45;	
	offsets = new Array({left:0, top:0}, { left:21, top:29});
	var offsetID = 0;
	initCourse(true);
	for(var left = -50; left<1024; left+=28.5){
	//for(var left = 100; left<294; left+=30){
		var offset = offsets[offsetID];	
		for(var top=-100; top<780; top+=56){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([-1]) + '-45.png" class="rotate' + rotation + '" width="78px" height="78px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top + 1) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top + 1) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-1]) + '-45.png" class="rotate' + rotation + '" width="78px" height="78px" style="left:' + (left + offset.left - 14) + 'px; top:' + (top + offset.top + 15) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left - 14) + 'px; top:' + (top + offset.top + 15) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([1]) + '-45.png" class="rotate' + rotation + '" width="78px" height="78px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '-45.png" class="rotate' + rotation + '" width="78px" height="78px" style="left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top + 14) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top + 15) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		if (offsetID==0) left += 70;
		initCourse();
	}
	return str;
}

/*function getPattern11(){
	var str = "";
	var rotation = 45;	
	offsets = new Array({left:0, top:0}, { left:28.189990, top:-14.094995});
	var offsetID = 0;
	initCourse(true);
	for(var top=-100; top<780; top+=65.053823){
		var offset = offsets[offsetID];	
		for(var left = -100; left<1024; left+=56.379981){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left + 14.094995) + 'px) translateY(' + (top + offset.top - 14.094995) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left + 14.094995) + 'px) translateY(' + (top + offset.top - 14.094995) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>'; // the combination of rotateY(180deg), negative translateX and rotateZ(45deg) flips the flag and rotates it 135deg, keeping the highlight edges at the top
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 14.094995)) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 14.094995)) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		//if (offsetID==0) top += 15;
		initCourse();
	}
	return str;
}*/
function getPattern11(){
	var str = "";
	var rotation = 45;	
	offsets = new Array({left:0, top:0}, { left:28, top:-6.5});
	var offsetID = 0;
	initCourse(true);
	for(var top=-100; top<780; top+=56.5){
		var offset = offsets[offsetID];	
		for(var left = -100; left<1024; left+=56){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([-1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top - 14) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top - 14) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top + 14) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top + 14) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		if (offsetID==0) top += 15;
		initCourse();
	}
	return str;
}

function getPattern12(){
	var str = "";
	var rotation = 90;	
	offsets = new Array({left:0, top:46}, { left:76, top:36});
	var offsetID = 0;
	initCourse(true);
	for(var top=0; top<1250; top+=46){
		var offset = { left:offsets[offsetID].left, top:offsets[offsetID].top };	
		for(var left = -780; left<1024; left+=40){
			if (rotation==0){
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 20) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 20) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left + 20) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 20) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}
			offset.top -= 40;
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==0) ? 90 : 0;
		if (rotation==90){
			offsets[0].left += 92;
			offsets[1].left += 92;
		}
		initCourse();
	}
	return str;
}

function getPattern13(){
	var str = "";
	var rotation = 90;	
	offsets = new Array({left:56, top:-16}, {left:20, top:14});
	var offsetID = 0;
	initCourse(true);
	for(var top=-900; top<0; top+=46){
		var offset = { left:offsets[offsetID].left, top:offsets[offsetID].top };	
		for(var left = 0; left<1324; left+=40){
			if (rotation==0){
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 20) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 20) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left - 20) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left - 20) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}
			offset.top += 40;
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==0) ? 90 : 0;
		if (rotation==90){
			offsets[0].left -= 92;
			offsets[1].left -= 92;
		}
		initCourse();
	}
	return str;
}

/*function getPattern14(){
	var str = "";
	var rotation = 45;	
	offsets = new Array({left:0, top:0}, { left:-28.189990, top:42.284986});
	var offsetID = 0;
	initCourse(true);
	for(var left = -50; left<1124; left+=65.053823){
		var offset = offsets[offsetID];	
		for(var top=-100; top<780; top+=84.569971){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left - 14.094995) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left - 14.094995) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-2]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left - 28.189990) + 'px) translateY(' + (top + offset.top + 28.189990) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left - 28.189990) + 'px) translateY(' + (top + offset.top + 28.189990) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([2]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>'; // the combination of rotateY(180deg), negative translateX and rotateZ(45deg) flips the flag and rotates it 135deg, keeping the highlight edges at the top
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 14.094995)) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 14.094995)) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 28.189990)) + 'px) translateY(' + (top + offset.top + 28.189990) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 28.189990)) + 'px) translateY(' + (top + offset.top + 28.189990) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		initCourse();
	}
	return str;
}*/
function getPattern14(){
	var str = "";
	var rotation = 45;	
	offsets = new Array({left:0, top:0}, { left:6.5, top:42.5});
	var offsetID = 0;
	initCourse(true);
	for(var left = -50; left<1124; left+=29){
	//for(var left = 100; left<334; left+=30){
		var offset = offsets[offsetID];	
		for(var top=-100; top<780; top+=84){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([-1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left - 14) + 'px; top:' + (top + offset.top + 14) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left - 14) + 'px; top:' + (top + offset.top + 14) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-2]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left - 28) + 'px; top:' + (top + offset.top + 28) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left - 28) + 'px; top:' + (top + offset.top + 28) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([2]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 0.5) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 0.5) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top + 13.5) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top + 13.5) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left + 28) + 'px; top:' + (top + offset.top + 27.5) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 28) + 'px; top:' + (top + offset.top + 27.5) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		if (rotation==45) left += 60;
		if (offsetID==0) left += 9;
		initCourse();
	}
	return str;
}

/*function getPattern15(){
	var str = "";
	var rotation = 45;	
	offsets = new Array({left:0, top:0}, { left:42.284986, top:-28.189990});
	var offsetID = 0;
	initCourse(true);
	for(var top=-68; top<785; top+=65.053823){
		var offset = offsets[offsetID];	
		for(var left = -100; left<1024; left+=84.569971){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left + 14.094995) + 'px) translateY(' + (top + offset.top - 14.094995) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left + 14.094995) + 'px) translateY(' + (top + offset.top - 14.094995) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-2]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: translateX(' + (left + offset.left + 28.189990) + 'px) translateY(' + (top + offset.top - 28.189990) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform:translateX(' + (left + offset.left + 28.189990) + 'px) translateY(' + (top + offset.top - 28.189990) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([2]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);"/>'; // the combination of rotateY(180deg), negative translateX and rotateZ(45deg) flips the flag and rotates it 135deg, keeping the highlight edges at the top
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left)) + 'px) translateY(' + (top + offset.top) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 14.094995)) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 14.094995)) + 'px) translateY(' + (top + offset.top + 14.094995) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" width="92px" height="19.933333px" style="-webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 28.189990)) + 'px) translateY(' + (top + offset.top + 28.189990) + 'px) rotateZ(' + 45 + 'deg);"/>';
	 		if (flagStatus) {str += '<div style="position:absolute; left:0px; top:0px; width:92px; height:19.933333px; -webkit-transform: rotateY(' + 180 + 'deg) translateX(' + (-(left + offset.left + 28.189990)) + 'px) translateY(' + (top + offset.top + 28.189990) + 'px) rotateZ(' + 45 + 'deg);">' + flagStatus + '</div>';}
			}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		initCourse();
	}
	return str;
}*/
function getPattern15(){
	var str = "";
	var rotation = 45;	
	offsets = new Array({left:0, top:0}, { left:42, top:-21});
	var offsetID = 0;
	initCourse(true);
	for(var top=-68; top<785; top+=56){
	//for(var top=100; top<270; top+=58){
		var offset = offsets[offsetID];	
		for(var left = -100; left<1024; left+=84){
			if (rotation==45){
				str += '<img src="flag-' + getFlagID([-1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top - 14) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top - 14) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-2]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left + 28) + 'px; top:' + (top + offset.top - 28) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 28) + 'px; top:' + (top + offset.top - 28) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([2]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top + 1) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top + 1) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top + 15) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 14) + 'px; top:' + (top + offset.top + 15) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '-45.png" class="rotate' + rotation + '" width="78.5px" height="78.5px" style="left:' + (left + offset.left + 28) + 'px; top:' + (top + offset.top + 29) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 28) + 'px; top:' + (top + offset.top + 29) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==45) ? 135 : 45;
		if (offsetID==0) top += 16;
		initCourse();
	}
	return str;
}

function getPattern16(){
	var str = "";
	var rotation = 90;	
	offsets = new Array({left:0, top:46}, { left:96, top:36});
	var offsetID = 0;
	initCourse(true);
	for(var top=0; top<1250; top+=46){
		var offset = { left:offsets[offsetID].left, top:offsets[offsetID].top };	
		for(var left = -780; left<1024; left+=60){
			if (rotation==0){
				str += '<img src="flag-' + getFlagID([2]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 20) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 20) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 40) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 40) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left + 20) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 20) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-2]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left + 40) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left + 40) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}
			offset.top -= 60;
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==0) ? 90 : 0;
		if (rotation==90){
			offsets[0].left += 92;
			offsets[1].left += 92;
		}
		initCourse();
	}
	return str;
}

function getPattern17(){
	var str = "";
	var rotation = 0;	
	offsets = new Array({left:34, top:54}, { left:70, top:4});
	var offsetID = 0;
	initCourse(true);
	for(var top=-950; top<50; top+=46){
		var offset = { left:offsets[offsetID].left, top:offsets[offsetID].top };	
		for(var left = 0; left<1324; left+=60){
			if (rotation==0){
				str += '<img src="flag-' + getFlagID([2]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 40) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 40) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 20) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top - 20) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}else{
				str += '<img src="flag-' + getFlagID([-1]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left - 40) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left - 40) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID() + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left - 20) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left - 20) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
				str += '<img src="flag-' + getFlagID([-2]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			}
			offset.top += 60;
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==0) ? 90 : 0;
		if (rotation==90){
			offsets[0].left -= 92;
			offsets[1].left -= 92;
		}
		initCourse();
	}
	return str;
}

function getPattern18(){
	var str = "";
	var rotation = 0;	
	offsets = new Array({left:0, top:46}, { left:36, top:56});
	var offsetID = 0;
	var double = true;
	initCourse(true);
	for(var top=0; top<1750; top+=46){
		var offset = { left:offsets[offsetID].left, top:offsets[offsetID].top };	
		for(var left = -100; left<1024; left+=20){
			str += '<img src="flag-' + getFlagID(offsetID == 0 ? [-4, -3] : [-1, 0]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
			if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
			offset.top -= 20;
		}
		if (rotation==90){
			if (double){
				initCourse();
				top += 92;
				var offset = { left:offsets[offsetID].left, top:offsets[offsetID].top };	
				for(var left = -100; left<1024; left+=20){
					str += '<img src="flag-' + getFlagID([0]) + '.jpg" class="rotate' + rotation + '" width="92px" height="20px" style="left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px;"/>';
	 		 if (flagStatus) {str += '<div class="rotate' + rotation + '" style="position:absolute; left:' + (left + offset.left) + 'px; top:' + (top + offset.top) + 'px; width:92px; height:20px;">' + flagStatus + '</div>';}
					offset.top -= 20;
				}
			}
			double = !double;
		}
		offsetID++;
		if (offsetID>=offsets.length) offsetID=0;
		rotation = (rotation==0) ? 90 : 0;
		if (rotation==0){
			top += 80;
			offsets[0].left += 12;
			offsets[1].left += 12;
		}
		initCourse();
	}
	return str;
}
