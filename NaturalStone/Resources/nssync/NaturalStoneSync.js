/**
 * NaturalStoneSync: LocalStorage for the project library
 */

var NaturalStoneSync = (function(){
    var _kclientProjectId = "ClientId";
    return {
        _syncTimer: 2000,
        _areWeOnline: true,
        _log: '',
        _iOSActive: false,
        _pullProjects: true,
        _syncing: false,
        _lastSyncTime: null,
        _maxLibraryAge: 20000,
        _doneProjectSync: false,
        //_defaultAPIBase: "http://marshallsstaging.dolphin.uk.com/naturalstone/",
        //_defaultAPIBase: "http://newsite.marshalls-etest.co.uk/naturalstoneapp/Services/Api/",
        _defaultAPIBase: "http://www.marshalls.co.uk/naturalstoneapp/api/",
        _defaultAPIPassword: "M4r5ha11s!",
        _previousData:'',

        setiOSActive: function(api_base, api_password, clear_local_storage) {
            NaturalStoneSync._iOSActive = true;
            NaturalStoneSync._defaultAPIBase = api_base;
            NaturalStoneSync._defaultAPIPassword = api_password;
            if (clear_local_storage) {
                localStorage.clear();
            }
        },
        log: function() {
            NaturalStoneSync._log += Array.prototype.join.call(arguments, ' ')+"\n";
            if (NaturalStoneSync._iOSActive) {
                window.location='log:///';
                //console.log(NaturalStoneSync._log);
            } else {
                console.log.call(console, arguments);
            }
        },
        purgeLog: function() {
            var log = NaturalStoneSync._log;
            NaturalStoneSync._log = '';
            return log;
        },
        getProjectLibrary: function() {
            //NaturalStoneSync.forceSync();
            // Irrespective of what the server has, return what we have right now
            var library = NaturalStoneSync._getLocal();
            var active_library = [];
            for (var i = 0, l = library.length; i < l; ++i) {
                if (!library[i]["IsDeleted"]) {
                    active_library.push(library[i]);
                }
            }
            return active_library;
        },
        
        forceSync: function() {
            NaturalStoneSync._pullProjects = true;
        },

        createProject: function(project) {
            // Update our local copy immediately;
            project["unsynced"] = true;

            var projects = NaturalStoneSync._getLocal();
            projects.unshift(project);
            NaturalStoneSync._setLocal(projects);
            NaturalStoneSync.forceSync();
            
            return projects;
        },

        queueEmail: function(action, clientProjectId, paramObj) {
            // This will get reset to zero below
            var nextID = NaturalStoneSync._getLocalEmailsID()+1;
            localStorage.setItem('NaturalStorageEmailID',nextID);
            var queue = NaturalStoneSync._getLocalEmails();
            NaturalStoneSync.log('queue');
            var quote = false;
            if (action == 'sendEmail') {
                action = 'ProjectEmail';
            } else if (action == 'sendQuote') {
                action = 'ProjectEmail';
                quote = true;
            } else {
                // Ignore
                return;
            }
            var email = {
                "ID" : nextID,
                "action" : action,
                "paramObj" : paramObj };
            email[_kclientProjectId] = clientProjectId,
            queue.push( email );
            NaturalStoneSync.log('queue '+JSON.stringify(queue[queue.length-1]));
            NaturalStoneSync._setLocalEmails(queue);
            NaturalStoneSync.forceSync();
        },

        sendEmail: function(paramObj) {
            NaturalStoneSync._sendRequest('POST', 'ProjectEmail', paramObj, function(resp, text) {
                if (NaturalStoneSync._iOSActive) {
                    window.location = 'emailsuccess://';
                } else {
                    alert('Email response - '+text);
                }
            }, function() {
                if (NaturalStoneSync._iOSActive) {
                    window.location = 'emailfailed://';
                } else {
                    alert( 'Email send failed with bad HTTP status code' );
                }
            });
        },
        sendQuote: function(paramObj) {
            NaturalStoneSync._sendRequest('POST', 'ProjectEmail', paramObj, function(resp, text) {
                if (NaturalStoneSync._iOSActive) {
                    NaturalStoneSync.log('resp='+resp);
                    NaturalStoneSync.log('text='+text);
                    window.location = 'emailsuccess://';
                } else {
                    alert('Email response - '+text);
                }
            }, function() {
                if (NaturalStoneSync._iOSActive) {
                    window.location = 'emailfailed://';
                } else {
                    alert( 'Email send failed with bad HTTP status code' );
                }
            });
        },
        deleteProject: function(clientProjectId) {
            var projects = NaturalStoneSync._getLocal();
            var remainingProjects = [];
            var deletedProjects = NaturalStoneSync._getLocalDeleted();
            console.log(deletedProjects);
            console.log('projects');
            console.log(projects);
            for (var i in projects) {
                if (projects[i][_kclientProjectId] == clientProjectId) {
                    // Update our local copy immediately
                    var deletedProject = projects[i];
                    deletedProjects.push(deletedProject);
                } else {
                    remainingProjects.push(projects[i]);
                }
            }
            console.log('remaining projects');
            console.log(remainingProjects);
            NaturalStoneSync._setLocalDeleted(deletedProjects);
            NaturalStoneSync._setLocal(remainingProjects);
            NaturalStoneSync.forceSync();
            return remainingProjects;
        },

        hasUnsavedChanges: function() {
            if (NaturalStoneSync._syncing)
                return true;

            // We don't need to check if a project has already been dealt
            // with within a sync as we won't be within a sync when we get her,
            // this applies to each of the checks below.

            // check for unsynced deleted projects, 
            var deletedProjects = NaturalStoneSync._getLocalDeleted();
            if (deletedProjects.length)
                return true;

            var projects = NaturalStoneSync._getLocal();
            var pushedProjects = NaturalStoneSync._getLocalPushedProjects();

            for (var i = 0, l = projects.length; i < l; ++i) {
                if (projects[i]["unsynced"]) {
                    return true;
                }
            }

            var emails = NaturalStoneSync._getLocalEmails();
            if (emails.length)
                return true;

            return false;
        },

        _getLocal: function() {
            var projects = JSON.parse(localStorage.getItem('NaturalStorageProjects'));
            if (!projects)
                projects = [];
            return projects;
        },

        _setLocal: function(obj) {
            localStorage.setItem('NaturalStorageProjects', JSON.stringify(obj));
        },

        _getLocalEmailsID: function() {
            var nextID = Number(localStorage.getItem('NaturalStorageEmailID'));
            if (isNaN(nextID))
                nextID = 0;
            return nextID;
        },

        _setLocalEmailsID: function(num) {
            localStorage.setItem('NaturalStorageEmails', num);
        },

        _getLocalEmails: function() {
            var emails = JSON.parse(localStorage.getItem('NaturalStorageEmails'));
            if (!emails)
                emails = [];
            return emails;
        },

        _setLocalEmails: function(obj) {
            localStorage.setItem('NaturalStorageEmails', JSON.stringify(obj));
        },

        _getLocalDeleted: function() {
            var projects = JSON.parse(localStorage.getItem('NaturalStorageDeletedProjects'));
            if (!projects)
                projects = [];
            return projects;
        },

        _setLocalDeleted: function(obj) {
            localStorage.setItem('NaturalStorageDeletedProjects', JSON.stringify(obj));
        },

        _getLocalPushedProjects: function() {
            return JSON.parse(localStorage.getItem('NaturalStoragePushedProjects'));
        },

        _setLocalPushedProjects: function(obj) {
            localStorage.setItem('NaturalStoragePushedProjects', JSON.stringify(obj));
        },

        _getLocalFullProjects: function() {
            return JSON.parse(localStorage.getItem('NaturalStorageFullProjects'));
        },

        _setLocalFullProjects: function(obj) {
            localStorage.setItem('NaturalStorageFullProjects', JSON.stringify(obj));
        },

        isoDate: function() {
            return ((new Date()).toISOString());
        },

        authToken: function() {
            var token = { "Password" : NaturalStoneSync._defaultAPIPassword, "TokenGeneratedDate" : NaturalStoneSync.isoDate() };
            NaturalStoneSync.log(JSON.stringify(token));
            var encrypted_token = btoa(des('B885E4CECE6BE86C0AC9B013',JSON.stringify(token),true,1,'BC52B04E',1));
            NaturalStoneSync.log(encrypted_token);
            return encrypted_token;
        },

        _sendRequest: function(method, api, data, callback, errorcallback) {
            var endpoint = NaturalStoneSync._defaultAPIBase;
//            var endpoint = 'http://richardlaptop.local/naturalstone-svc/MarshallsWCF.svc/json/';
            var djson = data ? JSON.stringify(data) : '';
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                //NaturalStoneSync.log("readyState="+xhr.readyState);
                if (xhr.readyState == 4 ) {
                        
                    if(NaturalStoneSync._previousData.length == 0 || NaturalStoneSync._previousData != xhr.responseText){
                        
                        //NaturalStoneSync.log("duylept parse sync if having change");
                        
                        NaturalStoneSync._previousData   =   xhr.responseText
                        NaturalStoneSync.log("got status "+xhr.status);
                        NaturalStoneSync.log(":"+xhr.responseText+":");
                        if (xhr.status >= 200 && xhr.status < 300) {
                            var json = null;
                            if (xhr.responseText) {
                                try {
                                    json = JSON.parse(xhr.responseText);
                                } catch(e) {
                                    NaturalStoneSync.log("Parsing json gave: "+e);
                                }
                            }
                            callback.call({}, json, xhr.responseText);
                        } else {
                            if (errorcallback) {
                                errorcallback(xhr);
                            }
                        }
                    }
                }
            }

            xhr.open(method, endpoint + api, true);
            xhr.setRequestHeader('Authentication-Token', NaturalStoneSync.authToken());
            xhr.setRequestHeader('Accept', 'text/json');
            xhr.setRequestHeader('Content-Type', 'text/json');
            xhr.setRequestHeader('Content-Length', djson.length);
            NaturalStoneSync.log('Requesting using '+method+': '+endpoint+api+' '+djson.length +' chars : '+djson);
            xhr.send(djson);
        },

        _syncLibrary: function() {
            if (NaturalStoneSync._syncing)
                return;
            console.log('pullProjects='+NaturalStoneSync._pullProjects);
            if (!NaturalStoneSync._pullProjects && NaturalStoneSync._lastSyncTime && ((new Date()) - NaturalStoneSync._lastSyncTime) < NaturalStoneSync._maxLibraryAge) {
                return;
            }
            NaturalStoneSync.log((new Date()) - NaturalStoneSync._lastSyncTime);
            NaturalStoneSync.log(NaturalStoneSync._maxLibraryAge);
            NaturalStoneSync.log( 'c' );
            NaturalStoneSync._pullProjects = false;
            NaturalStoneSync._syncing = true;
            NaturalStoneSync._doneProjectSync = false;
            // Just make sure it's blank
            NaturalStoneSync._setLocalPushedProjects({});

            NaturalStoneSync.log('here1');
            NaturalStoneSync._performNextSyncAction();
        },

        _performNextSyncAction: function() {
            NaturalStoneSync.log( 'd' );
            var deletedProjects = NaturalStoneSync._getLocalDeleted();
            NaturalStoneSync.log('here10');
            /*
            while (deletedProjects.length && deletedProjects[0].id == 0) {
                // Ignore any projects that were never saved
                deletedProjects.shift();
            }
            */
            if (deletedProjects.length) {
                NaturalStoneSync.log('delete one');
                var successfulDeleteCallback = function(){
                        var deletedProjects = NaturalStoneSync._getLocalDeleted();
                        var newDeletedProjects = []
                        NaturalStoneSync.log('removing from deleted projects');
                        for (var i = 0, l = deletedProjects.length; i < l; ++i) {
                            if (deletedProjects[i][_kclientProjectId] != deletedProject[_kclientProjectId]) {
                                newDeletedProjects.push(deletedProjects[i]);
                            }
                        }
                        NaturalStoneSync.log(deletedProjects);
                        NaturalStoneSync._setLocalDeleted(newDeletedProjects);

            NaturalStoneSync.log('here2');
                        setTimeout(NaturalStoneSync._performNextSyncAction, 1);
                    }
                var deletedProject = deletedProjects[0];
                NaturalStoneSync._sendRequest('DELETE', 'Project/'+deletedProject[_kclientProjectId], {
                    }, successfulDeleteCallback, function(xhr) {
                        if (xhr.status == 404) {
                            // Ignore this, it's fine
                            successfulDeleteCallback();
                        } else {
                            NaturalStoneSync._syncing = false;
                        }
                    });
                return;
            }
            NaturalStoneSync.log('here11');
            var projects = NaturalStoneSync._getLocal();
            NaturalStoneSync.log('here12');
            var pushedProjects = NaturalStoneSync._getLocalPushedProjects();
            NaturalStoneSync.log('here13');
            if (!projects)
                projects = []
            NaturalStoneSync.log("pushedProjects: "+JSON.stringify(pushedProjects));
            for (var i = 0, l = projects.length; i < l; ++i) {
                // Push the first unpushed project
                if (projects[i]["unsynced"] && ! pushedProjects[projects[i][_kclientProjectId]] ) {
                    NaturalStoneSync.log('push one');
                    NaturalStoneSync.log('project: '+JSON.stringify(projects[i]));
                    delete projects[i]["unsynced"];
                    NaturalStoneSync._sendRequest('POST', 'Project', projects[i], function() {
                        pushedProjects[projects[i][_kclientProjectId]] = 1;
                        NaturalStoneSync._setLocalPushedProjects(pushedProjects);

            NaturalStoneSync.log('here3');
                        NaturalStoneSync._setLocal(projects);
                        setTimeout(NaturalStoneSync._performNextSyncAction, 1);
                    }, function(xhr) {
                        //NaturalStoneSync.log(
                          var json = null;
                          if (xhr.responseText) {
                              try {
                                json = JSON.parse(xhr.responseText);
                              } catch(e) {
                                NaturalStoneSync.log("Parsing json gave: "+e);
                              }
                          }
                        NaturalStoneSync.log("Message: " + json.Message);
                        if (json.Message=="An error has occurred."){
                            NaturalStoneSync.log(json.Message + " projects count:" + projects.length);
                            NaturalStoneSync._syncing = false;
                            //Remove dodgy project
                            projects.splice(i, 1);
                            NaturalStoneSync.log("Removed dodgy project projects count:" + projects.length);
                        }else{
                            NaturalStoneSync._syncing = false;
                            projects[i]["unsynced"] = true;
                            NaturalStoneSync.log("Failed to sync, abandoning sync round.");
                        }
                        NaturalStoneSync._setLocal(projects);
                    });
                    return;
                }
            }

            // We've deleted everything we need to and saved everything we need to
            NaturalStoneSync.log("FINISHED SYNC");
            /*Start PKT added @ 25 Nov 2014 to fix syncing issue when created or updated project to library*/
            NaturalStoneSync._syncing = false;
            /*End PKT added*/
            pushedProjects = {};
            NaturalStoneSync._setLocalPushedProjects(pushedProjects);

            if (!NaturalStoneSync._doneProjectSync) {
                NaturalStoneSync._sendRequest('GET', 'Project', '', function(resp) {
                    var existing = NaturalStoneSync._getLocal();
                    var existingHash = {}
                    NaturalStoneSync.log( 'here - a');
                    // Put existing unsaved entries into a hash for convenience
                    for ( var i = 0, l = existing.length; i < l; ++i ) {
                        existingHash[existing[i][_kclientProjectId]] = existing[i];
                    }
                    NaturalStoneSync.log( 'here - b');
                    // Go over the new library, delete any entries from existing that are on the server
                    for (var i = 0, l = resp.length; i < l; ++i) {
                        existingHash[resp[i][_kclientProjectId]] = null;
                    }
                    NaturalStoneSync.log( 'here - c');
                    // Now add any that haven't been synced
                    for (var clientProjectId in existingHash) {
                    NaturalStoneSync.log( 'here - c1');
                        if (existingHash[clientProjectId] && existingHash[clientProjectId]["unsynced"]) {
                    NaturalStoneSync.log( 'here - c2');
                            NaturalStoneSync.log('adding '+existingHash[clientProjectId]);
                    NaturalStoneSync.log( 'here - c3');
                            resp.unshift(existingHash[clientProjectId]);
                    NaturalStoneSync.log( 'here - c4');
                        }
                    }
                    NaturalStoneSync.log( 'here - d');
                    NaturalStoneSync._setLocal(resp);
                    NaturalStoneSync.log('localStorage updated to', NaturalStoneSync._getLocal());
                    NaturalStoneSync.log( 'here');

                    NaturalStoneSync._doneProjectSync = true;
                    NaturalStoneSync._syncing = false;
                    setTimeout(NaturalStoneSync._performNextSyncAction, 1);
                }, function() {
                    NaturalStoneSync._syncing = false;
                });
                return;
            }

            // Check for queued emails
            var emails = NaturalStoneSync._getLocalEmails();
            var waitingForImages = false;
            for (var i = 0, l = emails.length; i < l; ++i) {
                var library = NaturalStoneSync._getLocal();
                var libraryProject;
            NaturalStoneSync.log('here15');
                NaturalStoneSync.log("email: "+JSON.stringify(emails[i]));
                for (var j = 0, jl = library.length; j < jl; ++j) {
                    if(library[j][_kclientProjectId] == emails[i][_kclientProjectId]) {
                        libraryProject = library[j];
                        break;
                    }
                }
            NaturalStoneSync.log('here16');
                if (!libraryProject) {
                    // If it's not in the library it must have been deleted, ignore it
                    NaturalStoneSync.log('Ignoring from queue '+JSON.stringify(emails[i]));
                    continue;
                }
            NaturalStoneSync.log('here17');
                if (libraryProject["unsynced"] || !libraryProject['LargeImageUrl']) {
                    // Image hasn't synced yet, try later
                    waitingForImages = true;
                    break;
                }
            NaturalStoneSync.log('here19');
                NaturalStoneSync.log('emailing from queue '+JSON.stringify(emails[i]));
                emails[i].action = "ProjectEmail";
                NaturalStoneSync._sendRequest('POST', emails[i]['action'], emails[i]['paramObj'], function(resp, text) {
                        // success
                        emails.shift();
                        NaturalStoneSync._setLocalEmails(emails);

                        setTimeout(NaturalStoneSync._performNextSyncAction, 1);
                }, function(xhr) {
                    if (xhr.status == 409) {
                        // Make sure it pauses
                        NaturalStoneSync._lastSyncTime = new Date();
                    }
                    NaturalStoneSync._syncing = false;
                });
                return;
            }
            // Images won't sync until the update:// happens so we need to do that
            // once and push the emails on the next round
            if (!waitingForImages) {
                NaturalStoneSync.log("Queue now "+JSON.stringify(emails));
                // No valid emails, so clear store
                NaturalStoneSync._setLocalEmailsID(0);
                NaturalStoneSync._setLocalEmails([]);
            }

            NaturalStoneSync._lastSyncTime = new Date();
            NaturalStoneSync._syncing = false;
            if (NaturalStoneSync._iOSActive ) {
                window.location = 'update://';
            }
         },
    };
})();
setInterval(NaturalStoneSync._syncLibrary, NaturalStoneSync._syncTimer);
