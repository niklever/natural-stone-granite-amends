//
//  DMPreviewViewController.m
//  NaturalStone
//
//  Created by John McKerrell on 08/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMPreviewViewController.h"

#import "ImageScrollView.h"
#import <QuartzCore/QuartzCore.h>

@implementation DMPreviewViewControllerBase

@synthesize project = __project;

@end

@interface DMPreviewViewController ()

@property (nonatomic, strong) IBOutlet ImageScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIWebView *previewView;
@property (nonatomic, strong) IBOutlet UIView *activityWrapperView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrContain;
@property (weak, nonatomic) IBOutlet UIImageView *imvToZoom;

- (void)savePreviewImage;

@end

@implementation DMPreviewViewController

@synthesize scrollView = __scrollView;
@synthesize previewView = __previewView;
@synthesize activityWrapperView = __activityWrapperView;
@synthesize activityView = __activityView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"PREVIEW_TITLE", @"");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.activityWrapperView.layer.cornerRadius = 10.0f;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Email" 
                                                                               style:UIBarButtonItemStyleDone 
                                                                              target:nil
                                                                              action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                             target:self.navigationController
                                                                                             action:@selector(saveAndCloseProject)];
    //self.scrollView = [[ImageScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //[self.view insertSubview:self.scrollView belowSubview:self.activityWrapperView];
    self.previewView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.previewView.delegate = self;
    self.previewView.scalesPageToFit = YES;
    [self.view insertSubview:self.previewView belowSubview:self.activityWrapperView];
    
    DMAppDelegate *appDelegate = (DMAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.window.viewToObserve = self.previewView;
    appDelegate.window.controllerThatObserves = self;
//    CALayer *testLayer1 = [CALayer layer];
//    testLayer1.frame = self.testView1.frame;   
//    testLayer1.contents = (id)[UIImage imageNamed:@"200x100-gra901.jpg"].CGImage;
//    [self.view.layer addSublayer:testLayer1];
//    CALayer *testLayer2 = [CALayer layer];
//    testLayer2.frame = self.testView2.frame;   
//    testLayer2.contents = (id)[UIImage imageNamed:@"200x100-gra901.jpg"].CGImage;
//    [self.view.layer addSublayer:testLayer2];
//    CALayer *testLayer3 = [CALayer layer];
//    testLayer3.frame = self.testView3.frame;   
//    testLayer3.contents = (id)[UIImage imageNamed:@"200x100-gra901.jpg"].CGImage;
//    [self.view.layer addSublayer:testLayer3];
    
    //[self.scrContain setMaximumZoomScale:5.0];
    //[self.scrContain setMinimumZoomScale:1.1];
    //[self.scrContain setZoomScale:1.01];
    UITapGestureRecognizer *gesture =   [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self.imvToZoom addGestureRecognizer:gesture];
    [self.imvToZoom setUserInteractionEnabled:YES];
    gesture =   nil;
    
    [kDMNavigationControllerRef.navBar showNavigationBar];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.scrollView = nil;
    self.previewView = nil;
    DMAppDelegate *appDelegate = (DMAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.window.viewToObserve = nil;
    appDelegate.window.controllerThatObserves = nil;
}

- (void)setPatternPreview:(NSString*)patternPreview {
    [self view];
//    self.scrollView.contentSize = self.previewView.frame.size;
//    [self.previewView.layer addSublayer:patternPreview];
    
//    [self.scrollView displayImage:patternPreview];
    [self.previewView loadHTMLString:patternPreview baseURL:[[NSBundle mainBundle] bundleURL]];
}

- (void)setProject:(DMProject *)project {
    [super setProject:project];
    
    [self view];
    NSMutableString *html = (NSMutableString*)[self.project patternHTML];
    NSRange start = [html rangeOfString:@"<div style"];
    if (start.location!=NSNotFound){
        NSRange end = [html rangeOfString:@";\"" options:0 range:NSMakeRange(start.location, 200)];
        if (end.location != NSNotFound){
            NSRange styleRange = NSMakeRange(start.location, end.location-start.location);
            NSString *sub = [html substringWithRange:styleRange];
            NSString *styleStr = [NSString stringWithFormat:@"%@; -webkit-transform: translateZ(0.1px)", sub];
            html = (NSMutableString*)[html stringByReplacingOccurrencesOfString:sub withString:styleStr];
        }
    }
    NSString *oldScaleStr = @"minimum-scale=0.5, maximum-scale=5";
    NSString *newScaleStr = @"minimum-scale=0.2, maximum-scale=2";
    html = (NSMutableString*)[html stringByReplacingOccurrencesOfString:oldScaleStr withString:newScaleStr];
    NSLog(html);
    
    self.activityWrapperView.hidden = NO;
    [self.previewView loadHTMLString:html baseURL:[[NSBundle mainBundle] bundleURL]];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (BOOL)updateProjectProperties:(BOOL)validate {
    //if (validate && self.activityView.isAnimating) {
    //    return NO;
    //}
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [webView scalesPageToFit];
    self.activityWrapperView.hidden = YES;
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(savePreviewImage) userInfo:nil repeats:NO];

}

- (void)savePreviewImage {
    
    UIGraphicsBeginImageContext(self.previewView.bounds.size);
    [self.previewView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (!self.project.saved) {
        [self.project setProjectImage:viewImage];
    }
    
    self.imvToZoom.image    =   viewImage;

    
    /*[UIView animateWithDuration:0.5f animations:^{
        self.activityWrapperView.alpha = 0.0;
        self.previewView.alpha =   0.0;
        self.scrContain.alpha   =   1.0;
    } completion:^(BOOL finished) {
        [self.activityView stopAnimating];
        self.previewView.hidden =   YES;
    }];*/
}

- (void)userDidTapWebView {
    [UIView animateWithDuration:0.375f animations:^{
        [kDMNavigationControllerRef.navBar showNavigationBar];
    }];
}

- (void)userDidMoveWebView {
    [UIView animateWithDuration:0.375f animations:^{
        [kDMNavigationControllerRef.navBar hideNavigationBar];
    }];
}

#pragma mark - handle gesture
- (void) handleTapGesture:(UITapGestureRecognizer *)gesture{
    if (gesture.state == UIGestureRecognizerStateEnded) {
        [UIView animateWithDuration:0.375f animations:^{
            [kDMNavigationControllerRef.navBar showNavigationBar];
        }];
    }
}

#pragma mark - UISCROLLVIEW DELEGATE
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    [UIView animateWithDuration:0.375f animations:^{
        [kDMNavigationControllerRef.navBar hideNavigationBar];
    }];
    return self.imvToZoom;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [UIView animateWithDuration:0.375f animations:^{
        [kDMNavigationControllerRef.navBar hideNavigationBar];
    }];
}

@end
