//
//  DMPreviewViewController.h
//  NaturalStone
//
//  Created by John McKerrell on 08/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMProjectViewControllerBase.h"

@class DMProject;

@interface DMPreviewViewControllerBase : DMProjectViewControllerBase

@property (nonatomic,strong) DMProject *project;

@end

@interface DMPreviewViewController : DMPreviewViewControllerBase <UIWebViewDelegate,DMTapDetectingWindowDelegate>

@end
