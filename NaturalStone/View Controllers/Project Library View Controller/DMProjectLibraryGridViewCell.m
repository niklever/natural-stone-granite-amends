//
//  DMProjectLibraryGridViewCell.m
//  NaturalStone
//
//  Created by John McKerrell on 10/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMProjectLibraryGridViewCell.h"

#import <QuartzCore/QuartzCore.h>
#import "NSDate+parseXMLDate.h"

@implementation DMProjectLibraryGridViewCellBase

@synthesize project = __project;

@end

@interface DMProjectLibraryGridViewCell ()

@property (nonatomic,strong) IBOutlet UIView *projectView;
@property (nonatomic,strong) IBOutlet UIButton *duplicateButton;
@property (nonatomic,strong) IBOutlet UIButton *deleteButton;
@property (nonatomic,strong) IBOutlet UIButton *thumbnailImageView;
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UILabel *locationLabel;
@property (nonatomic,strong) IBOutlet UILabel *dateLabel;

- (IBAction)buttonTapped:(id)sender;

@end

@implementation DMProjectLibraryGridViewCell

@synthesize projectView = __projectView;
@synthesize duplicateButton = __duplicateButton;
@synthesize deleteButton = __deleteButton;
@synthesize thumbnailImageView = __thumbnailImageView;
@synthesize titleLabel = __titleLabel;
@synthesize locationLabel = __locationLabel;
@synthesize dateLabel = __dateLabel;
@synthesize delegate = __delegate;

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [[NSBundle mainBundle] loadNibNamed:@"DMProjectLibraryGridViewCell" owner:self options:nil];
        [self.contentView addSubview:self.projectView];
        self.thumbnailImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.thumbnailImageView.layer.borderWidth = 1.0f;
        self.thumbnailImageView.layer.cornerRadius = 5.0f;
        self.thumbnailImageView.layer.masksToBounds = YES;
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        
        self.selectionStyle = AQGridViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setProject:(DMProject *)project {
    [super setProject:project];
    
    self.titleLabel.text = [self.project.properties objectForKey:kDMProjectPropertyTitle];
    self.locationLabel.text = [self.project.properties objectForKey:kDMProjectPropertyLocation];
    NSString *dateProperty = [self.project.properties objectForKey:kDMProjectPropertyDate];
    NSDate *date = [NSDate parseXMLDate:dateProperty];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    self.dateLabel.text = @"";
    if (date) {
        self.dateLabel.text = [dateFormatter stringFromDate:date];
    }
    
    self.titleLabel.font = [UIFont fontWithName:kDMDefaultFont size:15.0f];
    self.locationLabel.font = [UIFont fontWithName:kDMDefaultFont size:15.0f];
    self.dateLabel.font = [UIFont fontWithName:kDMDefaultFont size:15.0f];
    
    CGSize titleSize = [self.titleLabel.text sizeWithFont:self.titleLabel.font constrainedToSize:CGSizeMake(self.titleLabel.frame.size.width, 1000)];
    if (titleSize.height > 19.0f) {
        CGRect frame = self.titleLabel.frame;
        frame.size.height = 36.0f;
        self.titleLabel.frame = frame;
        self.titleLabel.numberOfLines = 2;
        
        frame = self.locationLabel.frame;
        frame.origin.y = 52.0f;
        self.locationLabel.frame = frame;
        
        frame = self.dateLabel.frame;
        frame.origin.y = 74.0f;
        self.dateLabel.frame = frame;
    } else {
        CGRect frame = self.titleLabel.frame;
        frame.size.height = 18.0f;
        self.titleLabel.frame = frame;
        self.titleLabel.numberOfLines = 1;
        
        frame = self.locationLabel.frame;
        frame.origin.y = 34.0f;
        self.locationLabel.frame = frame;
        
        frame = self.dateLabel.frame;
        frame.origin.y =  56.0f;
        self.dateLabel.frame = frame;
    }
    
    self.duplicateButton.hidden = NO;
    self.duplicateButton.layer.cornerRadius = 3.0;
    self.deleteButton.layer.cornerRadius = 3.0;
    
    NSArray *projectColours = DMProject.coloursData;
    
    for (NSDictionary *colour in self.project.colours) {
        if ([self.project.type isEqualToString:@"Stonespar"]){
            self.duplicateButton.hidden = YES;
            break;
        }
        NSString *ref = [colour objectForKey:@"Reference"];
        if ([ref hasPrefix:@"gra9"]){
            bool discontinued = NO;
            for (NSDictionary *projectColour in projectColours) {
                NSString *ref1 = [projectColour objectForKey:@"ref"];
                if ([ref1 isEqualToString:ref]){
                    NSNumber *num = [projectColour objectForKey:@"discontinued"];
                    if (num!=NULL && num){
                        discontinued = true;
                        break;
                    }
                }
            }
            if (discontinued){
                self.duplicateButton.hidden = YES;
                break;
            }
        }
    }
    UIImage *thumbnail = [project thumbnailImage];
    if (thumbnail) {
        [self.thumbnailImageView setImage:thumbnail forState:UIControlStateNormal];
    } else {
        [self.thumbnailImageView setImage:[UIImage imageNamed:@"project-thumb.jpg"] forState:UIControlStateNormal];
    }
    
    NSString *uuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *itemUUID = [project.properties objectForKey:kDMProjectPropertyUser];
    
    NSLog(@"UUID:%@ itemUUID:%@", uuid, itemUUID);
    
    if ([uuid isEqualToString:itemUUID]) {
        self.deleteButton.hidden = NO;
    } else {
        self.deleteButton.hidden = YES;
    }
}

- (void)buttonTapped:(id)sender {
    if (sender == self.duplicateButton) {
        [self.delegate projectLibraryGridViewCellDuplicateTapped:self];
    } else if (sender == self.deleteButton) {
        [self.delegate projectLibraryGridViewCellDeleteTapped:self];
    } else if (sender == self.thumbnailImageView) {
        [self.delegate projectLibraryGridViewCellThumbnailTapped:self];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
