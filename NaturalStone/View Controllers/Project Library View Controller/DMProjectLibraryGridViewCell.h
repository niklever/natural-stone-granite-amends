//
//  DMProjectLibraryGridViewCell.h
//  NaturalStone
//
//  Created by John McKerrell on 10/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AQGridViewCell.h"

#import "DMProject.h"

@class DMProjectLibraryGridViewCell;

@protocol DMProjectLibraryGridViewCellDelegate <NSObject>

- (void)projectLibraryGridViewCellDuplicateTapped:(DMProjectLibraryGridViewCell*)projectLibraryGridViewCell;
- (void)projectLibraryGridViewCellDeleteTapped:(DMProjectLibraryGridViewCell*)projectLibraryGridViewCell;
- (void)projectLibraryGridViewCellThumbnailTapped:(DMProjectLibraryGridViewCell*)projectLibraryGridViewCell;

@end

@interface DMProjectLibraryGridViewCellBase : AQGridViewCell

@property (nonatomic,strong) DMProject *project;

@end

@interface DMProjectLibraryGridViewCell : DMProjectLibraryGridViewCellBase

@property (nonatomic,weak) id<DMProjectLibraryGridViewCellDelegate> delegate;

@end
