//
//  DMProjectLibraryViewController.m
//  NaturalStone
//
//  Created by John McKerrell on 10/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMProjectLibraryViewController.h"

#import "DMImageCacher.h"

@interface DMProjectLibraryViewController ()

@property (nonatomic, strong) IBOutlet UILabel *noProjectsLabel;

@property (nonatomic, strong) AQGridView *libraryGridView;
@property (nonatomic, strong) NSArray *projectLibrary;
@property (nonatomic, strong) NSArray *filteredLibrary;
@property (nonatomic, strong) UIAlertView *deleteAlertView;
@property (nonatomic, strong) UIAlertView *duplicateAlertView;
@property (nonatomic, strong) DMProject *alertProject;
@property (nonatomic, strong) NSString *searchText;

-(void)keyboardWillShow:(NSNotification *)n;
-(void)keyboardWillHide:(NSNotification *)n;

@end

@implementation DMProjectLibraryViewController

@synthesize noProjectsLabel = __noProjectsLabel;
@synthesize libraryGridView = __libraryGridView;
@synthesize projectLibrary = __projectLibrary;
@synthesize filteredLibrary = __filteredLibrary;
@synthesize deleteAlertView = __deleteAlertView;
@synthesize duplicateAlertView = __duplicateAlertView;
@synthesize alertProject = __alertProject;
@synthesize searchText = __searchText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"PROJECT_LIBRARY_TITLE",@"");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BackGround.png"]];
//    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:[[[UIView alloc] initWithFrame:CGRectZero] autorelease]] autorelease];
    
    DMNavigationController *nav = kDMNavigationControllerRef;
    
    self.projectLibrary = kDMNavigationControllerRef.projectLibrary;
    self.filteredLibrary = self.projectLibrary;
    
    self.libraryGridView = [[AQGridView alloc] initWithFrame:CGRectMake(21, 0, 1003, 756)];
    self.libraryGridView.contentInset = UIEdgeInsetsMake(70, 0, 0, 0);
    self.libraryGridView.delegate = self;
    self.libraryGridView.dataSource = self;
    [self.view addSubview:self.libraryGridView];

    self.noProjectsLabel.text = NSLocalizedString(@"NO_PROJECTS_TITLE", @"");
    self.noProjectsLabel.font = [UIFont fontWithName:kDMDefaultFont size:50.0];
    
    [self updateData];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateData)
                                                 name:kDMProjectSyncCompletedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateData)
                                                 name:kDMImageSyncCompletedNotification
                                               object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)n
{
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    NSValue* boundsValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    CGSize keyboardSize = [boundsValue CGRectValue].size;
    NSNumber* durationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    [UIView animateWithDuration:[durationValue floatValue] animations:^{
        CGRect frame = self.libraryGridView.frame;
        frame.size.height = 736 - keyboardSize.height;
        self.libraryGridView.frame = frame;
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    kDMNavigationControllerRef.navBar.searchDelegate = self;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    kDMNavigationControllerRef.navBar.searchDelegate = nil;
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    NSNumber* durationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];

    [UIView animateWithDuration:[durationValue floatValue] animations:^{
        CGRect frame = self.libraryGridView.frame;
        frame.size.height = 736;
        self.libraryGridView.frame = frame;
    }];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.noProjectsLabel = nil;
    self.libraryGridView = nil;
    self.projectLibrary = nil;
    [self.deleteAlertView dismissWithClickedButtonIndex:self.deleteAlertView.cancelButtonIndex animated:NO];
    self.deleteAlertView = nil;
    [self.duplicateAlertView dismissWithClickedButtonIndex:self.duplicateAlertView.cancelButtonIndex animated:NO];
    self.duplicateAlertView = nil;
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)updateData {
    self.projectLibrary = kDMNavigationControllerRef.projectLibrary;
    [self customNavBar:nil searchTextUpdated:self.searchText];

    if ([self.projectLibrary count]) {
        self.libraryGridView.hidden = NO;
        self.noProjectsLabel.hidden = YES;
        
        [self.libraryGridView reloadData];
    } else {
        self.libraryGridView.hidden = YES;
        self.noProjectsLabel.hidden = NO;
    }
}

#pragma mark - Grid View Methods

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)gridView {
    return CGSizeMake(334.0f, 174.0f);
}

- (AQGridViewCell*)gridView:(AQGridView *)gridView cellForItemAtIndex:(NSUInteger)index {
    static NSString * ProjectGridCellIdentifier = @"ProjectGridCellIdentifier";
	
	DMProjectLibraryGridViewCell * cell = (DMProjectLibraryGridViewCell *)[gridView dequeueReusableCellWithIdentifier: ProjectGridCellIdentifier];
	if ( cell == nil )
	{
		cell = [[DMProjectLibraryGridViewCell alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 334.0f, 174.0f) reuseIdentifier: ProjectGridCellIdentifier];
	}
    cell.project = [self.filteredLibrary objectAtIndex:index];
    cell.delegate = self;
    return cell;
}

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) gridView {
    NSUInteger count = [self.filteredLibrary count];
    return count;
}

#pragma mark - Project Library Grid View Cell delegate methods

- (void)projectLibraryGridViewCellDeleteTapped:(DMProjectLibraryGridViewCell *)projectLibraryGridViewCell {
    self.alertProject = projectLibraryGridViewCell.project;
    self.deleteAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DELETE_PROJECT_TITLE", @"")
                                                       message:NSLocalizedString(@"DELETE_PROJECT_MESSAGE", @"")
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"CANCEL", @"")
                                             otherButtonTitles:NSLocalizedString(@"DELETE_PROJECT_BUTTON", @""), nil];
    [self.deleteAlertView show];
}

- (void)projectLibraryGridViewCellDuplicateTapped:(DMProjectLibraryGridViewCell *)projectLibraryGridViewCell {
    self.alertProject = projectLibraryGridViewCell.project;
    self.duplicateAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DUPLICATE_PROJECT_TITLE", @"")
                                                       message:NSLocalizedString(@"DUPLICATE_PROJECT_MESSAGE", @"")
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"CANCEL", @"")
                                             otherButtonTitles:NSLocalizedString(@"DUPLICATE_PROJECT_BUTTON", @""), nil];
    [self.duplicateAlertView show];
}

- (void)projectLibraryGridViewCellThumbnailTapped:(DMProjectLibraryGridViewCell *)projectLibraryGridViewCell {
    NSString *str = projectLibraryGridViewCell.project.type;
    if (str==nil || [str isEqualToString:@"Granite"]){
        [kDMNavigationControllerRef previewProject:projectLibraryGridViewCell.project];
    }else if ([str isEqualToString:@"Stonespar"]){
        [kDMNavigationControllerRef previewStonesparProject:projectLibraryGridViewCell.project];
    }
}

# pragma mark - Alert View delegate methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView == self.duplicateAlertView) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            [kDMNavigationControllerRef createProjectDuplicatingProject:self.alertProject];
        }
        self.alertProject = nil;
        self.duplicateAlertView = nil;
    } else if (alertView == self.deleteAlertView) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            [kDMNavigationControllerRef deleteProject:self.alertProject];
            // Make sure we have the latest copy
            [self updateData];
        }
        self.alertProject = nil;
        self.deleteAlertView = nil;
    }
}

#pragma mark - Nav Bar search delegate methods
- (void)customNavBarFilterByType:(NSString *)type{
    if ([type isEqualToString:@"All"]) {
        self.filteredLibrary = self.projectLibrary;
    }else{
        NSMutableArray *filteredLibrary = [[NSMutableArray alloc] init];
        for(DMProject *project in self.projectLibrary){
            if ([project.type isEqualToString:type]){
                [filteredLibrary addObject:project];
            }
        }
        self.filteredLibrary = filteredLibrary;
    }
    
    [self.libraryGridView reloadData];

}

- (void)customNavBar:(DMCustomNavBar*)navBar searchTextUpdated:(NSString*)searchText {
    self.searchText = searchText;
    if (searchText) {
        NSString *searchTextWithWildcard = [NSString stringWithFormat:@"*%@*", searchText];
        NSString *searchFormat = [NSString stringWithFormat:
                                  @"(SELF.properties.%@ LIKE[cd] %%@ OR SELF.properties.%@ LIKE[cd] %%@)",
                                  kDMProjectPropertyTitle,
                                  kDMProjectPropertyLocation];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:searchFormat, searchTextWithWildcard, searchTextWithWildcard];

        self.filteredLibrary = [self.projectLibrary filteredArrayUsingPredicate:predicate];
    } else {
        self.filteredLibrary = self.projectLibrary;
    }
    
    [self.libraryGridView reloadData];
}

- (void)customNavBarSearchCancelled:(DMCustomNavBar*)navBar {
    self.searchText = nil;
    self.filteredLibrary = self.projectLibrary;
    [self.libraryGridView reloadData];
}

@end
