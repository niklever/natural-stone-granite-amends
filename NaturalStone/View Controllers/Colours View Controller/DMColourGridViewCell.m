//
//  DMColourGridView.m
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMColourGridViewCell.h"

#import "UIImage+DMExtensions.h"

@implementation DMColourGridViewCellBase

@synthesize colourInfo = __colourInfo;
@synthesize ticked = __ticked;
@synthesize showingBack = __showingBack;

@end

@interface DMColourGridViewCell ()

@property (nonatomic,strong) UIView *frontView;
@property (nonatomic,strong) UIView *backView;
@property (nonatomic,strong) UIView *tickView;
@property (nonatomic,strong) UILabel *infoLabel;

- (void)handleGesture:(UIGestureRecognizer *)sender;

@end

@implementation DMColourGridViewCell

@synthesize frontView = __frontView;
@synthesize backView = __backView;
@synthesize tickView = __tickView;
@synthesize infoLabel = __infoLabel;
@synthesize delegate = __delegate;

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        
        self.frontView = [[UIView alloc] initWithFrame:self.frame];
        self.backView = [[UIView alloc] initWithFrame:self.frame];
        self.tickView = [[UIView alloc] initWithFrame:self.frame];
        self.infoLabel = [[UILabel alloc] initWithFrame:self.frame];
        
        [self.contentView addSubview:self.frontView];
        [self.contentView addSubview:self.backView];
        [self.frontView addSubview:self.tickView];
        [self.backView addSubview:self.infoLabel];
        self.backView.hidden = YES;
        self.backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ColourInfoBG.png"]];
        self.tickView.alpha = 0.0f;
        self.tickView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.infoLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.tickView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ColourSelected.png"]];
        self.infoLabel.numberOfLines = 2;
        self.infoLabel.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        self.infoLabel.textAlignment = UITextAlignmentCenter;
        self.infoLabel.font = [UIFont fontWithName:kDMDefaultFont size:20.0f];
        
        self.selectionStyle = AQGridViewCellSelectionStyleNone;
        
        //UITapGestureRecognizer *singleFingerDTap = [[UITapGestureRecognizer alloc]
        //                                            initWithTarget:self action:@selector(handleGesture:)];
        //singleFingerDTap.numberOfTapsRequired = 2;
        //singleFingerDTap.delegate = self;
        //singleFingerDTap.delaysTouchesBegan = YES;
//        [self.contentView addGestureRecognizer:singleFingerDTap];
        
        UISwipeGestureRecognizer *singleFingerSwipeLeft = [[UISwipeGestureRecognizer alloc]
                                                     initWithTarget:self action:@selector(handleGesture:)];
        singleFingerSwipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
        singleFingerSwipeLeft.delegate = self;
        //[self.contentView addGestureRecognizer:singleFingerSwipeLeft];
        [self addGestureRecognizer:singleFingerSwipeLeft];
        
        UISwipeGestureRecognizer *singleFingerSwipeRight = [[UISwipeGestureRecognizer alloc]
                                                           initWithTarget:self action:@selector(handleGesture:)];
        singleFingerSwipeRight.direction = UISwipeGestureRecognizerDirectionRight;
        singleFingerSwipeRight.delegate = self;
        //[self.contentView addGestureRecognizer:singleFingerSwipeRight];
        [self addGestureRecognizer:singleFingerSwipeRight];
        
        UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc]
                                                    initWithTarget:self action:@selector(handleGesture:)];
        singleFingerTap.numberOfTapsRequired = 1;
        //[singleFingerTap requireGestureRecognizerToFail:singleFingerDTap];
        singleFingerTap.delaysTouchesBegan = YES;
        singleFingerTap.delegate = self;
        //[self.contentView addGestureRecognizer:singleFingerTap];
        [self addGestureRecognizer:singleFingerTap];
        
         singleFingerTap = nil;
         //singleFingerDTap = nil;
         singleFingerSwipeLeft = nil;
         singleFingerSwipeRight = nil;
    }
    return self;
}

- (void)setColourInfo:(NSDictionary *)colourInfo {
    [super setColourInfo:colourInfo];
 
    NSString *ref = [self.colourInfo objectForKey:@"ref"];
    UIImage *refImage = [UIImage colourSwatchImageForRef:ref];
    UIColor *refColour = [UIColor colorWithPatternImage:refImage];
    self.frontView.backgroundColor = refColour;
    self.backView.backgroundColor = refColour;
    self.infoLabel.text = [NSString stringWithFormat:@"%@-%@",
                           [self.colourInfo objectForKey:@"name"],
                           [[self.colourInfo objectForKey:@"countryCode"] uppercaseString]];
}

- (void)setTicked:(BOOL)ticked {
    [super setTicked:ticked];
    [UIView animateWithDuration:0.3 animations:^{
        self.tickView.alpha = ticked ? 1.0f : 0.0f;
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.frontView.frame = CGRectMake(1.0f, 1.0f, (self.frame.size.width - 2.0f), (self.frame.size.height - 2.0f));
    self.backView.frame = self.frontView.frame;
}

- (void)setShowingBack:(BOOL)showingBack fromGesture:(UIGestureRecognizer*)gestureRecognizer {
    [super setShowingBack:showingBack];
    
    NSInteger direction = UIViewAnimationTransitionFlipFromLeft;
    if ([gestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]]) {
        if (((UISwipeGestureRecognizer*)gestureRecognizer).direction == UISwipeGestureRecognizerDirectionLeft) {
            direction = UIViewAnimationTransitionFlipFromRight;
        }
    } else if (!self.showingBack) {
        direction = UIViewAnimationTransitionFlipFromRight;
    }
    
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1.0];
    if (self.showingBack) {
        [UIView setAnimationTransition:direction
                               forView:[self contentView]
                                 cache:YES];
    } else {
        [UIView setAnimationTransition:direction
                               forView:[self contentView]
                                 cache:YES];
    }
    self.backView.hidden = ! self.showingBack;
	[UIView commitAnimations];
}
- (void)setShowingBack:(BOOL)showingBack {
    [self setShowingBack:showingBack fromGesture:nil];
}

- (void)handleGesture:(UIGestureRecognizer *)sender {
    if ([sender isKindOfClass:[UISwipeGestureRecognizer class]]) {
        [self setShowingBack:!self.showingBack fromGesture:sender];
    } else if (((UITapGestureRecognizer*)sender).numberOfTapsRequired == 2) {
        self.showingBack = !self.showingBack;
    } else if (((UITapGestureRecognizer*)sender).numberOfTapsRequired == 1 && ! self.showingBack) {
        if (self.ticked) {
            [self.delegate colourGridViewCellWillBeUnticked:self];
            self.ticked = NO;
        } else {
            if ([self.delegate colourGridViewCellShouldBeTicked:self]) {
                self.ticked = YES;
            }
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

@end
