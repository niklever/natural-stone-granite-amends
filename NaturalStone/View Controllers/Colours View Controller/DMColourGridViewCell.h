//
//  DMColourGridViewCell.h
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AQGridViewCell.h"

@interface DMColourGridViewCellBase : AQGridViewCell

@property (nonatomic,strong) NSDictionary *colourInfo;
@property (nonatomic,assign) BOOL ticked;
@property (nonatomic,assign) BOOL showingBack;

@end

@class DMColourGridViewCell;

@protocol DMColourGridViewDelegate <NSObject>

- (BOOL)colourGridViewCellShouldBeTicked:(DMColourGridViewCell*)cell;

- (void)colourGridViewCellWillBeUnticked:(DMColourGridViewCell*)cell;

@end

@interface DMColourGridViewCell : DMColourGridViewCellBase <UIGestureRecognizerDelegate>

@property (nonatomic, weak) id<DMColourGridViewDelegate> delegate;

@end