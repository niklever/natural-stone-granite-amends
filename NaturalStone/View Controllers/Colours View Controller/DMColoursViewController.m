//
//  DMColoursViewController.m
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMColoursViewController.h"

#import "JSONKit.h"

@interface DMColoursViewController ()

@property (nonatomic, strong) IBOutlet AQGridView *gridView;
@property (nonatomic, strong) IBOutlet UIView *infoView;
@property (nonatomic, strong) IBOutlet UILabel *infoColoursLeftCount;
@property (nonatomic, strong) IBOutlet UILabel *infoColoursLeftTitle;
@property (nonatomic, strong) IBOutlet UILabel *infoColoursInstruction;
@property (nonatomic, strong) NSArray *coloursData;

- (void)updateColoursLeftCount;

@end

@implementation DMColoursViewController

@synthesize gridView = __gridView;
@synthesize infoView = __infoView;
@synthesize infoColoursLeftCount = __infoColoursLeftCount;
@synthesize infoColoursLeftTitle = __infoColoursLeftTitle;
@synthesize infoColoursInstruction = __infoColoursInstruction;
@synthesize coloursData = __coloursData;
@synthesize colourSelections = __colourSelections;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.coloursData = [DMProject coloursData];
        self.title = NSLocalizedString(@"COLOURS_TITLE", @"");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!self.colourSelections) {
        NSMutableArray *projectColours = kDMNavigationControllerRef.currentProject.colours;
        self.colourSelections = [NSMutableSet setWithCapacity:kDMProjectMaxColours];
        for (NSDictionary *projectColour in projectColours) {
            NSString *col = [projectColour objectForKey:kDMProjectColourReferenceKey];
            [self.colourSelections addObject:col];
            NSNumber *num = [projectColour objectForKey:@"discontinued"];
            if (num != NULL && [num boolValue] ) break;
        }
    }
    // Do any additional setup after loading the view from its nib.
    self.infoView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ColourInfoBG.png"]];
    self.infoColoursLeftCount.font = [UIFont fontWithName:kDMDefaultBoldFont size:60.0f];
    self.infoColoursLeftTitle.font = [UIFont fontWithName:kDMDefaultBoldFont size:22.0f];
    self.infoColoursInstruction.font = [UIFont fontWithName:kDMDefaultBoldFont size:13.0f];
    self.infoColoursLeftTitle.text = NSLocalizedString(@"COLOURS_LEFT_TITLE", @"");
    self.infoColoursInstruction.text = NSLocalizedString(@"COLOURS_INSTRUCTION", @"");
    self.gridView.scrollEnabled = NO;
    self.gridView.backgroundColor = [UIColor clearColor];

    [self updateColoursLeftCount];
    [self.gridView reloadData];
    
    [kDMNavigationControllerRef listVCs];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.infoView = nil;
    self.infoColoursLeftCount = nil;
    self.infoColoursLeftTitle = nil;
    self.infoColoursInstruction = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


- (BOOL)updateProjectProperties:(BOOL)validate {
    if (validate) {
        if ([self.colourSelections count] == 0) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PROJECT_COLOURS_INVALID_TITLE", @"")
                                         message:NSLocalizedString(@"PROJECT_COLOURS_INVALID_MESSAGE", @"")
                                        delegate:nil
                               cancelButtonTitle:NSLocalizedString(@"CLOSE", @"")
                               otherButtonTitles:nil] show];
            return NO;
        }
    }
    
    NSMutableSet *colourSelections = [self.colourSelections mutableCopy];
    NSMutableArray *projectColours = [kDMNavigationControllerRef.currentProject.colours mutableCopy];
    BOOL resetPercentages = NO;
    for (NSDictionary *projectColour in kDMNavigationControllerRef.currentProject.colours) {
        NSString *ref = [projectColour objectForKey:kDMProjectColourReferenceKey];
        if (![colourSelections containsObject:ref]) {
            // If we've deselected a colour then remove it from the project colours
            [projectColours removeObject:projectColour];
            resetPercentages = YES;
        } else {
            // Otherwise this colour is already selected so re-use the existing one
            // Meaning remove it from the list of selections as we don't need to do anything
            [colourSelections removeObject:ref];
        }
    }

    if ([colourSelections count]) {
        resetPercentages = YES;
    }
    
    if (resetPercentages) {
        for (NSMutableDictionary *colourInfo in projectColours) {
            [colourInfo setObject:[NSNumber numberWithInteger:0] forKey:kDMProjectColourPercentageKey];
        }
    }
    for (NSString *ref in colourSelections) {
        NSMutableDictionary *colourInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:ref, kDMProjectColourReferenceKey,
                                    [NSNumber numberWithInteger:0], kDMProjectColourPercentageKey, nil];
        [projectColours addObject:colourInfo];
    }
    [kDMNavigationControllerRef.currentProject.colours setArray:projectColours];
    
     colourSelections = nil;
     projectColours = nil;
    return YES;
}

- (void)updateColoursLeftCount {
    self.infoColoursLeftCount.text = [NSString stringWithFormat:@"%i", (kDMProjectMaxColours - [self.colourSelections count])];
}

#pragma mark - Grid View Methods

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)gridView {
    return CGSizeMake(127.0f, 187.0f);
}

- (AQGridViewCell*)gridView:(AQGridView *)gridView cellForItemAtIndex:(NSUInteger)index {
    static NSString * ColourGridCellIdentifier = @"ColourGridCellIdentifier";
	
	DMColourGridViewCell * cell = (DMColourGridViewCell *)[gridView dequeueReusableCellWithIdentifier: ColourGridCellIdentifier];
	if ( cell == nil )
	{
		cell = [[DMColourGridViewCell alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 127.0f, 187.0f) reuseIdentifier: ColourGridCellIdentifier];
        cell.delegate = self;
	}
    cell.colourInfo = [self.coloursData objectAtIndex:index];
    if ([self.colourSelections containsObject:[cell.colourInfo objectForKey:@"ref"]]) {
        cell.ticked = YES;
    }
    
    return cell;
}

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) gridView {
    return 30;
}

#pragma mark - Colour Grid View Cell delegate methods

- (BOOL)colourGridViewCellShouldBeTicked:(DMColourGridViewCell *)cell {
    if ([self.colourSelections count] == kDMProjectMaxColours) {
        return NO;
    }
    [self.colourSelections addObject:[cell.colourInfo objectForKey:@"ref"]];
    [self updateColoursLeftCount];
    
    return YES;
}

- (void)colourGridViewCellWillBeUnticked:(DMColourGridViewCell *)cell {
    [self.colourSelections removeObject:[cell.colourInfo objectForKey:@"ref"]];
    [self updateColoursLeftCount];
}

- (IBAction)testPressed:(id)sender {
    NSLog(@"Test pressed");
}
@end
