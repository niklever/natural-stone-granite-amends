//
//  DMColoursViewController.h
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AQGridView.h"
#import "DMColourGridViewCell.h"

@interface DMColoursViewController : DMProjectViewControllerBase <AQGridViewDelegate, AQGridViewDataSource, DMColourGridViewDelegate>

@property (nonatomic, strong) NSMutableSet *colourSelections;
- (IBAction)testPressed:(id)sender;

@end
