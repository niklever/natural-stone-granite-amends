//
//  DMColourMixViewController.m
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMColourMixViewController.h"

#import "DMColourMixView.h"

@interface DMColourMixViewController ()

@end

@implementation DMColourMixViewController

- (void)viewDidLoad {
    self.title = NSLocalizedString(@"COLOUR_MIX_TITLE", @"");

    NSMutableArray *projectColours = kDMNavigationControllerRef.currentProject.colours;
    DMColourMixView *colourMixView = [[DMColourMixView alloc] initWithColors:projectColours];
    colourMixView.frame = self.view.frame;
    [self.view addSubview:colourMixView];
     colourMixView = nil;
}

- (BOOL)updateProjectProperties:(BOOL)validate {
    return YES;
}

@end
