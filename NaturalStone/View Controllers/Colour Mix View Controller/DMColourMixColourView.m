//
//  DMColourMixColourView.m
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMColourMixColourView.h"

#import <QuartzCore/QuartzCore.h>

#import "UIImage+DMExtensions.h"

@implementation DMColourMixColourView

@synthesize descriptionLabel = __descriptionLabel;
@synthesize percentageLabel = __percentageLabel;
@synthesize backgroundLayer = __backgroundLayer;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        __descriptionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        __percentageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        
        __backgroundLayer = [[UIView alloc] initWithFrame:CGRectZero];
        [self addSubview:__backgroundLayer];
        
        self.descriptionLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        self.percentageLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        self.descriptionLabel.textAlignment = UITextAlignmentCenter;
        self.descriptionLabel.textColor = [UIColor blackColor];
        self.descriptionLabel.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.5f];
        self.descriptionLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:15.0f];
        self.percentageLabel.textAlignment = UITextAlignmentCenter;
        self.percentageLabel.textColor = [UIColor whiteColor];
        self.percentageLabel.font = [UIFont fontWithName:kDMDefaultFont size:25.0f];
        self.percentageLabel.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        
        [self addSubview:self.descriptionLabel];
        [self addSubview:self.percentageLabel];

        self.backgroundColor = [UIColor clearColor];
        
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.backgroundLayer.frame = CGRectMake(-self.frame.origin.x,0,1024,748);
    
    CGRect frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, 40.0f);
    frame.origin.y -= frame.size.height;
    self.percentageLabel.frame = frame;

    frame.origin.y -= frame.size.height;
    self.descriptionLabel.frame = frame;
}

@end
