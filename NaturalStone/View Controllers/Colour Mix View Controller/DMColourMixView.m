//
//  DMColourMixView.m
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMColourMixView.h"

#import "DMColourMixColourView.h"
#import "DMColourMixSlider.h"
#import "UIImage+DMExtensions.h"

@interface DMColourMixView ()

@property (nonatomic, strong) NSArray *colourViews;
@property (nonatomic, strong) NSArray *colours;
@property (nonatomic, strong) NSArray *sliders;
@property (nonatomic, strong) DMColourMixSlider *activeSlider;
@property (nonatomic, strong) NSArray *activeColourViews;
@property (nonatomic, strong) NSArray *activeColours;
@property (nonatomic, assign) BOOL sliderMoved;

@end

@implementation DMColourMixView

@synthesize colourViews = __colourViews;
@synthesize colours = __colours;
@synthesize sliders = __sliders;
@synthesize activeSlider = __activeSlider;
@synthesize activeColourViews = __activeColourViews;
@synthesize activeColours = __activeColours;
@synthesize sliderMoved = __sliderMoved;

- (id)initWithColors:(NSArray*)colours
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        // Initialization code
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.colours = colours;
        
        int l = [self.colours count], last = l - 1;
        NSUInteger percentage = 100.0f / (float)[self.colours count];
        NSDictionary *coloursData = [DMProject coloursDataAsDictionary];
        NSMutableArray *colourViews = [NSMutableArray arrayWithCapacity:l];
        NSMutableArray *sliders = [NSMutableArray arrayWithCapacity:last];
        for (int i = 0; i < l; ++i) {
            NSMutableDictionary *colour = [self.colours objectAtIndex:i];
            if ([[colour objectForKey:kDMProjectColourPercentageKey] integerValue] == 0) {
                if (i == last) {
                    // Work out the remainder
                    percentage = 100 - (percentage * last);
                }
                [colour setObject:[NSNumber numberWithInteger:percentage] forKey:kDMProjectColourPercentageKey];
            }
            NSDictionary *colourInfo = [coloursData objectForKey:[colour objectForKey:kDMProjectColourReferenceKey]];
            DMColourMixColourView *colourView = [[DMColourMixColourView alloc] initWithFrame:CGRectZero];
            colourView.descriptionLabel.text = [NSString stringWithFormat:@"%@-%@",
                                                [colourInfo objectForKey:@"name"],
                                                [[colourInfo objectForKey:@"countryCode"] uppercaseString]];
            
            colourView.backgroundLayer.backgroundColor = [UIColor colorWithPatternImage:[UIImage colourSwatchImageForRef:[colourInfo objectForKey:@"ref"]]];
            [self addSubview:colourView];
            [colourViews addObject:colourView];
             colourView = nil;
        }
        for (int i = 0; i < last; ++i) {
            DMColourMixSlider *slider = [[DMColourMixSlider alloc] initWithFrame:CGRectZero];
            [sliders addObject:slider];
            [self addSubview:slider];
             slider = nil;
        }
        self.colourViews = [NSArray arrayWithArray:colourViews];
        self.sliders = [NSArray arrayWithArray:sliders];
        
        self.multipleTouchEnabled = NO;
    }
    return self;
}


- (void)layoutSubviews {
    CGRect frame = CGRectMake(0, 0, 0, self.frame.size.height);
    CGRect sliderFrame = CGRectMake(0, 0, kDMColourMixSliderWidth, frame.size.height);
    // j will be one less than i
    for (int i = 0, j = 0, l = [self.colours count], last = l-1; i < l; j = i++) {
        DMColourMixColourView *colourView = [self.colourViews objectAtIndex:i];
        NSMutableDictionary *colour = [self.colours objectAtIndex:i];
        frame.origin.x += frame.size.width;
        if (i == last) {
            frame.size.width = self.frame.size.width - frame.origin.x;
        } else {
            frame.size.width = (int)(self.frame.size.width * ([[colour objectForKey:kDMProjectColourPercentageKey] floatValue] / 100.0f));
        }
        colourView.percentageLabel.text = [NSString stringWithFormat:@"%@%%", [colour objectForKey:kDMProjectColourPercentageKey]];
        colourView.frame = frame;
        if (i) { // i > 0
            DMColourMixSlider *slider = [self.sliders objectAtIndex:j];
            sliderFrame.origin.x = frame.origin.x - kDMColourMixSliderHalfWidth;
            slider.frame = sliderFrame;
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if ([touch.view isKindOfClass:[DMColourMixSlider class]]) {
        self.activeSlider = (DMColourMixSlider*)touch.view;
        if (!self.activeSlider.locked) {
            for (int i = 0, l = ([self.sliders count]); i < l; ++i) {
                if (self.activeSlider == [self.sliders objectAtIndex:i]) {
                    self.activeColourViews = [NSArray arrayWithObjects:[self.colourViews objectAtIndex:i], [self.colourViews objectAtIndex:(i+1)], nil];
                    self.activeColours = [NSArray arrayWithObjects:[self.colours objectAtIndex:i], [self.colours objectAtIndex:(i+1)], nil];
                }
            }
        }
        self.sliderMoved = NO;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {

    if (self.sliderMoved) {
        [self touchesMoved:touches withEvent:event];
    } else {
        self.activeSlider.locked = ! self.activeSlider.locked;
    }
    self.activeSlider = nil;
    self.activeColourViews = nil;
    self.activeColours = nil;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesEnded:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.activeSlider) {
        self.sliderMoved = YES;
    }
    if (self.activeColourViews) {
        DMColourMixColourView *leftView = [self.activeColourViews objectAtIndex:0];
        DMColourMixColourView *rightView = [self.activeColourViews objectAtIndex:1];
        NSMutableDictionary *leftColour = [self.activeColours objectAtIndex:0];
        NSMutableDictionary *rightColour = [self.activeColours objectAtIndex:1];
        
        UITouch *touch = [touches anyObject];
        CGPoint touchPosition = [touch locationInView:self];
        if (touchPosition.x < (leftView.frame.origin.x) || touchPosition.x > (rightView.frame.origin.x+rightView.frame.size.width)) {
            return;
        }
        CGFloat leftWidth = touchPosition.x - leftView.frame.origin.x;
        CGFloat totalWidth = leftView.frame.size.width + rightView.frame.size.width;
        NSUInteger totalPercent = [[leftColour objectForKey:kDMProjectColourPercentageKey] integerValue] + [[rightColour objectForKey:kDMProjectColourPercentageKey] integerValue];
        
        NSUInteger leftPercent = (NSUInteger)((leftWidth/totalWidth)*(CGFloat)totalPercent);
        NSUInteger rightPercent = totalPercent - leftPercent;
        if (leftPercent < 5 || rightPercent < 5) {
            return;
        }
        [leftColour setObject:[NSNumber numberWithInteger:leftPercent] forKey:kDMProjectColourPercentageKey];
        [rightColour setObject:[NSNumber numberWithInteger:rightPercent] forKey:kDMProjectColourPercentageKey];
        [self setNeedsLayout];
    }
}

@end
