//
//  DMColourMixSlider.h
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kDMColourMixSliderWidth 54.0f
#define kDMColourMixSliderHalfWidth 27.0f

@interface DMColourMixSliderBase : UIView

@property (nonatomic, assign) BOOL locked;

@end

@interface DMColourMixSlider : DMColourMixSliderBase

@end
