//
//  DMColourMixColourView.h
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMColourMixColourView : UIView

@property (nonatomic, readonly) UILabel *descriptionLabel;
@property (nonatomic, readonly) UILabel *percentageLabel;
@property (strong, nonatomic, readonly) UIView *backgroundLayer;

@end