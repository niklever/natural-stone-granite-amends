//
//  DMColourMixSlider.m
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMColourMixSlider.h"

#import <QuartzCore/QuartzCore.h>

@implementation DMColourMixSliderBase

@synthesize locked = __locked;

@end

@interface DMColourMixSlider ()

@property (nonatomic, strong) CALayer *handleLayer;
@property (nonatomic, strong) CALayer *divider;

@end

@implementation DMColourMixSlider

@synthesize handleLayer = __handleLayer;
@synthesize divider = __divider;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CALayer *divider = [CALayer layer];
        divider.backgroundColor = [[UIColor whiteColor] CGColor];
        self.divider = divider;
        [self.layer addSublayer:divider];

        self.handleLayer = [CALayer layer];
        
        [self.layer addSublayer:self.handleLayer];
        
        self.locked = NO;
    }
    return self;
}

- (void)setLocked:(BOOL)locked {
    // !!! DISABLING LOCKING
    locked = NO;
    [super setLocked:locked];
    
    if (locked) {
        self.handleLayer.contents = (id)[[UIImage imageNamed:@"color-slider-locked.png"] CGImage];         
    } else {
        self.handleLayer.contents = (id)[[UIImage imageNamed:@"color-slider.png"] CGImage];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

    self.divider.frame = CGRectMake(self.frame.size.width/2, 0, 1, self.frame.size.height);
    self.handleLayer.frame = CGRectMake(0, (self.frame.size.height - 174.0f)/2.0f, 54, 174.0f);
}

@end
