//
//  DMEmailViewController.h
//  NaturalStone
//
//  Created by John McKerrell on 13/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@class DMEmailViewController;

@protocol DMEmailViewControllerDelegate <NSObject>

- (void)emailViewControllerDidCancel:(DMEmailViewController*)emailViewController;
- (void)emailViewControllerShouldSend:(DMEmailViewController*)emailViewController;

@end

@interface DMEmailViewController : UIViewController <UITextViewDelegate,UITextFieldDelegate,ABPeoplePickerNavigationControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic,readonly) IBOutlet UITextField *toField;
@property (weak, nonatomic,readonly) IBOutlet UITextField *fromField;
@property (weak, nonatomic,readonly) IBOutlet UITextField *subjectField;
@property (weak, nonatomic,readonly) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic,readonly) IBOutlet UISwitch *quoteSwitch;
@property (weak, nonatomic) IBOutlet UILabel *message_lbl;

@property (nonatomic,strong) IBOutlet id<DMEmailViewControllerDelegate> delegate;

- (void)setActive:(BOOL)active;

@end
