//
//  DMEmailViewController.m
//  NaturalStone
//
//  Created by John McKerrell on 13/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMEmailViewController.h"

#import <QuartzCore/QuartzCore.h>

@interface DMEmailViewController (){
    CGRect originalFrameTxt;
}

@property (nonatomic,strong) IBOutlet UILabel *headerLabel;
@property (nonatomic,strong) IBOutlet UIButton *cancelButton;
@property (nonatomic,strong) IBOutlet UIButton *sendButton;
@property (nonatomic,strong) IBOutlet UILabel *toLabel;
@property (nonatomic,strong) IBOutlet UITextField *toField;
@property (nonatomic,strong) IBOutlet UILabel *fromLabel;
@property (nonatomic,strong) IBOutlet UITextField *fromField;
@property (nonatomic,strong) IBOutlet UILabel *subjectLabel;
@property (nonatomic,strong) IBOutlet UITextField *subjectField;
@property (nonatomic,strong) IBOutlet UILabel *contentLabel;
@property (nonatomic,strong) IBOutlet UITextView *contentTextView;
@property (nonatomic,strong) IBOutlet UIView *emailShadowView;
@property (nonatomic,strong) IBOutlet UIView *emailWrapperView;
@property (nonatomic,strong) IBOutlet UIView *activityWrapperView;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) IBOutlet UIButton *toContactButton;
@property (nonatomic,strong) IBOutlet UIButton *fromContactButton;
@property (nonatomic,strong) IBOutlet UISwitch *quoteSwitch;
@property (nonatomic,strong) UITextField *activeContactField;
@property (nonatomic,strong) UIPopoverController *addressesPopover;

- (IBAction)buttonTapped:(id)sender;
- (void)keyboardWillShow:(NSNotification *)n;
- (void)keyboardWillHide:(NSNotification *)n;

@end

@implementation DMEmailViewController

@synthesize headerLabel = __headerLabel;
@synthesize cancelButton = __cancelButton;
@synthesize sendButton = __sendButton;
@synthesize toLabel = __toLabel;
@synthesize toField = __toField;
@synthesize fromLabel = __fromLabel;
@synthesize fromField = __fromField;
@synthesize subjectLabel = __subjectLabel;
@synthesize subjectField = __subjectField;
@synthesize contentLabel = __contentLabel;
@synthesize contentTextView = __contentTextView;
@synthesize emailShadowView = __emailShadowView;
@synthesize emailWrapperView = __emailWrapperView;
@synthesize delegate = __delegate;
@synthesize activityWrapperView = __activityWrapperView;
@synthesize activityView = __activityView;
@synthesize scrollView = __scrollView;
@synthesize toContactButton = __toContactButton;
@synthesize fromContactButton = __fromContactButton;
@synthesize activeContactField = __activeContactField;
@synthesize addressesPopover = __addressesPopover;
@synthesize quoteSwitch = __quoteSwitch;
@synthesize message_lbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.headerLabel.font = [UIFont fontWithName:kDMDefaultFont size:25.0f];
    self.headerLabel.text = NSLocalizedString(@"EMAIL_TITLE", @"");
    self.toLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:25.0f];
    self.toLabel.text = NSLocalizedString(@"EMAIL_TO_TITLE", @"");
    self.fromLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:25.0f];
    self.fromLabel.text = NSLocalizedString(@"EMAIL_FROM_TITLE", @"");
    self.subjectLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:25.0f];
    self.subjectLabel.text = NSLocalizedString(@"EMAIL_SUBJECT_TITLE", @"");
    self.contentLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:25.0f];
    self.contentLabel.text = NSLocalizedString(@"EMAIL_CONTENT_TITLE", @"");
    self.contentTextView.font = [UIFont fontWithName:kDMDefaultTallFont size:21.0f];
    self.toField.placeholder = NSLocalizedString(@"EMAIL_TO_PLACEHOLDER",@"");
    self.fromField.placeholder = NSLocalizedString(@"EMAIL_FROM_PLACEHOLDER",@"");
    self.subjectField.placeholder = NSLocalizedString(@"EMAIL_SUBJECT_PLACEHOLDER",@"");
    [self.toField fixForDolphin];
    [self.fromField fixForDolphin];
    [self.subjectField fixForDolphin];
    
    self.contentTextView.textColor = [UIColor colorWithRed:0.77 green:0.52 blue:0.23 alpha:1.00];
    
    self.contentTextView.layer.cornerRadius = 3.0f;
    
    self.emailWrapperView.layer.cornerRadius = 5.0f;
    self.emailShadowView.layer.shadowColor = [[UIColor colorWithWhite:0.6f alpha:1.0] CGColor];
    self.emailShadowView.layer.shadowOffset = CGSizeMake(0, 1);
    self.emailShadowView.layer.shadowRadius = 2.0f;
    self.emailShadowView.layer.shadowOpacity = 1.0f;
    self.emailShadowView.backgroundColor = [UIColor clearColor];
    
    [self.activityView stopAnimating];
    self.activityWrapperView.alpha = 0.0f;
    self.activityWrapperView.layer.cornerRadius = 5.0f;
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    originalFrameTxt    =   self.contentTextView.frame;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //[self.scrollView scrollRectToVisible:textField.frame animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
//    [self.scrollView scrollRectToVisible:textView.frame animated:YES];
    //textView.frame  =   CGRectMake(textView.frame.origin.x, textView.frame.origin.y, textView.frame.size.width, 80);
    
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    self.message_lbl.hidden = ([txtView.text length] > 0);
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)txtView
{
    self.message_lbl.hidden = ([txtView.text length] > 0);
    txtView.frame  =   originalFrameTxt;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    NSValue* boundsValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    CGSize keyboardSize = [boundsValue CGRectValue].size;
    NSNumber* durationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    [UIView animateWithDuration:[durationValue floatValue] animations:^{
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
    }];
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    NSNumber* durationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    self.contentTextView.frame  =   originalFrameTxt;
    
    [UIView animateWithDuration:[durationValue floatValue] animations:^{
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height-55);
    [self.toField becomeFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.headerLabel = nil;
    self.cancelButton = nil;
    self.sendButton = nil;
    self.toLabel = nil;
    self.toField = nil;
    self.fromLabel = nil;
    self.fromField = nil;
    self.subjectLabel = nil;
    self.subjectField = nil;
    self.contentLabel = nil;
    self.contentTextView = nil;
    self.emailShadowView = nil;
    self.emailWrapperView = nil;
    self.delegate = nil;
    self.activityWrapperView = nil;
    self.activityView = nil;
    self.scrollView = nil;
    self.fromContactButton = nil;
    self.toContactButton = nil;
    self.activeContactField = nil;
    [self.addressesPopover dismissPopoverAnimated:NO];
    self.addressesPopover = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
     
- (IBAction)buttonTapped:(id)sender {
    if (sender == self.cancelButton) {
        [self setActive:YES];
        [self.delegate emailViewControllerDidCancel:self];
    } else if (sender == self.sendButton) {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 

        if (!([self.toField.text length] && [self.fromField.text length] && [emailTest evaluateWithObject:self.toField.text] && [emailTest evaluateWithObject:self.fromField.text])) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"EMAIL_FIELDS_REQUIRED_TITLE", @"")
                                         message:NSLocalizedString(@"EMAIL_FIELDS_REQUIRED_MESSAGE", @"")
                                        delegate:self
                               cancelButtonTitle:NSLocalizedString(@"CLOSE", @"")
                               otherButtonTitles:nil] show];
            return;     
        }
        [self setActive:YES];
        [self.delegate emailViewControllerShouldSend:self];
    } else if (sender == self.toContactButton || sender == self.fromContactButton) {
        if (sender == self.toContactButton) {
            self.activeContactField = self.toField;
        } else {
            self.activeContactField = self.fromField;
        }
        ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
        picker.peoplePickerDelegate = self;
//        picker.modalPresentationStyle = UIModalPresentationCurrentContext;
//        picker.modalInPopover = YES;
//        picker.delegate = self;
        
        picker.displayedProperties = [NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonEmailProperty]];
        
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        self.addressesPopover = popover;
        [popover presentPopoverFromRect:CGRectMake(0,((UIView*)sender).frame.size.height/2.0f,1,1) inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//        [self presentModalViewController:popover animated:YES];
         picker = nil;
         popover = nil;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)setActive:(BOOL)active {
    if (active) {
        for (UIControl *control in [NSArray arrayWithObjects:self.toField, self.fromField, self.contentTextView, self.subjectField, nil]) {
            [control resignFirstResponder];
        }
    }
    [UIView animateWithDuration:0.25 animations:^{
        if (active) {
            [self.activityView startAnimating];
        } else {
            [self.activityView stopAnimating];
        }
        self.activityWrapperView.alpha = active ? 1.0f : 0.0f;
    }];
}

//- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
//    viewController.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(peoplePickerNavigationControllerDidCancel:)] autorelease];
//
//}
/**
 * If the user cancels the addressbook picker then make sure
 * we hide it.
 */
- (void)peoplePickerNavigationControllerDidCancel:
(ABPeoplePickerNavigationController *)peoplePicker {
    [self.addressesPopover dismissPopoverAnimated:YES];
}


/**
 * The user has clicked on a person, we will let them continue
 * on into the person's details so that they can then select
 * an address property that we can take the postcode from.
 */
- (BOOL)peoplePickerNavigationController:
(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    return YES;
}

/**
 * The user has clicked on a value within the person record.
 * The addresses are in a multi value property so identifier
 * will be zero or higher, we then check the type of the
 * property. If that's ok we look for a ZIP value, if there
 * is one then we parse it, set the postcode fields and hide
 * the address book picker.
 */
- (BOOL)peoplePickerNavigationController:
(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
                                property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier{
    if( identifier != -1 ) {
        ABPropertyType type = ABPersonGetTypeOfProperty(property);
        if( type == 257 ) {
            NSString *address = (__bridge_transfer NSString*)ABRecordCopyValue(person, property);  
            
            if (address) {
                self.activeContactField.text = address;
            }
            [self.addressesPopover dismissPopoverAnimated:YES];
        }
    }
    return NO;
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    if( identifier != -1 ) {
        ABPropertyType type = ABPersonGetTypeOfProperty(property);
        if( type == 257 ) {
            ABMultiValueRef addressRef = ABRecordCopyValue(person, property);
            CFStringRef addressCFRef = ABMultiValueCopyValueAtIndex(addressRef, 0);
            NSString *address = (__bridge_transfer NSString *)addressCFRef;
            CFRelease(addressCFRef);
            //NSString *addressB = (__bridge NSString*)ABRecordCopyValue(person, property);
            
            if (address) {
                self.activeContactField.text = address;
            }
            [self.addressesPopover dismissPopoverAnimated:YES];
        }
    }
}
@end
