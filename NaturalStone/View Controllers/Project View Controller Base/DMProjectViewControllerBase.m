//
//  DMProjectViewControllerBase.m
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMProjectViewControllerBase.h"

#import <QuartzCore/QuartzCore.h>

@interface DMProjectViewControllerBase ()

@end

@implementation DMProjectViewControllerBase

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (BOOL)updateProjectProperties:(BOOL)validate {
    return NO;
}

- (void)applyStandardRoundedShadowEffect:(UIView *)view {
    for (UIView *childView in view.subviews) {
        childView.layer.cornerRadius = 5.0f;
    }
    view.layer.shadowColor = [[UIColor colorWithWhite:0.6f alpha:1.0] CGColor];
    view.layer.shadowOffset = CGSizeMake(0, 1);
    view.layer.shadowRadius = 2.0f;
    view.layer.shadowOpacity = 1.0f;
    view.backgroundColor = [UIColor clearColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BackGround.png"]];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIView alloc] initWithFrame:CGRectZero]];
}

@end
