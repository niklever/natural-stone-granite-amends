//
//  DMProjectViewControllerBase.h
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMProjectViewControllerBase : UIViewController

- (BOOL)updateProjectProperties:(BOOL)validate;
- (void)applyStandardRoundedShadowEffect:(UIView*)view;

@end
