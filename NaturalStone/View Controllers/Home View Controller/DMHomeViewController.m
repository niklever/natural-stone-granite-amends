//
//  DMHomeViewController.m
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMHomeViewController.h"

#import "DMNavigationController.h"

@interface DMHomeViewController ()

// createProject rather than newProject as convention suggests I shouldn't have new in the method name
@property (nonatomic, strong) IBOutlet UIButton *createProjectBtn;
@property (nonatomic, strong) IBOutlet UIButton *projectLibraryBtn;
@property (nonatomic, assign) BOOL doneFirstLoad;

@end

@implementation DMHomeViewController

@synthesize createProjectBtn = __createProjectBtn;
@synthesize projectLibraryBtn = __projectLibraryBtn;
@synthesize doneFirstLoad = __doneFirstLoad;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"title.png"]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if (self.doneFirstLoad) {
        self.createProjectBtn.alpha = 1.0;
        self.projectLibraryBtn.alpha = 1.0;
    } else {
        self.createProjectBtn.alpha = 0.0;
        self.projectLibraryBtn.alpha = 0.0;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    self.createProjectBtn = nil;
    self.projectLibraryBtn = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)viewWillAppear:(BOOL)animated {
    [kDMNavigationControllerRef.navBar hideNavigationBarAnimated:animated];
    if (!self.doneFirstLoad) {
        self.doneFirstLoad = YES;
        [UIView animateWithDuration:0.375 animations:^{
            self.createProjectBtn.alpha = 1.0;
            self.projectLibraryBtn.alpha = 1.0;
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [kDMNavigationControllerRef.navBar showNavigationBarAnimated:animated];
}

- (IBAction)buttonClicked:(id)sender {
    if (sender == self.createProjectBtn) {
        [kDMNavigationControllerRef showGranite];
    } else if (sender == self.projectLibraryBtn) {
        [kDMNavigationControllerRef showProjectLibrary];
    }
}

@end
