//
//  DMSizeMixViewController.h
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMProjectViewControllerBase.h"

#import "DMFlagPercentageView.h"

@interface DMSizeMixViewController : DMProjectViewControllerBase <UITableViewDataSource,UITableViewDelegate,DMFlagPercentageViewDelegate,UITextFieldDelegate>

@end
