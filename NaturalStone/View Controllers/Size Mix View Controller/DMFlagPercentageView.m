//
//  DMFlagPercentageView.m
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMFlagPercentageView.h"

#import <QuartzCore/QuartzCore.h>

@implementation DMFlagPercentageViewBase

@synthesize gauge = __gauge;
@synthesize length = __length;
@synthesize percentage = __percentage;

@end

@interface DMFlagPercentageView ()

@property (nonatomic,strong) UIView *detailView;
@property (nonatomic,strong) UIButton *enableButton;
@property (nonatomic,strong) UISlider *percentageSlider;
@property (nonatomic,strong) UILabel *lengthLabel;
@property (nonatomic,strong) UILabel *percentageLabel;

- (void)enableClicked:(id)sender;
- (void)percentageUpdated:(id)sender;

@end

@implementation DMFlagPercentageView

@synthesize detailView = __detailView;
@synthesize enableButton = __enableButton;
@synthesize percentageSlider = __percentageSlider;
@synthesize lengthLabel = __lengthLabel;
@synthesize percentageLabel = __percentageLabel;
@synthesize delegate = __delegate;

// Full size 190 x 107

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.detailView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.percentageSlider = [[UISlider alloc] initWithFrame:CGRectMake(12, 64, 165, 21)];
        [self.percentageSlider setThumbImage:[UIImage imageNamed:@"slider-handle.png"] forState:UIControlStateNormal];
        [self.percentageSlider setMinimumTrackImage:[[UIImage imageNamed:@"slider-fill.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:0] forState:UIControlStateNormal];
        [self.percentageSlider setMaximumTrackImage:[UIImage imageNamed:@"slider-base.png"] forState:UIControlStateNormal];
        
        self.enableButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.enableButton.frame = CGRectMake(11, 38, 167, 32);
        [self.enableButton setBackgroundImage:[UIImage imageNamed:@"value-btn.png"] forState:UIControlStateNormal];
        [self.enableButton setBackgroundImage:[UIImage imageNamed:@"value-btn-on.png"] forState:UIControlStateHighlighted];
        [self.enableButton setTitleColor:[UIColor colorWithRed:0.19 green:0.19 blue:0.19 alpha:1.00] forState:UIControlStateNormal];
        [self.enableButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        
        UIView *infoView = [[UIView alloc] initWithFrame:CGRectMake(11, 10, 167, 32)];
        
        infoView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"value-btn-on.png"]];
        
        self.lengthLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 165, 32)];
        self.lengthLabel.backgroundColor = [UIColor clearColor];
        self.lengthLabel.textColor = [UIColor whiteColor];
        self.lengthLabel.font = [UIFont fontWithName:kDMDefaultFont size:20];
        self.lengthLabel.textAlignment = UITextAlignmentCenter;
        [infoView addSubview:self.lengthLabel];

        self.percentageLabel = [[UILabel alloc] initWithFrame:CGRectMake(122, 0, 38, 32)];
        self.percentageLabel.backgroundColor = [UIColor clearColor];
        self.percentageLabel.textColor = [UIColor whiteColor];
        self.percentageLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:15];
        [infoView addSubview:self.percentageLabel];
        
        [self addSubview:self.detailView];
        [self.detailView addSubview:self.percentageSlider];
        [self.detailView addSubview:infoView];
        [self addSubview:self.enableButton];
         infoView = nil;
        
        self.detailView.hidden = YES;
        
        [self.enableButton addTarget:self action:@selector(enableClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.percentageSlider addTarget:self action:@selector(percentageUpdated:) forControlEvents:UIControlEventValueChanged];
    }
    return self;
}

- (void)enableClicked:(id)sender {
    self.percentage = 5;
    self.detailView.hidden = NO;
    self.enableButton.hidden = YES;
}

- (void)percentageUpdated:(id)sender {
    NSUInteger newPercentage = 5 * ceil(20.0 * self.percentageSlider.value);
    self.percentage = newPercentage;
}

- (void)setPercentage:(NSUInteger)percentage {
    if ([self.delegate flagPercentageView:self shouldUpdatePercentage:percentage]) {
        [super setPercentage:percentage];
        
        [self.delegate flagPercentageViewUpdatedPercentage:self];

        self.percentageLabel.text = [NSString stringWithFormat:@"%i%%", self.percentage];
        
        if (self.percentage == 0) {
            self.detailView.hidden = YES;
            self.enableButton.hidden = NO;
        } else {
            self.detailView.hidden = NO;
            self.enableButton.hidden = YES;
        }
    }
    self.percentageSlider.value = (float)self.percentage / 100.0f;
}

- (void)setLength:(NSUInteger)length {
    [super setLength:length];
    
    [self.enableButton setTitle:[NSString stringWithFormat:@"%i", self.length] forState:UIControlStateNormal];
    self.lengthLabel.text = [NSString stringWithFormat:@"%i", self.length];
}

- (BOOL)enabled {
    return self.enableButton.hidden;
}

- (void)dealloc {
    self.delegate = nil;
    
}



@end
