//
//  DMSizeMixViewController.m
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMSizeMixViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "DMSizeMixTableViewCell.h"
#import "DMFlagPercentageView.h"

@interface DMSizeMixViewController ()

@property (nonatomic,strong) IBOutlet UIImageView *flagTypeImage;
@property (nonatomic,strong) IBOutlet UIButton *settsButton;
@property (nonatomic,strong) IBOutlet UIButton *pavingButton;
@property (nonatomic,strong) IBOutlet UISegmentedControl *flagTypeControl;
@property (nonatomic,strong) IBOutlet UIView *areaAndThicknessView;
@property (nonatomic,strong) IBOutlet UIView *flagSizesView;
@property (nonatomic,strong) IBOutlet UIView *areaAndThicknessViewWrapper;
@property (nonatomic,strong) IBOutlet UIView *flagSizesViewWrapper;
@property (nonatomic,strong) IBOutlet UILabel *areaLabel;
@property (nonatomic,strong) IBOutlet UITextField *areaField;
@property (nonatomic,strong) IBOutlet UILabel *thicknessLabel;
@property (nonatomic,strong) IBOutlet UITextField *thicknessField;
@property (nonatomic,strong) IBOutlet UILabel *courseWidthLabel;
@property (nonatomic,strong) IBOutlet UILabel *lengthSizeMixLabel;
@property (nonatomic,strong) IBOutlet UIButton *distributeEvenlyButton;
@property (nonatomic,strong) IBOutlet UITableView *sizesTableView;

@property (nonatomic,strong) NSDictionary *sizesData;
@property (nonatomic,strong) NSArray *currentSizes;
@property (nonatomic,strong) NSMutableDictionary *currentProjectSizeMix;
@property (nonatomic,strong) NSMutableDictionary *currentProjectSizeMixSizes;

- (IBAction)flagTypeChanged:(id)sender;
- (IBAction)distributeEvenlyTapped:(id)sender;
- (void)hideKeyboard;
- (void)loadSizeMixes:(NSString*)flagType;
- (void)saveSizeMixes:(NSString*)flagType;
@end

@implementation DMSizeMixViewController

@synthesize flagTypeImage = __flagTypeImage;
@synthesize settsButton = __settsButton;
@synthesize pavingButton = __pavingButton;
@synthesize flagTypeControl = __flagTypeControl;
@synthesize areaAndThicknessViewWrapper = __areaAndThicknessViewWrapper;
@synthesize flagSizesViewWrapper = __flagSizesViewWrapper;
@synthesize areaAndThicknessView = __areaAndThicknessView;
@synthesize flagSizesView = __flagSizesView;
@synthesize areaLabel = __areaLabel;
@synthesize areaField = __areaField;
@synthesize thicknessLabel = __thicknessLabel;
@synthesize thicknessField = __thicknessField;
@synthesize courseWidthLabel = __courseWidthLabel;
@synthesize lengthSizeMixLabel = __lengthSizeMixLabel;
@synthesize distributeEvenlyButton = __distributeEvenlyButton;
@synthesize sizesData = __sizesData;
@synthesize currentProjectSizeMix = __currentProjectSizeMix;
@synthesize sizesTableView = __sizesTableView;
@synthesize currentSizes = __currentSizes;
@synthesize currentProjectSizeMixSizes = __currentProjectSizeMixSizes;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"SIZE_MIX_TITLE", @"");
    }
    return self;
}

- (IBAction)flagTypeChanged:(id)sender {
    [self hideKeyboard];
    
    if (sender == self.settsButton) {
        [self saveSizeMixes:@"pavingSizes"];
        [self.currentProjectSizeMix setObject:kDMProjectFlagTypeSett forKey:kDMProjectFlagTypeKey];
        self.currentSizes = [self.sizesData objectForKey:@"settSizes"];
        [self loadSizeMixes:@"settSizes"];
        self.flagTypeImage.image = [UIImage imageNamed:@"toggle-left.png"];
    } else {
        [self saveSizeMixes:@"settSizes"];
        [self.currentProjectSizeMix setObject:kDMProjectFlagTypePaving forKey:kDMProjectFlagTypeKey];
        self.currentSizes = [self.sizesData objectForKey:@"pavingSizes"];
        [self loadSizeMixes:@"pavingSizes"];
        self.flagTypeImage.image = [UIImage imageNamed:@"toggle-right.png"];
    }

    [self.sizesTableView reloadData];
}

- (void)loadSizeMixes:(NSString*)flagType {
    NSMutableArray *projectSizeMix = [self.currentProjectSizeMix objectForKey:flagType];
    NSMutableDictionary *sizeMixLookup = [NSMutableDictionary dictionaryWithCapacity:[projectSizeMix count]];
    for (NSMutableDictionary *sizeMix in projectSizeMix) {
        NSString *key = [NSString stringWithFormat:@"%@-%@", [sizeMix objectForKey:kDMProjectSizeMixWidth], [sizeMix objectForKey:kDMProjectSizeMixLength]];
        NSLog(@"%@=%@", kDMProjectSizeMixPercentageArea, [sizeMix objectForKey:kDMProjectSizeMixPercentageArea]);
        [sizeMixLookup setObject:[sizeMix objectForKey:kDMProjectSizeMixPercentageArea] forKey:key];
    }
    self.currentProjectSizeMixSizes = sizeMixLookup;
}

- (void)saveSizeMixes:(NSString*)flagType {
    if (!self.currentProjectSizeMixSizes) {
        return;
    }
    NSMutableArray *projectSizeMix = [NSMutableArray arrayWithCapacity:[self.currentProjectSizeMixSizes count]];
    for (NSString *key in self.currentProjectSizeMixSizes) {
        NSArray *gaugeAndLength = [key componentsSeparatedByString:@"-"];
        [projectSizeMix addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [NSNumber numberWithInteger:[[gaugeAndLength objectAtIndex:0] integerValue]], kDMProjectSizeMixWidth,
                                   [NSNumber numberWithInteger:[[gaugeAndLength objectAtIndex:1] integerValue]], kDMProjectSizeMixLength,
                                   [self.currentProjectSizeMixSizes objectForKey:key], kDMProjectSizeMixPercentageArea,
                                   [self.currentProjectSizeMixSizes objectForKey:key], kDMProjectSizeMixPercentageFlags,
                                   nil]];
    }
    [self.currentProjectSizeMix setObject:projectSizeMix forKey:flagType];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.sizesData = [DMProject sizesData];
    
    self.currentProjectSizeMix = kDMNavigationControllerRef.currentProject.sizeMix;
    
//    [self.flagTypeControl setTitle:NSLocalizedString(@"SIZE_MIX_SETTS_TITLE", @"") forSegmentAtIndex:0];
//    [self.flagTypeControl setTitle:NSLocalizedString(@"SIZE_MIX_PAVING_TITLE", @"") forSegmentAtIndex:1];
//    [self.flagTypeControl addTarget:self action:@selector(flagTypeChanged:) forControlEvents:UIControlEventValueChanged];
    [self flagTypeChanged:([kDMProjectFlagTypePaving isEqualToString:[self.currentProjectSizeMix objectForKey:kDMProjectFlagTypeKey]]?self.pavingButton:self.settsButton)];

//    [self applyStandardRoundedShadowEffect:self.flagTypeControl];
    [self applyStandardRoundedShadowEffect:self.areaAndThicknessViewWrapper];
    [self applyStandardRoundedShadowEffect:self.flagSizesViewWrapper];
    
    CALayer *areaAndThicknessDivider = [CALayer layer];
    areaAndThicknessDivider.backgroundColor = [[UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1.00] CGColor];
    areaAndThicknessDivider.frame = CGRectMake(475, 0, 1, 90);
    [self.areaAndThicknessView.layer addSublayer:areaAndThicknessDivider];
    
    CALayer *flagSizesDivider = [CALayer layer];
    flagSizesDivider.backgroundColor = [[UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1.00] CGColor];
    flagSizesDivider.frame = CGRectMake(190, 0, 1, 490);
    [self.flagSizesView.layer addSublayer:flagSizesDivider];

    CALayer *divider = [CALayer layer];
    divider.backgroundColor = [[UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1.00] CGColor];
    divider.frame = CGRectMake(380, 60, 1, 430);
    [self.flagSizesView.layer addSublayer:divider];
    divider = [CALayer layer];
    divider.backgroundColor = [[UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1.00] CGColor];
    divider.frame = CGRectMake(570, 60, 1, 430);
    [self.flagSizesView.layer addSublayer:divider];
    divider = [CALayer layer];
    divider.backgroundColor = [[UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1.00] CGColor];
    divider.frame = CGRectMake(760, 60, 1, 430);
    [self.flagSizesView.layer addSublayer:divider];
    
    self.areaLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:20];
    self.thicknessLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:20];
    self.courseWidthLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:20];
    self.lengthSizeMixLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:20];
    self.distributeEvenlyButton.titleLabel.font = [UIFont fontWithName:kDMDefaultFont size:15];
    
    self.areaLabel.text = NSLocalizedString(@"SIZE_MIX_AREA_TITLE", @"");
    self.thicknessLabel.text = NSLocalizedString(@"SIZE_MIX_THICKNESS_TITLE", @"");
    self.courseWidthLabel.text = NSLocalizedString(@"SIZE_MIX_COURSE_WIDTH_TITLE", @"");
    self.lengthSizeMixLabel.text = NSLocalizedString(@"SIZE_MIX_LENGTH_SIZE_TITLE", @"");
//    [self.distributeEvenlyButton setTitle:NSLocalizedString(@"SIZE_MIX_DISTRIBUTE_EVENLY_TITLE", @"") forState:UIControlStateNormal];
    [self.distributeEvenlyButton addTarget:self action:@selector(distributeEvenlyTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.areaField fixForDolphin];
    [self.thicknessField fixForDolphin];
    self.areaField.text = [[kDMNavigationControllerRef.currentProject.properties objectForKey:kDMProjectPropertyArea] stringValue];
    self.thicknessField.text = [[kDMNavigationControllerRef.currentProject.properties objectForKey:kDMProjectPropertyThickness] stringValue];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    [self.currentProjectSizeMix setObject:[NSNumber numberWithFloat:[self.areaField.text floatValue]] forKey:kDMProjectPropertyArea];
    [self.currentProjectSizeMix setObject:[NSNumber numberWithFloat:[self.thicknessField.text floatValue]] forKey:kDMProjectPropertyThickness];

    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.flagTypeImage = nil;
    self.settsButton = nil;
    self.pavingButton = nil;
    self.areaAndThicknessView = nil;
    self.flagSizesView = nil;
    self.areaAndThicknessViewWrapper = nil;
    self.flagSizesViewWrapper = nil;
    self.areaLabel = nil;
    self.areaField = nil;
    self.thicknessLabel = nil;
    self.thicknessField = nil;
    self.sizesData = nil;
}

- (BOOL)updateProjectProperties:(BOOL)validate {
    NSUInteger totalPercentage = 0;
    for (NSString *key in self.currentProjectSizeMixSizes) {
        totalPercentage += [[self.currentProjectSizeMixSizes objectForKey:key] integerValue];
    }
    if (validate && totalPercentage < 100) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SIZE_MIX_INVALID_PERCENTAGE_TITLE", @"")
                                     message:[NSString stringWithFormat:NSLocalizedString(@"SIZE_MIX_INVALID_PERCENTAGE_MESSAGE", @""), totalPercentage]
                                    delegate:nil
                           cancelButtonTitle:NSLocalizedString(@"CLOSE", @"")
                           otherButtonTitles:nil] show];
        return NO;
    }
    
    if ([kDMProjectFlagTypePaving isEqualToString:[self.currentProjectSizeMix objectForKey:kDMProjectFlagTypeKey]]) {
        [self saveSizeMixes:@"pavingSizes"];
    } else {
        [self saveSizeMixes:@"settSizes"];
    }
    [kDMNavigationControllerRef.currentProject.properties setObject:[NSNumber numberWithFloat:[self.areaField.text floatValue]] forKey:kDMProjectPropertyArea];
    [kDMNavigationControllerRef.currentProject.properties setObject:[NSNumber numberWithFloat:[self.thicknessField.text floatValue]] forKey:kDMProjectPropertyThickness];

    return YES;
}

- (void)hideKeyboard {
    if (self.areaField.isFirstResponder) {
        [self.areaField resignFirstResponder];
    } else if (self.thicknessField.isFirstResponder) {
        [self.thicknessField resignFirstResponder];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self hideKeyboard];
}

- (IBAction)distributeEvenlyTapped:(id)sender {
    [self hideKeyboard];
    NSMutableArray *flagPercentageViews = [NSMutableArray arrayWithCapacity:([self.sizesTableView numberOfRowsInSection:0]*4)];
    for (DMSizeMixTableViewCell *sizeMixTableCell in [self.sizesTableView visibleCells]) {
        for (DMFlagPercentageView *flagPercentageView in sizeMixTableCell.percentageViews) {
            if (!flagPercentageView.hidden && flagPercentageView.enabled) {
                [flagPercentageViews addObject:flagPercentageView];
            }
        }
    }
    [self.currentProjectSizeMixSizes removeAllObjects];
    NSInteger i = [flagPercentageViews count], last = i-1;
    NSUInteger totalPercentageLeft = 100;
    NSUInteger percentage = 0;
    for (; i > 0; --i, --last) {
        DMFlagPercentageView *flagPercentageView = [flagPercentageViews objectAtIndex:last];
        if (last == 0) {
            percentage = totalPercentageLeft;
        } else {
            percentage = round((float)totalPercentageLeft / (float)i);
        }
        flagPercentageView.percentage = percentage;
        totalPercentageLeft -= percentage;
    }
}

#pragma mark - Table View data source methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 107;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.currentSizes count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    DMSizeMixTableViewCell *cell = (DMSizeMixTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[DMSizeMixTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *sizeInfo = [self.currentSizes objectAtIndex:indexPath.row];
    cell.textLabel.text = [[sizeInfo objectForKey:kDMProjectSizeMixWidth] stringValue];
    
    NSArray *lengths = [sizeInfo objectForKey:@"lengths"];
    for (int i = 0, l = [cell.percentageViews count]; i < l; ++i) {
        id length = nil;
        if (i < [lengths count]) {
            length = [lengths objectAtIndex:i];
        }
        DMFlagPercentageView *flagPercentageView = [cell.percentageViews objectAtIndex:i];
        flagPercentageView.delegate = self;
        if (length == nil || length == [NSNull null]) {
            flagPercentageView.hidden = YES;
        } else {
            flagPercentageView.hidden = NO;
            flagPercentageView.length = [[lengths objectAtIndex:i] integerValue];
            flagPercentageView.gauge = [[sizeInfo objectForKey:kDMProjectSizeMixWidth] integerValue];
            NSString *key = [NSString stringWithFormat:@"%i-%i", flagPercentageView.gauge, flagPercentageView.length];
            NSNumber *percentage = [self.currentProjectSizeMixSizes objectForKey:key];
            if (percentage) {
                flagPercentageView.percentage = [percentage integerValue];
            } else {
                flagPercentageView.percentage = 0;
            }
        }
    }
    
    if ((indexPath.row % 2) == 0) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.00];
    } else {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.00];
    }
    return cell;
}

#pragma mark - Flag Percentage View delegate methods

- (BOOL)flagPercentageView:(DMFlagPercentageView *)flagPercentageView shouldUpdatePercentage:(NSUInteger)toPercentage {
    NSString *updatedKey = [NSString stringWithFormat:@"%i-%i", flagPercentageView.gauge, flagPercentageView.length];
    NSUInteger totalPercentage = 0;
    for (NSString *key in self.currentProjectSizeMixSizes) {
        if (![key isEqualToString:updatedKey]) {
            totalPercentage += [[self.currentProjectSizeMixSizes objectForKey:key] integerValue];
        }
    }
    totalPercentage += toPercentage;
    return totalPercentage > 100 ? NO : YES;
}

- (void)flagPercentageViewUpdatedPercentage:(DMFlagPercentageView *)flagPercentageView {
    NSString *key = [NSString stringWithFormat:@"%i-%i", flagPercentageView.gauge, flagPercentageView.length];
    if (flagPercentageView.percentage == 0) {
        [self.currentProjectSizeMixSizes removeObjectForKey:key];
    } else {
        [self.currentProjectSizeMixSizes setObject:[NSNumber numberWithInteger:flagPercentageView.percentage] forKey:key];
    }
}

#pragma mark - Text Field delegate methods

- (void)textFieldDidEndEditing:(UITextField *)textField {
    CGFloat value = [textField.text floatValue];
    if (value <= 0) {
        textField.text = @"1";
    } else if (textField == self.thicknessField && value > 200) {
        textField.text = @"200";
    }
}

@end
