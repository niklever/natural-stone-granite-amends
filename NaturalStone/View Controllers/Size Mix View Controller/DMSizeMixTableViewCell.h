//
//  DMSizeMixTableViewCell.h
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMSizeMixTableViewCellBase : UITableViewCell

@end

@interface DMSizeMixTableViewCell : DMSizeMixTableViewCellBase

@property (nonatomic,strong) NSArray *percentageViews;

@end
