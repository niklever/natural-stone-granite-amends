//
//  DMFlagPercentageView.h
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMFlagPercentageView;

@protocol DMFlagPercentageViewDelegate <NSObject>

- (BOOL)flagPercentageView:(DMFlagPercentageView*)flagPercentageView shouldUpdatePercentage:(NSUInteger)toPercentage;
- (void)flagPercentageViewUpdatedPercentage:(DMFlagPercentageView*)flagPercentageView;

@end

@interface DMFlagPercentageViewBase : UIView

@property (nonatomic,assign) NSUInteger gauge;
@property (nonatomic,assign) NSUInteger length;
@property (nonatomic,assign) NSUInteger percentage;

@end

@interface DMFlagPercentageView : DMFlagPercentageViewBase

@property (nonatomic,weak) id<DMFlagPercentageViewDelegate> delegate;
@property (nonatomic,readonly) BOOL enabled;

@end
