//
//  DMSizeMixTableViewCell.m
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMSizeMixTableViewCell.h"

#import "DMFlagPercentageView.h"

@implementation DMSizeMixTableViewCellBase

//@property (nonatomic,retain)

@end

@interface DMSizeMixTableViewCell ()


@end

@implementation DMSizeMixTableViewCell

@synthesize percentageViews = __percentageViews;

// Full size 950 x 107

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.textAlignment = UITextAlignmentCenter;
        self.textLabel.font = [UIFont fontWithName:kDMDefaultFont size:20];
        self.detailTextLabel.hidden = YES;
        
        self.percentageViews = [NSArray arrayWithObjects:
                                [[DMFlagPercentageView alloc] initWithFrame:CGRectMake(190, 0, 190, 107)],
                                [[DMFlagPercentageView alloc] initWithFrame:CGRectMake(380, 0, 190, 107)],
                                [[DMFlagPercentageView alloc] initWithFrame:CGRectMake(570, 0, 190, 107)],
                                [[DMFlagPercentageView alloc] initWithFrame:CGRectMake(760, 0, 190, 107)],
                                nil];
        for (UIView *view in self.percentageViews) {
            [self.contentView addSubview:view];
        }
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.textLabel.frame = CGRectMake(0, 0, 190, 107);
}

@end
