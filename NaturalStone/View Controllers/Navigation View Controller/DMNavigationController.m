//
//  DMNavigationController.m
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMNavigationController.h"

#ifdef DEBUG
@interface DebugWebDelegate : NSObject
@end
@implementation DebugWebDelegate
@class WebView;
@class WebScriptCallFrame;
@class WebFrame;
- (void)webView:(WebView *)webView   exceptionWasRaised:(WebScriptCallFrame *)frame
       sourceId:(int)sid
           line:(int)lineno
    forWebFrame:(WebFrame *)webFrame
{
    //NSLog(@"NSDD: exception: sid=%d line=%d function=%@, caller=%@, exception=%@",
    //      sid, lineno, [frame functionName], [frame caller], [frame exception]);
}
@end
@interface DebugWebView : UIWebView {
    id windowScriptObject;
    id privateWebView;
}
-(void)setScriptDebugDelegate:(DebugWebDelegate*)delegate;
@end
@implementation DebugWebView
- (void)webView:(id)sender didClearWindowObject:(id)windowObject forFrame:(WebFrame*)frame
{
    [(DebugWebView *)sender setScriptDebugDelegate:[[DebugWebDelegate alloc] init]];
}
@end
#endif


#import "DMCustomNavBar.h"
#import "DMHomeViewController.h"
#import "DMNewProjectViewController.h"
#import "DMColoursViewController.h"
#import "DMColourMixViewController.h"
#import "DMSizeMixViewController.h"
#import "DMPreviewViewController.h"
#import "DMPatternGenerator.h"
#import "DMProjectLibraryViewController.h"
#import "DMEmailViewController.h"
#import "DMImageCacher.h"
#import <QuartzCore/QuartzCore.h>
#import "SSHomeVC.h"
#import "SSMenuVC.h"
#import "SSGalleryVC.h"
#import "SSMainVC.h"
#import "SSRenderVC.h"

#import "JSONKit.h"

typedef enum {
    ProjectEditStepCreate,
    ProjectEditStepColours,
    ProjectEditStepColourMix,
    ProjectEditStepSizeMix,
    ProjectEditStepPreview,
    ProjectEditStepQuote,
    ProjectEditStepEmail
} ProjectEditSteps;



@interface DMNavigationController ()

@property (nonatomic, strong) DMHomeViewController *homeViewController;
@property (nonatomic,strong) UIAlertView *projectSavedAlert;
@property (nonatomic,strong) UIWebView *dataWebView;
@property (nonatomic,strong) UIAlertView *abandonProjectAlert;
@property (nonatomic,assign) ProjectEditSteps currentProjectStep;
@property (nonatomic,strong) NSDate *emailProjectSyncStartTime;
@property (nonatomic,strong) NSTimer *emailProjectSyncTimer;
@property (nonatomic,strong) DMEmailViewController *emailViewController;
@property (nonatomic,strong) UIAlertView *emailErrorAlertView;
@property (nonatomic,strong) UIAlertView *emailSuccessAlertView;
@property (nonatomic,assign) BOOL needToSendEmail;
@property (nonatomic,assign) BOOL needToSendQuote;
@property (nonatomic,strong) NSString *lastEmailAction;
@property (nonatomic,strong) NSDictionary *lastEmailParams;
@property (nonatomic, strong) SSMenuVC *ssMenuVC;
@property (nonatomic, strong) SSHomeVC *ssHomeVC;

//self.viewController = [[UINavigationController alloc] initWithRootViewController:[[SSHomeVC alloc] initWithNibName:@"SSHomeVC" bundle:nil]];
//[self.viewController setNavigationBarHidden:YES animated:YES];

//ss_menu = [[SSMenuVC alloc] initWithNibName:@"SSMenuVC" bundle:nil];

- (void)createProjectWithProject:(DMProject*)newProject;
- (void)generatePreview;
- (void)setPreview;
- (void)sendEmailIfProjectReady;

@end

@implementation DMNavigationController

@synthesize currentProject = __currentProject;
//@synthesize projectLibrary = __projectLibrary;
@synthesize dataWebView = __dataWebView;

@synthesize homeViewController = __homeViewController;
@synthesize currentProjectStep = __currentProjectStep;
@synthesize projectSavedAlert = __projectSavedAlert;
@synthesize abandonProjectAlert = __abandonProjectAlert;
@synthesize navBar = __navBar;
@synthesize navController = __navController;
@synthesize emailProjectSyncStartTime = __emailProjectSyncStartTime;
@synthesize emailProjectSyncTimer = __emailProjectSyncTimer;
@synthesize emailViewController = __emailViewController;
@synthesize emailErrorAlertView = __emailErrorAlertView;
@synthesize emailSuccessAlertView = __emailSuccessAlertView;
@synthesize needToSendEmail = __needToSendEmail;
@synthesize needToSendQuote = __needToSendQuote;
@synthesize lastEmailAction = __lastEmailAction;
@synthesize lastEmailParams = __lastEmailParams;
@synthesize activeVC;
@synthesize ssMenuVC;
@synthesize ssHomeVC;
@synthesize stonespar_dct;
@synthesize stonesparEmail_dct;

- (id)initWithHomeViewController {
    self = [super init];
    if (self) {
//        self.projectLibrary = [NSMutableArray array];
//        NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"library.plist"];
//        NSArray *jsonLibrary = [NSArray arrayWithContentsOfFile:path];
//        for (NSDictionary *jsonProject in jsonLibrary) {
//            [self.projectLibrary addObject:[DMProject projectFromJSONDictionary:jsonProject]];
//        }
        
#ifdef DEBUG
        self.dataWebView = [[DebugWebView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
#else
        self.dataWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
#endif
        self.dataWebView.delegate = self;
        [self.dataWebView loadRequest:[NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:@"NaturalStoneSync" withExtension:@"html" subdirectory:@"nssync"]]];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    ssHomeVC = [[SSHomeVC alloc] initWithNibName:@"SSHomeVC" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:ssHomeVC];
    [navigationController setNavigationBarHidden:YES animated:NO];
    
    [self addChildViewController:navigationController];
    [self.view addSubview:navigationController.view];
    self.navController = navigationController;
    
    ssMenuVC = [[SSMenuVC alloc] initWithNibName:@"SSMenuVC" bundle:nil];
    ssMenuVC.navController = self;
    [self.view addSubview:ssMenuVC.view];
    
    //navigationController = nil;
    
    DMCustomNavBar *navBar = [[DMCustomNavBar alloc] initWithFrame:CGRectMake(0, 0, 1024, 77)];
    self.navBar = navBar;
    self.navBar.hidden = YES;
    [self.view addSubview:navBar];
    
    navBar = nil;
}

- (NSArray*) projectLibrary {
    NSString *libraryJSON = [self.dataWebView stringByEvaluatingJavaScriptFromString:@"JSON.stringify(NaturalStoneSync.getProjectLibrary())"];
    NSArray *library = [libraryJSON objectFromJSONString];
    if (![library isKindOfClass:[NSArray class]]) {
        library = [NSArray array];
    } else {
        NSMutableArray *projectLibrary = [NSMutableArray arrayWithCapacity:[library count]];
        for (__strong NSDictionary* projectDictionary in library) {
            NSString *type = [projectDictionary objectForKey:@"ProjectTypeName"];
            [self LogColours:projectDictionary withPrefix:@"Before swap:"];
            projectDictionary = [self swapOldColours:projectDictionary];
            [self LogColours:projectDictionary withPrefix:@"After swap:"];
            [projectLibrary addObject:[DMProject projectFromJSONDictionary:projectDictionary]];
        }
        library = [NSArray arrayWithArray:projectLibrary];
    }
    return library;
}

-(void)LogColours:(NSDictionary*)dict withPrefix:(NSString*)prefix{
    NSString *str = prefix;
    NSArray *colours = [dict objectForKey:@"ProjectColours"];
    for(NSDictionary* colour in colours){
        str = [NSString stringWithFormat:@"%@%@,", str, [colour objectForKey:@"Reference"]];
    }
    NSLog(@"DMNavigationController.LogColours %@", str);
}

-(NSDictionary*)swapOldColours:(NSDictionary*)dict{
    NSArray *projectColours = DMProject.coloursData;
    NSMutableDictionary *newProject = [dict mutableCopy];
    NSMutableArray *colours = [[newProject objectForKey:@"ProjectColours"] mutableCopy];
    bool changed = false;
    for (int i=0; i<[colours count]; i++) {
        NSMutableDictionary* colour = [[colours objectAtIndex:i] mutableCopy];
        NSString *ref = [colour objectForKey:@"Reference"];
        if ([ref hasPrefix:@"gra9"]){
            for (NSDictionary *projectColour in projectColours) {
                NSString *ref1 = [projectColour objectForKey:@"oldref"];
                NSNumber *num = [projectColour objectForKey:@"discontinued"];
                if (ref1!=NULL && ([ref1 isEqualToString:ref] && num==NULL)){
                    NSString *name = [projectColour objectForKey:@"name"];
                    [colour setObject:[name lowercaseString] forKey:@"Reference"];
                    [colours replaceObjectAtIndex:i withObject:colour];
                    changed = true;
                    break;
                }
            }
        }
    }
    if (changed) [newProject setObject:colours forKey:@"ProjectColours"];
    return newProject;
}


- (BOOL)projectLibraryHasUnsyncedChanges {
    if ([DMImageCacher sharedInstance].syncing) {
        return YES;
    }
    for (DMProject *project in self.projectLibrary) {
        if (!project.synced) {
            return YES;
        } else if (![project projectImageHasBeenSynced:kDMProjectImageLarge]) {
            return YES;
        }
    }
    NSString *syncUnsavedChangesJSON = [self.dataWebView stringByEvaluatingJavaScriptFromString:@"JSON.stringify(NaturalStoneSync.hasUnsavedChanges())"];
    BOOL syncUnsavedChanges = ![syncUnsavedChangesJSON isEqualToString:@"false"];

    return syncUnsavedChanges;
}

- (void)showPhotoGallery {
    if (![activeVC isEqualToString:@"Gallery"]){
        activeVC = @"Gallery";
        SSGalleryVC *vc = [[SSGalleryVC alloc] initWithNibName:@"SSGalleryVC" bundle:nil];
        [self.navController pushViewController:vc animated:YES];
        vc.menu_vc = self.ssMenuVC;
        self.ssMenuVC.hideAfterAnimation = true;
        self.navBar.hidden = YES;
    }
}

- (void)showStonesparRender{
    if (![activeVC isEqualToString:@"StonesparRender"]){
        activeVC = @"StonesparRender";
        SSRenderVC *vc = [[SSRenderVC alloc] initWithNibName:@"SSRenderVC" bundle:nil];
        [self.navController pushViewController:vc animated:YES];
        vc.menu_vc = self.ssMenuVC;
        vc.data = stonespar_dct;
        vc.delegate = self;
        self.navBar.hidden = YES;
        self.ssMenuVC.hideAfterAnimation = true;
    }
}

- (void)showStonespar {
    if (![activeVC isEqualToString:@"Stonespar"]){
        activeVC = @"Stonespar";
        SSMainVC *vc = [[SSMainVC alloc] initWithNibName:@"SSMainVC" bundle:nil];
        [self.navController pushViewController:vc animated:YES];
        vc.menu_vc = self.ssMenuVC;
        vc.delegate = self;
        self.navBar.hidden = YES;
        self.ssMenuVC.hideAfterAnimation = true;
    }
}

- (void)showProjectLibrary {
    if (![self.navController.visibleViewController.title isEqualToString:@"Library"]){
        activeVC = @"Library";
        DMProjectLibraryViewController *viewController = [[DMProjectLibraryViewController alloc] initWithNibName:@"DMProjectLibraryViewController" bundle:nil];
        viewController.title = @"Library";
        [self.navController pushViewController:viewController animated:YES];
        [self.navBar updateForViewController:viewController transitionDirection:DMTransitionRightToLeft buttonStates:DMNavigationHomeButton|DMNavigationSearchButton|DMNavigationProjectFilter];
        self.navBar.projectFilter.selectedSegmentIndex = 0;
        self.navBar.hidden = NO;
        self.ssMenuVC.hideAfterAnimation = true;
        self.ssMenuVC.dummy_menu_btn = nil;
         viewController = nil;
    }
}

- (void)showGranite {
    if (![activeVC isEqualToString:@"Granite"]){
        activeVC = @"Granite";
        self.ssMenuVC.hideAfterAnimation = true;
        self.ssMenuVC.dummy_menu_btn = nil;
        [self createProjectWithProject:[[DMProject alloc] init]];
    }
}

- (void)previewStonesparProject:(DMProject*)project {
    self.currentProject = project;
    activeVC = @"StonesparRender";
    SSRenderVC *vc = [[SSRenderVC alloc] initWithProject:project];
    [self.navController pushViewController:vc animated:YES];
    vc.menu_vc = self.ssMenuVC;
    vc.delegate = self;
    self.navBar.hidden = YES;
    self.ssMenuVC.hideAfterAnimation = true;
}

- (void)createProjectWithProject:(DMProject*)newProject {
    self.currentProjectStep = ProjectEditStepCreate;
    self.currentProject = newProject;
    
    DMNewProjectViewController *viewController = [[DMNewProjectViewController alloc] initWithNibName:@"DMNewProjectViewController" bundle:nil];
    [self.navController pushViewController:viewController animated:YES];
    self.navBar.hidden = NO;
    [self.navBar updateForViewController:viewController transitionDirection:DMTransitionRightToLeft buttonStates:(DMNavigationHomeButton|DMNavigationForwardButton)];
     viewController = nil;    
}

- (void)createProjectDuplicatingProject:(DMProject*)existingProject {
    DMProject *project = [existingProject mutableCopy];
    [project swapOldColours];
    [self createProjectWithProject:project];
}

- (void)goBack {
    DMProjectViewControllerBase *liveViewController = (DMProjectViewControllerBase*)[self.navController.viewControllers lastObject];
    if (![liveViewController updateProjectProperties:NO]) {
        return;
    }
    [self.navController popViewControllerAnimated:YES];
    if (self.currentProjectStep) {
        self.currentProjectStep -= 1;
    }
    if (self.currentProjectStep == ProjectEditStepColourMix && [self.currentProject.colours count] == 1) {
        // Skip colour mix
        self.currentProjectStep -= 1;
    }
    if (self.currentProject && self.currentProject.saved) {
        self.currentProject = nil;
        [self.navBar updateForViewController:[self.navController.viewControllers lastObject] transitionDirection:DMTransitionLeftToRight buttonStates:DMNavigationHomeButton|DMNavigationSearchButton];
    } else if (self.currentProjectStep == ProjectEditStepCreate) {
        [self.navBar updateForViewController:[self.navController.viewControllers lastObject] transitionDirection:DMTransitionLeftToRight buttonStates:DMNavigationHomeButton|DMNavigationForwardButton];
    } else {
        [self.navBar updateForViewController:[self.navController.viewControllers lastObject] transitionDirection:DMTransitionLeftToRight buttonStates:DMNavigationHomeButton|DMNavigationForwardButton|DMNavigationBackButton];
    }
}

- (void)sendEmail {
    if (![self saveProject]) {
        return;
    }
    DMEmailViewController *viewController = [[DMEmailViewController alloc] initWithNibName:@"DMEmailViewController" bundle:nil];
    viewController.modalPresentationStyle = UIModalPresentationPageSheet;
    viewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    viewController.delegate = self;
    self.currentProjectStep = ProjectEditStepEmail;
    self.emailViewController = viewController;
    [self presentViewController:viewController animated:YES completion:nil];
    
     viewController = nil;
}

- (void)initiateQuote {
    if (![self saveProject]) {
        return;
    }
    DMEmailViewController *viewController = [[DMEmailViewController alloc] initWithNibName:@"DMEmailViewController" bundle:nil];
    viewController.modalPresentationStyle = UIModalPresentationPageSheet;
    viewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    viewController.delegate = self;
    self.currentProjectStep = ProjectEditStepQuote;
    self.emailViewController = viewController;
    [self presentViewController:viewController animated:YES completion:nil];
    
     viewController = nil;
}

- (void)sendStonesparEmail:(DMProject *)project  from:(NSString *)from to:(NSString*)to message:(NSString*)message quote:(BOOL)quote{
    self.currentProject = project;
    
    self.emailProjectSyncStartTime = [NSDate date];

    self.needToSendEmail = !quote;
    self.needToSendQuote = quote;
    self.stonesparEmail_dct = [NSDictionary dictionaryWithObjectsAndKeys:
                            from, @"FromAddress",
                            to, @"ToAddress",
                            message, @"Content",
                            [NSNumber numberWithBool:quote], @"SendQuote",
                            [project.properties objectForKey:kDMProjectPropertyClientID], kDMProjectPropertyClientID,
                            nil];
    
    [self.emailProjectSyncTimer invalidate];
    self.emailProjectSyncTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                  target:self
                                                                selector:@selector(sendStonesparEmailIfProjectReady)
                                                                userInfo:nil
                                                                 repeats:YES];
}

- (void)sendStonesparEmailIfProjectReady{
    // Don't need to check the project ID as the image won't be synced unless the ID is set
    BOOL forceSend = NO;
    if ([self.emailProjectSyncStartTime timeIntervalSinceNow] < kDMEmailProjectSyncMaxTime)  forceSend = YES;

    if (forceSend || [self.currentProject projectImageHasBeenSynced:kDMProjectImageLarge]) {
        DMProject *savedProject = nil;
        for (DMProject *project in self.projectLibrary) {
            if ([[project.properties objectForKey:kDMProjectPropertyClientID] isEqualToString:[self.currentProject.properties objectForKey:kDMProjectPropertyClientID]]) {
                savedProject = project;
                break;
            }
        }

        NSString *action = nil;
        
        if (self.needToSendEmail) {
            action = @"sendEmail";
            self.needToSendEmail = NO;
        } else if (self.needToSendQuote) {
            action = @"sendQuote";
            self.needToSendQuote = NO;
        }
        self.lastEmailAction = action;
        self.lastEmailParams = self.stonesparEmail_dct;
        
        if (action) {
            NSString *javascript = [NSString stringWithFormat:@"NaturalStoneSync.%@(%@)",
                                    action,
                                    [self.stonesparEmail_dct JSONString]];
            [self.dataWebView stringByEvaluatingJavaScriptFromString:javascript];
            [self.emailProjectSyncTimer invalidate];
            self.emailProjectSyncTimer = nil;
            // And wait for the success or failure message to be fired
        }
    }
}

- (void)sendEmailIfProjectReady {
    // Don't need to check the project ID as the image won't be synced unless the ID is set
    BOOL forceSend = NO;
    if ([self.emailProjectSyncStartTime timeIntervalSinceNow] < kDMEmailProjectSyncMaxTime) {
        forceSend = YES;
    }
    if (forceSend || ([self.currentProject projectImageHasBeenSynced:kDMProjectImageLarge]&&[self.currentProject projectImageHasBeenSynced:kDMProjectImageHTML])) {
        DMProject *savedProject = nil;
        for (DMProject *project in self.projectLibrary) {
            if ([[project.properties objectForKey:kDMProjectPropertyClientID] isEqualToString:[self.currentProject.properties objectForKey:kDMProjectPropertyClientID]]) {
                savedProject = project;
                break;
            }
        }
        if (!savedProject) {
            // We'll rely on the clientID to get us through
            // This should never really happen but if we wait we'll time-out at least.
//            return;
        }
        NSString *to = self.emailViewController.toField.text,
        *from = self.emailViewController.fromField.text,
        *content = self.emailViewController.contentTextView.text;
        BOOL sendQuote = self.emailViewController.quoteSwitch.on;
        if (!to) to = @"";
        if (!from) from = @"";
        if (!content) content = @"";
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                from, @"FromAddress",
                                to, @"ToAddress",
//                                self.emailViewController.subjectField.text, @"Subject",
                                content, @"Content",
                                [NSNumber numberWithBool:sendQuote], @"SendQuote",
                                [[savedProject properties] objectForKey:kDMProjectPropertyClientID], kDMProjectPropertyClientID,
                                nil];
        NSDictionary *fullObject = params;
        NSString *action = nil;
        if (self.needToSendEmail) {
            action = @"sendEmail";
            self.needToSendEmail = NO;
        } else if (self.needToSendQuote) {
            action = @"sendQuote";
            self.needToSendQuote = NO;
        }
        self.lastEmailAction = action;
        self.lastEmailParams = fullObject;
        if (action) {
            NSString *javascript = [NSString stringWithFormat:@"NaturalStoneSync.%@(%@)",
                                    action,
                                    [fullObject JSONString]];
            [self.dataWebView stringByEvaluatingJavaScriptFromString:javascript];    
            [self.emailProjectSyncTimer invalidate];
            self.emailProjectSyncTimer = nil;
            // And wait for the success or failure message to be fired
        } else {
            // All done!
            self.emailSuccessAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(self.currentProjectStep == ProjectEditStepEmail?@"EMAIL_SEND_SUCCESS_TITLE":@"QUOTE_SEND_SUCCESS_TITLE", @"")
                                                                     message:NSLocalizedString(self.currentProjectStep == ProjectEditStepEmail?@"EMAIL_SEND_SUCCESS_MESSAGE":@"QUOTE_SEND_SUCCESS_MESSAGE", @"")
                                                                    delegate:self
                                                           cancelButtonTitle:NSLocalizedString(@"CLOSE", @"")
                                                           otherButtonTitles:nil];
            [self.emailSuccessAlertView show];
        }
        return;
    }
}

- (void)generatePreview {
    @autoreleasepool {
        [self.currentProject patternPreview];
    }
    
    [self performSelectorOnMainThread:@selector(setPreview) withObject:nil waitUntilDone:NO];
}

- (void)setPreview {
    DMPreviewViewController *viewController = [self.navController.viewControllers lastObject];
    if ([viewController isKindOfClass:[DMPreviewViewController class]]) {
        viewController.project = self.currentProject;
    }
}

- (void)goForwards {
    UIViewController *viewController = nil;
    DMProjectViewControllerBase *liveViewController = (DMProjectViewControllerBase*)[self.navController.viewControllers lastObject];
    if (![liveViewController updateProjectProperties:YES]) {
        return;
    }
    DMNavigationButtons buttonsState = DMNavigationHomeButton|DMNavigationForwardButton|DMNavigationBackButton;

    switch (self.currentProjectStep) {
        case ProjectEditStepCreate:
            self.currentProjectStep = ProjectEditStepColours;
            
            viewController = [[DMColoursViewController alloc] initWithNibName:@"DMColoursViewController" bundle:nil];
            
            break;
        case ProjectEditStepColours:
            if ([self.currentProject.colours count] > 1) {
                self.currentProjectStep = ProjectEditStepColourMix;
                
                viewController = [[DMColourMixViewController alloc] initWithNibName:@"DMColourMixViewController" bundle:nil];
                break;
            }
            // Otherwise drop straight to edit step
        case ProjectEditStepColourMix:
            self.currentProjectStep = ProjectEditStepSizeMix;
            
            viewController = [[DMSizeMixViewController alloc] initWithNibName:@"DMSizeMixViewController" bundle:nil];
            
            break;
        case ProjectEditStepSizeMix:
            self.currentProjectStep = ProjectEditStepPreview;
            
            viewController = [[DMPreviewViewController alloc] initWithNibName:@"DMPreviewViewController" bundle:nil];

//            [(DMPreviewViewController*)viewController setPatternPreview:[self.currentProject patternPreview]];
            [self performSelectorInBackground:@selector(generatePreview) withObject:nil];
                        
            buttonsState = DMNavigationHomeButton | DMNavigationBackButton | DMNavigationSaveButton | DMNavigationExportButton;
            
            break;
        default:
            break;
    }
    if (viewController) {
        [self.navController pushViewController:viewController animated:YES];
        [self.navBar updateForViewController:viewController transitionDirection:DMTransitionRightToLeft buttonStates:buttonsState];
         viewController = nil;
    }
}

- (void)goHome {
    BOOL unsavedProject = NO;
    if (self.currentProject) {
        unsavedProject = !self.currentProject.saved;
    }
    if (unsavedProject && self.currentProject && [self.currentProject isTitleAndLocationValid]) {
        self.abandonProjectAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ABANDON_PROJECT_TITLE", @"")
                                                               message:NSLocalizedString(@"ABANDON_PROJECT_MESSAGE", @"")
                                                              delegate:self
                                                     cancelButtonTitle:NSLocalizedString(@"CANCEL", @"")
                                                     otherButtonTitles:NSLocalizedString(@"ABANDON_PROJECT_BUTTON", @""), nil];
        [self.abandonProjectAlert show];
    } else {
        [self.navController popToRootViewControllerAnimated:YES];
    }
    
    self.navBar.hidden = YES;
    self.ssMenuVC.view.hidden = NO;
    self.ssMenuVC.hideAfterAnimation = NO;
    
    self.currentProject = nil;
    activeVC = @"";
}

-(void)listVCs{
    NSArray * controllerArray = [self.navController viewControllers];
    
    for (UIViewController *controller in controllerArray){
        //Code here.. e.g. print their titles to see the array setup;
        NSLog(@"%@",controller.title);
    }
}

- (void)saveAndCloseProject {
    self.navBar.hidden = YES;
    self.ssMenuVC.view.hidden = NO;
    self.ssMenuVC.hideAfterAnimation = NO;
    
    if (self.currentProject.saved) {
        activeVC = @"";
        [self.navController popToRootViewControllerAnimated:YES];
        self.currentProject = nil;
        return;
    }
    if ([self saveProject]) {
        self.currentProject = nil;
        self.projectSavedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PROJECT_SAVED_TITLE", @"")
                                                              message:NSLocalizedString(@"PROJECT_SAVED_MESSAGE", @"")
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"CLOSE", @"")
                                                    otherButtonTitles:nil];
        [self.projectSavedAlert show];
    }
}

- (BOOL)saveStonesparProject:(DMProject *)project {
    if (project.saved) return YES;
    
    NSDictionary *jsonDict = [project fullDictionaryForStonesparJSON];
    //[jsonDict setValue:@"uuid" forKey:kDMProjectPropertyUser];
    NSError *error;
    NSString *json = [jsonDict JSONStringWithOptions:JKSerializeOptionNone error:&error];
    NSLog(@"saveStonesparProject json:%@", json);
    project.saved = YES;
    [self.dataWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"NaturalStoneSync.createProject(%@)", json]];
    
    return YES;
}

- (BOOL)saveProject {
    if (self.currentProject.saved) {
        return YES;
    }
    DMProjectViewControllerBase *liveViewController = (DMProjectViewControllerBase*)[self.navController.viewControllers lastObject];
    if (![liveViewController updateProjectProperties:YES]) {
        return NO;
    }
    NSDictionary *jsonDict = [self.currentProject fullDictionaryForJSON];
    NSString *json = [jsonDict JSONString];
    self.currentProject.saved = YES;
    [self.dataWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"NaturalStoneSync.createProject(%@)", json]];
    DMPreviewViewController *viewController = [self.navController.viewControllers lastObject];
    if ([viewController isKindOfClass:[DMPreviewViewController class]]) {
        [self.navBar setButtonsState:DMNavigationHomeButton|DMNavigationExportButton|DMNavigationSaveButton];
    }
    return YES;
}

- (void)previewProject:(DMProject*)project {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[project pathForImage:kDMProjectImageHTML]]) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PREVIEW_NOT_SYNCED_TITLE", @"")                           
                                     message:NSLocalizedString(@"PREVIEW_NOT_SYNCED_MESSAGE", @"")               
                                    delegate:self
                           cancelButtonTitle:NSLocalizedString(@"CLOSE", @"")
                           otherButtonTitles:nil] show];
        return;
    }
    self.currentProject = project;
    self.currentProjectStep = ProjectEditStepPreview;
    DMPreviewViewController *viewController = [[DMPreviewViewController alloc] initWithNibName:@"DMPreviewViewController" bundle:nil];
    viewController.project = project;
    [self.navController pushViewController:viewController animated:YES];
    [self.navBar updateForViewController:viewController transitionDirection:DMTransitionRightToLeft buttonStates:DMNavigationHomeButton|DMNavigationSaveButton|DMNavigationExportButton|DMNavigationLibraryButton];
     viewController = nil;
}

- (void)deleteProject:(DMProject*)project {
    [project deleteImages];
    NSString *javascript = [NSString stringWithFormat:@"NaturalStoneSync.deleteProject(%@)", [[project.properties objectForKey:kDMProjectPropertyClientID] JSONString]];
    [self.dataWebView stringByEvaluatingJavaScriptFromString:javascript];
}

- (NSString*)authToken {
    NSString *javascript = @"NaturalStoneSync.authToken()";
    return [self.dataWebView stringByEvaluatingJavaScriptFromString:javascript];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView == self.projectSavedAlert) {
        [self goHome];
        self.projectSavedAlert = nil;
    } else if (alertView == self.abandonProjectAlert) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            [self.navController popToRootViewControllerAnimated:YES];
            [self.currentProject deleteImages];
            self.currentProject = nil;
        }
    } else if (alertView == self.emailErrorAlertView) {
        [self.emailViewController setActive:NO];
        self.emailErrorAlertView = nil;
    } else if (alertView == self.emailSuccessAlertView) {
        [self dismissModalViewControllerAnimated:YES];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Web View delegate methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog([[request URL] scheme] );
    if ([[[request URL] scheme] isEqualToString:@"log"]) {
        NSLog(@"%@",[self.dataWebView stringByEvaluatingJavaScriptFromString:@"NaturalStoneSync.purgeLog()"]);
        return NO;
    }
    if ([[[request URL] scheme] isEqualToString:@"update"]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kDMProjectSyncCompletedNotification object:self];
        
        [[DMImageCacher sharedInstance] beginSync];
        
        return NO;
    }
    if ([[[request URL] scheme] isEqualToString:@"emailsuccess"]) {
        [self sendEmailIfProjectReady];
    }
    if ([[[request URL] scheme] isEqualToString:@"emailfailed"]) {
        NSMutableArray *emailActions = [NSMutableArray arrayWithCapacity:2];
        [emailActions addObject:self.lastEmailAction];
        if (self.needToSendEmail) {
            // Shouldn't really happen as this should always
            // be in self.lastEmailAction but just in case the
            // order changes..
            [emailActions addObject:@"sendEmail"];
        }
        if (self.needToSendQuote) {
            [emailActions addObject:@"sendQuote"];
        }
        for (NSString *action in emailActions) {
            NSString *javascript = [NSString stringWithFormat:@"NaturalStoneSync.queueEmail(%@,%@,%@)",
                                    [action JSONString],
                                    [[self.currentProject.properties objectForKey:kDMProjectPropertyClientID] JSONString],
                                    [self.lastEmailParams JSONString]];
            [self.dataWebView stringByEvaluatingJavaScriptFromString:javascript]; 
        }
        
        self.emailSuccessAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(self.currentProjectStep == ProjectEditStepEmail?@"EMAIL_SEND_ERROR_TITLE":@"QUOTE_SEND_ERROR_TITLE", @"")
                                                                 message:NSLocalizedString(self.currentProjectStep == ProjectEditStepEmail?@"EMAIL_SEND_ERROR_MESSAGE":@"QUOTE_SEND_ERROR_MESSAGE", @"")
                                                                delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"CLOSE", @"")
                                                       otherButtonTitles:nil];
        [self.emailSuccessAlertView show];
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *clearLocalStorage = @"false";
    if ([userDefaults boolForKey:@"clearLocalStorage"]) {
        clearLocalStorage = @"true";
        [userDefaults setBool:NO forKey:@"clearLocalStorage"];
    }
    NSString *activateJS = [NSString stringWithFormat:@"NaturalStoneSync.setiOSActive('%@', '%@', %@)", [userDefaults objectForKey:kDefaultAPIBaseKey], kDefaultAPIPassword, clearLocalStorage];
    [self.dataWebView stringByEvaluatingJavaScriptFromString:activateJS];
}

- (void)emailViewControllerDidCancel:(DMEmailViewController *)emailViewController {
    self.currentProjectStep = ProjectEditStepPreview;
    [self dismissModalViewControllerAnimated:YES];
    self.emailViewController = nil;
}

- (void)emailViewControllerShouldSend:(DMEmailViewController *)emailViewController {
    [self.emailViewController setActive:YES];
    self.emailProjectSyncStartTime = [NSDate date];
    self.needToSendEmail = YES;
//    self.needToSendQuote = self.emailViewController.quoteSwitch.on;
    [self.emailProjectSyncTimer invalidate];
    self.emailProjectSyncTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                  target:self
                                                                selector:@selector(sendEmailIfProjectReady)
                                                                userInfo:nil
                                                                 repeats:YES];
}
@end
