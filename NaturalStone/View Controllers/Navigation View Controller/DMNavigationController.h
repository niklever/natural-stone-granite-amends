//
//  DMNavigationController.h
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DMProject.h"
#import "DMEmailViewController.h"

#define kDMProjectSyncCompletedNotification @"kDMProjectSyncCompletedNotification"
#define kDMDefaultFont @"MyriadPro-Light"
#define kDMDefaultBoldFont @"MyriadPro-Regular"
#define kDMDefaultFormFont @"HelveticaNeue-Light"
#define kDMDefaultTallFont @"HelveticaNeue-Light"
//#define kDMDefaultFont @"Helvetica"
//#define kDMDefaultFormFont [UIFont systemFontOfSize:12.0].fontName
#define kDMEmailProjectSyncMaxTime -10.0

@class DMCustomNavBar;

@interface DMNavigationController : UIViewController <UIAlertViewDelegate,UIWebViewDelegate,DMEmailViewControllerDelegate>

@property (nonatomic,strong) DMCustomNavBar *navBar;
@property (nonatomic,strong) DMProject *currentProject;
@property (strong, nonatomic,readonly) NSArray *projectLibrary;
@property (strong, nonatomic) NSDictionary *stonespar_dct;
@property (strong, nonatomic) NSDictionary *stonesparEmail_dct;
@property (nonatomic,strong) UINavigationController *navController;
@property (nonatomic, strong) NSString *activeVC;

- (id)initWithHomeViewController;

- (void)showProjectLibrary;
- (void)showPhotoGallery;
- (void)showStonespar;
- (void)showGranite;
- (void)showStonesparRender;
- (void)goBack;
- (void)goForwards;
- (void)goHome;
- (BOOL)saveProject;
- (BOOL)saveStonesparProject:(DMProject *)project;
- (void)deleteProject:(DMProject*)project;
- (void)createProjectDuplicatingProject:(DMProject*)existingProject;
- (void)previewProject:(DMProject*)project;
- (void)previewStonesparProject:(DMProject*)project;
- (void)sendEmail;
- (void)sendStonesparEmail:(DMProject *)project from:(NSString *)from to:(NSString*)to message:(NSString*)message quote:(BOOL)quote;
- (void)initiateQuote;
- (NSString*)authToken;
- (BOOL)projectLibraryHasUnsyncedChanges;
- (void)listVCs;


@end
