//
//  DMNewProjectViewController.m
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMNewProjectViewController.h"

#import <QuartzCore/QuartzCore.h>

@interface DMNewProjectViewController ()

@property (nonatomic, strong) IBOutlet UIView *wrapperView;
@property (nonatomic, strong) IBOutlet UIView *shadowView;
@property (nonatomic, strong) IBOutlet UILabel *projectTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *locationLabel;
@property (nonatomic, strong) IBOutlet UITextField *projectTitleField;
@property (nonatomic, strong) IBOutlet UITextField *locationField;
@property (nonatomic, strong) IBOutlet UIView *projectTitleFieldWrapper;
@property (nonatomic, strong) IBOutlet UIView *locationFieldWrapper;

@end

@implementation DMNewProjectViewController

@synthesize wrapperView = __wrapperView;
@synthesize shadowView = __shadowView;
@synthesize projectTitleLabel = __projectTitleLabel;
@synthesize locationLabel = __locationLabel;
@synthesize projectTitleField = __projectTitleField;
@synthesize locationField = __locationField;
@synthesize projectTitleFieldWrapper = __projectTitleFieldWrapper;
@synthesize locationFieldWrapper = __locationFieldWrapper;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"NEW_PROJECT_TITLE", @"");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self applyStandardRoundedShadowEffect:self.shadowView];
    
    self.projectTitleLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:25.0f];
    self.projectTitleLabel.text = NSLocalizedString(@"PROJECT_TITLE_LABEL", @"");
    self.locationLabel.font = [UIFont fontWithName:kDMDefaultBoldFont size:25.0f];
    self.locationLabel.text = NSLocalizedString(@"LOCATION_LABEL", @"");
    self.projectTitleField.placeholder = NSLocalizedString(@"PROJECT_TITLE_PLACEHOLDER", @"");
    self.locationField.placeholder = NSLocalizedString(@"LOCATION_PLACEHOLDER", @"");
    [self.projectTitleField fixForDolphin];
    [self.locationField fixForDolphin];
    
    // This uses a private value but we're not being reviewed anyway:
    [self.projectTitleField setValue:[UIColor colorWithRed:0.77 green:0.52 blue:0.23 alpha:0.5f] 
                    forKeyPath:@"_placeholderLabel.textColor"];
    [self.locationField setValue:[UIColor colorWithRed:0.77 green:0.52 blue:0.23 alpha:0.5f] 
                          forKeyPath:@"_placeholderLabel.textColor"];
    
    self.projectTitleField.text = [kDMNavigationControllerRef.currentProject.properties objectForKey:kDMProjectPropertyTitle];
    self.locationField.text = [kDMNavigationControllerRef.currentProject.properties objectForKey:kDMProjectPropertyLocation];
    [self.projectTitleField becomeFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.wrapperView = nil;
    self.shadowView = nil;
    self.projectTitleLabel = nil;
    self.locationLabel = nil;
    self.projectTitleField = nil;
    self.locationField = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (BOOL)updateProjectProperties:(BOOL)validate {
    if (self.projectTitleField.text) {
        [kDMNavigationControllerRef.currentProject.properties setObject:self.projectTitleField.text forKey:kDMProjectPropertyTitle];
    } else {
        [kDMNavigationControllerRef.currentProject.properties removeObjectForKey:kDMProjectPropertyTitle];
    }
    if (self.locationField.text) {
        [kDMNavigationControllerRef.currentProject.properties setObject:self.locationField.text forKey:kDMProjectPropertyLocation];
    } else {
        [kDMNavigationControllerRef.currentProject.properties removeObjectForKey:kDMProjectPropertyLocation];
    }

    if (validate && ![kDMNavigationControllerRef.currentProject isTitleAndLocationValid]) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PROJECT_TITLE_OR_LOCATION_INVALID_TITLE", @"")
                                     message:NSLocalizedString(@"PROJECT_TITLE_OR_LOCATION_INVALID_MESSAGE", @"")
                                    delegate:nil
                           cancelButtonTitle:NSLocalizedString(@"CLOSE", @"")
                           otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}
#pragma mark - Text Field delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField.text length] == 0) {
        [self updateProjectProperties:YES];
        return NO;
    }
    
    if (textField.returnKeyType == UIReturnKeyGo) {
        // Make sure we saved the changes
        [self textFieldDidEndEditing:textField];
        
        [kDMNavigationControllerRef goForwards];
    } else {
        if (textField == self.projectTitleField) {
            [self.locationField becomeFirstResponder];
        } else {
            [self.projectTitleField becomeFirstResponder];
        }
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ((textField == self.projectTitleField && [self.locationField.text length])
        || (textField == self.locationField && [self.projectTitleField.text length])) {
        textField.returnKeyType = UIReturnKeyGo;
    } else {
        textField.returnKeyType = UIReturnKeyNext;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *propertyKey = nil;
    if (textField == self.projectTitleField) {
        propertyKey = kDMProjectPropertyTitle;
    } else if (textField == self.locationField) {
        propertyKey = kDMProjectPropertyLocation;
    }
    if (propertyKey) {
        [kDMNavigationControllerRef.currentProject.properties setObject:textField.text forKey:propertyKey];
    }
}

@end
