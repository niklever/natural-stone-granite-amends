//
//  DMNewProjectViewController.h
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMNewProjectViewController : DMProjectViewControllerBase <UITextFieldDelegate>

@end
