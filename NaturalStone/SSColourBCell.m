//
//  SSColourBCell.m
//  NaturalStone
//
//  Created by Nik Lever on 30/04/2014.
//
//

#import "SSColourBCell.h"

@implementation SSColourBCell
@synthesize colour_img;
@synthesize colour_lbl;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
