//
//  UIImage+DMExtensions.m
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImage+DMExtensions.h"

@implementation UIImage (DMExtensions)

+ (UIImage*)colourSwatchImageForRef:(NSString*)ref {
    NSLog(@"UIImage+DMExtension colourSwatchImageForRef colors-%@.jpg", ref);
    return [UIImage imageNamed:[NSString stringWithFormat:@"color-mix-%@.jpg", ref]];
}

@end
