//
//  NSDate+parseXMLDate.h
//  7digital
//
//  Created by John McKerrell on 31/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (parseXMLDate)

+ (NSDate*)parseXMLDate:(NSString*)releaseDateStr;

@end
