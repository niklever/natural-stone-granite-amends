//
//  UIImage+DMExtensions.h
//  NaturalStone
//
//  Created by John McKerrell on 07/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (DMExtensions)

+ (UIImage*)colourSwatchImageForRef:(NSString*)ref;

@end
