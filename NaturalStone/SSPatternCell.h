//
//  SSPatternCell.h
//  NaturalStone
//
//  Created by Nik Lever on 29/04/2014.
//
//

#import <UIKit/UIKit.h>

@interface SSPatternCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *pattern_img;
@property (weak, nonatomic) IBOutlet UILabel *pattern_lbl;
@property (weak, nonatomic) IBOutlet UIImageView *arrow_img;

@end
