//
//  SSRenderVC.m
//  NaturalStone
//
//  Created by Nik Lever on 30/04/2014.
//
//

#import "SSRenderVC.h"
#import "SSMenuVC.h"
#import "SSPatternBCell.h"
#import "SSColourBCell.h"
#import "SSColourCCell.h"
#import "SSProportionsBVC.h"
#import <MediaPlayer/MediaPlayer.h>


enum{ _SS_TOOLBAR_MAIN, _SS_TOOLBAR_PATTERN, _SS_TOOLBAR_PROPORTIONS, _SS_TOOLBAR_MIX, _SS_TOOLBAR_COLOURS, _SS_TOOLBAR_NONE };

@interface SSRenderVC (){
    int mode;
    int premix_colours[6][6];
    int patternIdx;
    int mixIdx;
    int colours[6];
    bool selected_colours[6];
    int renderIdx;
    bool modifiedColours;
    bool project_saved;
    MPMoviePlayerController *player;
}

@end

@implementation SSRenderVC
@synthesize menu_vc;
@synthesize data;
@synthesize delegate;
@synthesize menu_btn;
@synthesize btns_cv;
@synthesize render_img;
@synthesize editbar_vw;
@synthesize patternbar_vw;
@synthesize proportionsbar_vw;
@synthesize title_lbl;
@synthesize play_btn;
@synthesize action_btn;
@synthesize overlay_img;
@synthesize btns_layout;
@synthesize proportions_vc;
@synthesize actions_vw;
@synthesize project_pnl;
@synthesize email_pnl;
@synthesize render_wv;
@synthesize activity_vw;
@synthesize projectLocation_txt;
@synthesize projectName_txt;
@synthesize project;
@synthesize customersEmail_txt;
@synthesize marshallsEmail_txt;
@synthesize message_txt;
@synthesize message_lbl;
@synthesize quote_swt;
@synthesize area_txt;
@synthesize area_lbl;
@synthesize projectInfo;
@synthesize addproject_btn;
@synthesize email_btn;
@synthesize bg_vw;
@synthesize addtolibrary_btn;
@synthesize project_pnl_title_lbl;
@synthesize toolbar_btn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        [self initVariables];
        project_saved = NO;
    }
    return self;
}

- (id)initWithProject:(DMProject *)_project{
    self = [super initWithNibName:@"SSRenderVC" bundle:nil];
    if (self){
        [self initVariables];
        self.project = _project;
        project_saved = YES;
    }
    return self;
}

- (IBAction)toolbarBtnPressed:(id)sender {
    if (mode == _SS_TOOLBAR_NONE){
        [self showToolbarScreen];
    }else{
        [self showFullScreen];
    }
}

-(void)initVariables{
    // Custom initialization
    for(int i=0; i<6; i++){
        for(int j=0; j<6; j++) premix_colours[i][j] = 0;
    }
    
    premix_colours[0][0] = 50;
    premix_colours[0][1] = 25;
    premix_colours[0][3] = 25;
    premix_colours[1][0] = 50;
    premix_colours[1][1] = 25;
    premix_colours[1][2] = 25;
    premix_colours[2][1] = 30;
    premix_colours[2][2] = 60;
    premix_colours[2][4] = 10;
    premix_colours[3][1] = 45;
    premix_colours[3][3] = 45;
    premix_colours[3][4] = 10;
    premix_colours[4][1] = 34;
    premix_colours[4][3] = 50;
    premix_colours[4][5] = 16;
    premix_colours[5][3] = 11;
    premix_colours[5][4] = 24;
    premix_colours[5][5] = 65;
    
    renderIdx = 1;
    
    mode = _SS_TOOLBAR_MAIN;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.btns_cv registerNib:[UINib nibWithNibName:@"SSPatternBCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"PatternBCell"];
    [self.btns_cv registerNib:[UINib nibWithNibName:@"SSColourBCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"ColourBCell"];
    [self.btns_cv registerNib:[UINib nibWithNibName:@"SSColourCCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"ColourCCell"];
    [self addSwipeEvent:self.view];
    
    self.proportions_vc = [[SSProportionsBVC alloc] initWithNibName:@"SSProportionsBVC" bundle:nil];
    [self.proportionsbar_vw addSubview:self.proportions_vc.view];
    
    project_pnl.hidden = YES;
    email_pnl.hidden = YES;
    actions_vw.hidden = YES;
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    NSString *moviePath = [[NSBundle mainBundle] pathForResource:@"stonespar" ofType:@"mp4"];
    NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
    player = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    player.view.frame = CGRectMake((1024-800)/2, (768-450)/2, 800, 450);
    player.view.hidden = YES;
    [self.view addSubview:player.view];
    
    if (project == nil){
        [self createProject];
        kDMNavigationControllerRef.currentProject = project;
        patternIdx = [[data objectForKey:@"pattern"] intValue];
        mixIdx = [[data objectForKey:@"mix"] intValue];
        if (mixIdx<6){
            for (int i=0; i<6; i++) colours[i] = premix_colours[mixIdx][i];
        }else{
            NSArray *coloursArray = [data objectForKey:@"colours"];
            for(int i=0; i<[coloursArray count]; i++) colours[i] = [[coloursArray objectAtIndex:i] intValue];
        }
    }else{
        patternIdx = [[project.properties objectForKey:@"PatternId"] intValue];
        for (int i=0; i<6; i++) colours[i] = 0;
        for(NSDictionary *col in project.colours){
            NSString *str = [col objectForKey:@"Reference"];
            int idx = [[str substringFromIndex:3] intValue] - 1;
            colours[idx] = [[col objectForKey:@"Percentage"] intValue];
        }
        projectName_txt.text = [project.properties objectForKey:@"Title"];
        projectLocation_txt.text = [project.properties objectForKey:@"Location"];
        area_txt.text = [NSString stringWithFormat:@"%d",[[project.properties objectForKey:@"Area"] intValue]];
        mixIdx = 6;
    }
    
    NSLog(@"Data pattern:%d mix:%d, colours:%d, %d, %d, %d, %d, %d", patternIdx, mixIdx, colours[0], colours[1], colours[2], colours[3], colours[4], colours[5]);
    
    NSString *str = [[NSBundle mainBundle] pathForResource:@"www/pattern" ofType:@"html"];
    if (str!=nil) [self.render_wv loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:str]]];
    
    //toolbar_btn.hidden = YES;
    //self.proportionsbar_vw.hidden = YES;
}

-(void)viewDidAppear:(BOOL)animated{
    mode = _SS_TOOLBAR_MAIN;
    CGRect frame = editbar_vw.frame;
    frame.origin.y = 668;
    editbar_vw.frame = frame;
    frame.origin.y = 768;
    patternbar_vw.frame = frame;
    proportionsbar_vw.frame = frame;
    if (project_saved){
        [self updateMode];
    }else{
        email_btn.enabled = NO;
        email_btn.alpha = 0.5;
    }
}

-(void)updateMode{
    addproject_btn.titleLabel.text = @"Update in library";
    project_pnl_title_lbl.text = @"Update project in library";
    addtolibrary_btn.titleLabel.text = @"Update";
}

-(void)createProject{
    DMProject *proj = [[DMProject alloc] init];
    self.project = proj;
    [self.project.properties setObject:@"Stonespar" forKey:kDMProjectTypeName];
    [self.project.properties setObject:[NSNumber numberWithInt:65] forKey:kDMProjectPropertyThickness];
    [self.project.properties setObject:[NSNumber numberWithInt:10] forKey:kDMProjectPropertyArea];
}

-(void)updateProject{
    if (self.project == nil) [self createProject];
    
    UIGraphicsBeginImageContext(self.render_wv.bounds.size);
    [self.render_wv.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.project setProjectImage:viewImage];
    
    [self.project.properties setObject:projectName_txt.text forKey:kDMProjectPropertyTitle];
    [self.project.properties setObject:projectLocation_txt.text forKey:kDMProjectPropertyLocation];
    
    [self.project.properties setObject:[NSNumber numberWithInt:patternIdx] forKey:@"PatternId"];
    
    NSString *str = area_txt.text;
    double area = [str floatValue];
    if (area>0){
        [self.project.properties setObject:[NSNumber numberWithFloat:area] forKey:kDMProjectPropertyArea];
    }
    
    NSMutableArray *cols = [[NSMutableArray alloc] init];
    
    for(int i=0; i<6; i++){
        if (colours[i]>0){
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setObject:[NSString stringWithFormat:@"col%d", (i+1)] forKey:@"reference"];
            [dict setObject:[NSNumber numberWithInt:colours[i]] forKey:@"percentage"];
            [cols addObject:dict];
        }
    }
    
    self.project.colours = cols;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changePatternPressed:(id)sender {
    mode = _SS_TOOLBAR_PATTERN;
    title_lbl.text = @"Choose a pattern";
    [btns_cv reloadData];
    [btns_layout invalidateLayout];
    CGRect frame = btns_cv.frame;
    frame.origin.x = 0;
    btns_cv.frame = frame;
    
    CGRect framea = patternbar_vw.frame;
    framea.origin.y = 668;
    CGRect frameb = editbar_vw.frame;
    frameb.origin.y = 768;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    patternbar_vw.frame = framea;
    editbar_vw.frame = frameb;
    [UIView commitAnimations];
}

- (IBAction)editProportionsPressed:(id)sender {
    mode = _SS_TOOLBAR_PROPORTIONS;
    title_lbl.text = @"Edit proportions";
    
    NSArray *cols = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:colours[0]],
                     [NSNumber numberWithInt:colours[1]],
                     [NSNumber numberWithInt:colours[2]],
                     [NSNumber numberWithInt:colours[3]],
                     [NSNumber numberWithInt:colours[4]],
                     [NSNumber numberWithInt:colours[5]], nil];
    [proportions_vc setColours:cols];
    //proportionsbar_vw.hidden = NO;
    
    CGRect framea = proportionsbar_vw.frame;
    framea.origin.y = 668;
    CGRect frameb = editbar_vw.frame;
    frameb.origin.y = 768;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    proportionsbar_vw.frame = framea;
    editbar_vw.frame = frameb;
    patternbar_vw.frame = frameb;
    [UIView commitAnimations];
}

- (IBAction)changeColoursPressed:(id)sender {
    mode = _SS_TOOLBAR_MIX;
    title_lbl.text = @"Change colours";
    [btns_cv reloadData];
    [btns_layout invalidateLayout];
    CGRect frame = btns_cv.frame;
    frame.origin.x = 30;
    btns_cv.frame = frame;
    
    CGRect framea = patternbar_vw.frame;
    framea.origin.y = 668;
    CGRect frameb = editbar_vw.frame;
    frameb.origin.y = 768;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    patternbar_vw.frame = framea;
    editbar_vw.frame = frameb;
    [UIView commitAnimations];
}

- (IBAction)menuPressed:(id)sender {
    [menu_vc showMenu:menu_btn];
}

- (IBAction)playPressed:(id)sender {
    //[self showFullScreen];
    player.view.hidden = NO;
    [player play];
}

- (IBAction)actionsPressed:(id)sender {
    if (project_saved) addtolibrary_btn.titleLabel.text = @"Update";
    actions_vw.hidden = NO;
}

- (IBAction)proportionsDonePressed:(id)sender {
    //if (proportions_vc.modified){
        NSArray *cols = [proportions_vc getColours];
        for(int i=0; i<[cols count]; i++) colours[i] = [[cols objectAtIndex:i] intValue];
        [self updatePattern];
        proportions_vc.modified = NO;
    //}
    [self showFullScreen];
    /*CGRect frame = proportionsbar_vw.frame;
    frame.origin.y = 768;
    [UIView animateWithDuration: 0.5
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                        proportionsbar_vw.frame = frame;
                     }
                     completion: ^(BOOL finished) {
                     }];*/

}

- (IBAction)addProjectPressed:(id)sender {
    NSLog(@"Add project to library");
    project_pnl.hidden = NO;
}

- (IBAction)sendEmailPressed:(id)sender {
    NSLog(@"Send email");
    email_pnl.hidden = NO;
}

- (IBAction)cancelAddProjectPressed:(id)sender {
    project_pnl.hidden = YES;
    actions_vw.hidden = YES;
    
    [self hideKeyboard];
}

- (IBAction)addToLibraryPressed:(id)sender {
    if ([projectName_txt.text isEqualToString:@""]){
        [self showMessage:@"Please choose a name for this project"];
    }else if([projectLocation_txt.text isEqualToString:@""]){
        [self showMessage:@"Please choose a location for this project"];
    }else if([area_txt.text isEqualToString:@""]){
        [self showMessage:@"Please choose an area for this project"];
    }else{
        project_pnl.hidden = YES;
        actions_vw.hidden = YES;
        
        //addproject_btn.enabled = NO;
        //addproject_btn.alpha = 0.5;
        email_btn.enabled = YES;
        email_btn.alpha = 1.0;
        
        [self updateProject];
        [kDMNavigationControllerRef saveStonesparProject:project];
        
        [self hideKeyboard];
        
        [self updateMode];
    }
}

- (void)showMessage:(NSString *)str {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                      message:str
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    
    [message show];
}

- (IBAction)sendEmailBPressed:(id)sender {
    email_pnl.hidden = YES;
    actions_vw.hidden = YES;
    [kDMNavigationControllerRef sendStonesparEmail:self.project
                                              from:marshallsEmail_txt.text
                                                to:customersEmail_txt.text
                                           message:message_txt.text
                                             quote:quote_swt.on];
    [self hideKeyboard];
}

- (IBAction)closeEmailPressed:(id)sender {
    email_pnl.hidden = YES;
    actions_vw.hidden = YES;
    [self hideKeyboard];
}

- (IBAction)quoteChanged:(id)sender {
    area_txt.hidden = area_lbl.hidden = !quote_swt.on;
}

-(void)updatePattern{
    //renderIdx++;
    //if (renderIdx>2) renderIdx = 1;
    //render_img.image = [UIImage imageNamed:[NSString stringWithFormat:@"render%d", renderIdx]];
    [self updatePreview];
}

-(void)showChooseColours{
    mode = _SS_TOOLBAR_COLOURS;
    title_lbl.text = @"Select colours";
    for(int i=0; i<6; i++){
        selected_colours[i] = (colours[i]>0);
    }
    modifiedColours = false;
    CGRect frame = btns_cv.frame;
    frame.origin.x = 100;
    btns_cv.frame = frame;
    [btns_cv reloadData];
    [btns_layout invalidateLayout];
}

-(void)showFullScreen{
    mode = _SS_TOOLBAR_NONE;
    CGRect frame = editbar_vw.frame;
    frame.origin.y = 768;
    menu_btn.hidden = YES;
    title_lbl.hidden = YES;
    play_btn.hidden = YES;
    action_btn.hidden = YES;
    overlay_img.hidden = YES;
    actions_vw.hidden = YES;
    //toolbar_btn.hidden = NO;
    [self.view.layer removeAllAnimations];
    [UIView animateWithDuration: 0.5
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         editbar_vw.frame = frame;
                         patternbar_vw.frame = frame;
                         proportionsbar_vw.frame = frame;
                     }
                     completion: ^(BOOL finished) {
                     }];
}

-(void)showToolbarScreen{
    mode = _SS_TOOLBAR_MAIN;
    title_lbl.text = @"Stonespar";
    CGRect frame = editbar_vw.frame;
    frame.origin.y = 668;
    menu_btn.hidden = NO;
    title_lbl.hidden = NO;
    play_btn.hidden = NO;
    action_btn.hidden = NO;
    overlay_img.hidden = NO;
    //toolbar_btn.hidden = YES;
    [self.view.layer removeAllAnimations];
    [UIView animateWithDuration: 0.5
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         editbar_vw.frame = frame;
                    }
                     completion: ^(BOOL finished) {
                     }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    // _data is a class member variable that contains one array per section.
    return 1;
}

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    //NSArray* sectionArray = [_data objectAtIndex:section];
    int count = 0;
    switch(mode){
        case _SS_TOOLBAR_MIX:
            count = 7;
            break;
        case _SS_TOOLBAR_COLOURS:
            count = 7;
            break;
        case _SS_TOOLBAR_PATTERN:
            count = 18;
            break;
    }
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SSPatternBCell *cella;
    SSColourBCell *cellm;
    SSColourCCell *cellc;
    NSArray *labels;
    int index;
    NSString *label;
    NSString *imageName;
    
    switch(mode){
        case _SS_TOOLBAR_PATTERN:
            cella = [collectionView dequeueReusableCellWithReuseIdentifier:@"PatternBCell"
                                                              forIndexPath:indexPath];
            
            labels = [[NSArray alloc] initWithObjects:@"Stretcher Bond", @"Stitch Bond", @"Elongated Stitch Bond", @"Third Bond - Left Hand", @"Third Bond - Right Hand",
                      @"Vertical Herringbone", @"Horizontal Herringbone", @"Herringbone - Left Hand", @"Herringbone - Right Hand", @"Vertical Double Herringbone",
                      @"Horizontal Double Herringbone", @"Left Hand Double Herringbone", @"Right Hand Double Herringbone", @"Vertical Triple Herringbone", @"Horizontal Triple Herringbone",
                      @"Left Hand Triple Herringbone", @"Right Hand Tripe Herringbone", @"Erratic Herringbone", nil];
            index = indexPath.row;
            
            label = [labels objectAtIndex:index];
            if (indexPath.row==patternIdx){
                imageName = [NSString stringWithFormat:@"pattern%d.png", (index+1)];
            }else{
                imageName = [NSString stringWithFormat:@"patternb%d.png", (index+1)];
            }
            //NSLog(@"collectionView %@ %@", label, imageName);
            
            [[cella pattern_lbl] setText:label];
            [[cella pattern_img] setImage:[UIImage imageNamed:imageName]];
            cella.selected_img.hidden = (indexPath.row!=patternIdx);
            if (indexPath.row==patternIdx){
                cella.pattern_lbl.textColor = [UIColor colorWithRed:185/255.0 green:139/255.0 blue:91/255.0 alpha:1];
            }else{
                cella.pattern_lbl.textColor = [UIColor whiteColor];
            }
            return cella;
            break;
        case _SS_TOOLBAR_MIX:
            cellm = [collectionView dequeueReusableCellWithReuseIdentifier:@"ColourBCell"
                                                              forIndexPath:indexPath];
            
            labels = [[NSArray alloc] initWithObjects:@"Vasanta", @"Grishma", @"Sharad", @"Hemant", @"Dava", @"Nisha", @"Choose your own", nil];
            index = indexPath.row;
            
            label = [labels objectAtIndex:index];
            if (indexPath.row==6 && mixIdx==6){
                imageName = [NSString stringWithFormat:@"btn_colours%db.png", (index+1)];
            }else{
                imageName = [NSString stringWithFormat:@"btn_colours%d.png", (index+1)];
            }
            //NSLog(@"collectionView %@ %@", label, imageName);
            
            [[cellm colour_lbl] setText:label];
            [[cellm colour_img] setImage:[UIImage imageNamed:imageName]];
            cellm.selected_img.hidden = !colours[indexPath.row];
            cellm.selected_img.hidden = (indexPath.row!=mixIdx);
            if (indexPath.row==mixIdx){
                cellm.colour_lbl.textColor = [UIColor colorWithRed:185/255.0 green:139/255.0 blue:91/255.0 alpha:1];
            }else{
                cellm.colour_lbl.textColor = [UIColor whiteColor];
            }
            return cellm;
            break;

        case _SS_TOOLBAR_COLOURS:
            cellc = [collectionView dequeueReusableCellWithReuseIdentifier:@"ColourCCell"
                                                              forIndexPath:indexPath];
            
            labels = [[NSArray alloc] initWithObjects:@"Chaler Beige", @"Jayrum Light", @"Ranya Brown", @"Salva Grey", @"Aruva Red", @"Holkar Black", @"Done", nil];
            index = indexPath.row;
            
            label = [labels objectAtIndex:index];
            imageName = [NSString stringWithFormat:@"colourb%d.png", (index+1)];
            NSLog(@"collectionView %@ %@", label, imageName);
            
            [[cellc colour_lbl] setText:label];
            [[cellc colour_img] setImage:[UIImage imageNamed:imageName]];
            cellc.selected_img.hidden = (indexPath.row==6 || colours[indexPath.row]==0);
            return cellc;
            break;
    }
    
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SSPatternBCell *cellp;
    SSColourBCell *cellm;
    SSColourCCell *cellc;
    NSIndexPath *prevPath;
    
    switch(mode){
        case _SS_TOOLBAR_COLOURS:
            if (indexPath.row==6){
                if (modifiedColours){
                    int count = 0;
                    for(int i=0; i<6; i++){
                        prevPath = [NSIndexPath indexPathForRow:i inSection:0];
                        cellc = (SSColourCCell *)[collectionView cellForItemAtIndexPath:prevPath];
                        selected_colours[i] = !cellc.selected_img.hidden;
                        if (selected_colours[i]) count++;
                    }
                    int average = 100/count;
                    for(int i=0; i<6; i++){
                        colours[i] = (selected_colours[i]) ? average : 0;
                    }
                }
                [self editProportionsPressed:nil];
            }else{
                modifiedColours = true;
                cellc = (SSColourCCell *)[collectionView cellForItemAtIndexPath:indexPath];
                cellc.selected_img.hidden = !cellc.selected_img.hidden;
            }
            break;
        case _SS_TOOLBAR_MIX:
            if (mixIdx!=indexPath.row){
                prevPath = [NSIndexPath indexPathForRow:mixIdx inSection:0];
                cellm = (SSColourBCell *)[collectionView cellForItemAtIndexPath:prevPath];
                cellm.selected_img.hidden = YES;
                if (prevPath.row==6) cellm.colour_img.image = [UIImage imageNamed:@"btn_colours7.png"];
                cellm.colour_lbl.textColor = [UIColor whiteColor];
                cellm = (SSColourBCell *)[collectionView cellForItemAtIndexPath:indexPath];
                if (indexPath.row==6) cellm.colour_img.image = [UIImage imageNamed:@"btn_colours7b.png"];
                cellm.selected_img.hidden = NO;
                cellm.colour_lbl.textColor = [UIColor colorWithRed:185/255.0 green:139/255.0 blue:91/255.0 alpha:1];
                mixIdx = indexPath.row;
                if (indexPath.row==6){
                    [self showChooseColours];
                }else{
                    //Premix
                    for(int i=0; i<6; i++) colours[i] = premix_colours[indexPath.row][i];
                    //Show mix;
                    [self updatePattern];
                }
            }else if(mixIdx==6){
                [self showChooseColours];
            }
            break;
        case _SS_TOOLBAR_PATTERN:
            if (patternIdx != indexPath.row){
                prevPath = [NSIndexPath indexPathForRow:patternIdx inSection:0];
                cellp = (SSPatternBCell *)[collectionView cellForItemAtIndexPath:prevPath];
                cellp.selected_img.hidden = YES;
                cellp.pattern_img.image = [UIImage imageNamed:[NSString stringWithFormat:@"patternb%d.png", (prevPath.row+1)]];
                cellp.pattern_lbl.textColor = [UIColor whiteColor];
                cellp = (SSPatternBCell *)[collectionView cellForItemAtIndexPath:indexPath];
                cellp.pattern_img.image = [UIImage imageNamed:[NSString stringWithFormat:@"pattern%d.png", (indexPath.row+1)]];
                cellp.selected_img.hidden = NO;
                cellp.pattern_lbl.textColor = [UIColor colorWithRed:185/255.0 green:139/255.0 blue:91/255.0 alpha:1];
                patternIdx = indexPath.row;
                [self updatePattern];
            }
            break;
    }
    
    if (!((mode==_SS_TOOLBAR_MIX && indexPath.row==6) || mixIdx==6))[self showFullScreen];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    CGFloat flt = 10.0f;
    
    switch(mode){
        case _SS_TOOLBAR_MIX:
            flt = 40.0f;
            break;
        case _SS_TOOLBAR_COLOURS:
            flt = 0.0f;
            break;
        case _SS_TOOLBAR_PATTERN:
            flt = 10.0f;
            break;
    }
    
    return flt;

}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    CGFloat flt = 10.0f;
    
    switch(mode){
        case _SS_TOOLBAR_MIX:
            flt = 40.0f;
            break;
        case _SS_TOOLBAR_COLOURS:
            flt = 0.0f;
            break;
        case _SS_TOOLBAR_PATTERN:
            flt = 10.0f;
            break;
    }
    
    return flt;
}

-(void)addSwipeEvent:(UIView*)subView{
    UISwipeGestureRecognizer *upRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
    upRecognizer.direction=UISwipeGestureRecognizerDirectionUp;
    upRecognizer.numberOfTouchesRequired = 1;
    upRecognizer.delegate = self;
    [subView addGestureRecognizer:upRecognizer];
    UISwipeGestureRecognizer *downRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
    downRecognizer.direction=UISwipeGestureRecognizerDirectionDown;
    downRecognizer.numberOfTouchesRequired = 1;
    downRecognizer.delegate = self;
    [subView addGestureRecognizer:downRecognizer];
}

- (void) SwipeRecognizer:(UISwipeGestureRecognizer *)sender {
    CGRect framea = editbar_vw.frame;
    CGRect frameb = editbar_vw.frame;
    framea.origin.y = 668;
    frameb.origin.y = 768;
    
    if ( sender.direction== UISwipeGestureRecognizerDirectionUp ){
        //NSLog(@" *** SWIPE UP ***");
        if (actions_vw.hidden){
            if (mode == _SS_TOOLBAR_NONE) [self showToolbarScreen];
        }else{
            actions_vw.hidden = YES;
        }
    }
    if ( sender.direction == UISwipeGestureRecognizerDirectionDown ){
        //NSLog(@" *** SWIPE DOWN ***");
        if (!actions_vw.hidden){
            actions_vw.hidden = YES;
        }else if (mode == _SS_TOOLBAR_MAIN){
            [self showFullScreen];
        }else if (mode==_SS_TOOLBAR_NONE){
            [self showToolbarScreen];
        }else{
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            editbar_vw.frame = framea;
            switch(mode){
                case _SS_TOOLBAR_PATTERN:
                case _SS_TOOLBAR_COLOURS:
                case _SS_TOOLBAR_MIX:
                    patternbar_vw.frame = frameb;
                    break;
                case _SS_TOOLBAR_PROPORTIONS:
                    proportionsbar_vw.frame = frameb;
                    break;
            }
            [UIView commitAnimations];
            mode = _SS_TOOLBAR_MAIN;
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIView class]])
    {
        return YES;
    }
    return NO;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGRect framea = editbar_vw.frame;
    CGRect frameb = editbar_vw.frame;
    framea.origin.y = 668;
    frameb.origin.y = 768;

    UITouch *touch = [[event allTouches] anyObject];
    
    if (!player.view.hidden){
        if ([touch view] != player.view){
            [player pause];
            player.view.hidden = YES;
            //[self showToolbarScreen];
        }
    }else if (!actions_vw.hidden){
        if ([touch view] == bg_vw && project_pnl.hidden && email_pnl.hidden){
            actions_vw.hidden = YES;
        }
    }else if (mode == _SS_TOOLBAR_MAIN){
        [self showFullScreen];
    }else if (mode==_SS_TOOLBAR_NONE){
        [self showToolbarScreen];
    }else{
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        editbar_vw.frame = framea;
        switch(mode){
            case _SS_TOOLBAR_PATTERN:
            case _SS_TOOLBAR_COLOURS:
            case _SS_TOOLBAR_MIX:
                patternbar_vw.frame = frameb;
                break;
            case _SS_TOOLBAR_PROPORTIONS:
                proportionsbar_vw.frame = frameb;
                break;
        }
        [UIView commitAnimations];
        mode = _SS_TOOLBAR_MAIN;
    }
    
    NSArray *textFields = [[NSArray alloc] initWithObjects:projectLocation_txt, projectName_txt, area_txt, customersEmail_txt, marshallsEmail_txt, message_txt, nil];
    for(UITextField *textField in textFields){
        if ([textField isFirstResponder] && [touch view] != textField) [textField resignFirstResponder];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updatePreview) userInfo:nil repeats:NO];
}

-(void)updatePreview{
    [activity_vw startAnimating];
    activity_vw.hidden = NO;
    [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(callJavascript) userInfo:nil repeats:NO];
    self.project.saved = NO;
}

-(void)callJavascript{
    NSString *javaScript = [NSString stringWithFormat:@"updatePattern(%d, %d, %d, %d, %d, %d, %d)", (patternIdx+1), colours[0], colours[1], colours[2], colours[3], colours[4], colours[5]];
    NSLog(@"callJavascript %@", javaScript);
    [render_wv stringByEvaluatingJavaScriptFromString:javaScript];
    [activity_vw stopAnimating];
    activity_vw.hidden = YES;
}

-(void)hideKeyboard{
    NSArray *textFields = [[NSArray alloc] initWithObjects:projectLocation_txt, projectName_txt, area_txt, customersEmail_txt, marshallsEmail_txt, nil];
    for(UITextField *textField in textFields){
        if ([textField isFirstResponder]) [textField resignFirstResponder];
    }
    if ([message_txt isFirstResponder]) [message_txt resignFirstResponder];

}

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    CGRect frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    if (!email_pnl.hidden){
        frame = email_pnl.frame;
        frame.origin.y = -60;
        email_pnl.frame = frame;
    }
    if (!project_pnl.hidden){
        frame = project_pnl.frame;
        frame.origin.y = 70;
        project_pnl.frame = frame;
    }
    [UIView commitAnimations];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    CGRect frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    frame = email_pnl.frame;
    frame.origin.y = 122;
    email_pnl.frame = frame;
    frame = project_pnl.frame;
    frame.origin.y = 192;
    project_pnl.frame = frame;
    [UIView commitAnimations];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
}

- (void)textViewDidChange:(UITextView *)txtView
{
     self.message_lbl.hidden = ([txtView.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)txtView
{
    self.message_lbl.hidden = ([txtView.text length] > 0);
}

@end
