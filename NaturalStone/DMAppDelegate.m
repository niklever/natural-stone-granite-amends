//
//  DMAppDelegate.m
//  NaturalStone
//
//  Created by John McKerrell on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DMAppDelegate.h"

#import "DMNavigationController.h"
#import "DMCustomNavBar.h"
#import "DMImageCacher.h"
#import "SSHomeVC.h"
#import "SSMenuVC.h"
#import "SSMainVC.h"
#import "SSGalleryVC.h"

const NSString *kDefaultAPIBaseKey = @"APIBaseKey";
//const NSString *kDefaultAPIBase = @"http://www.marshalls.co.uk/naturalstoneapp/api/";
/* 
 test server
 const NSString *kDefaultAPIBase = @"http://newsite.marshalls-etest.co.uk/naturalstoneapp/api/";
*/

/* live server */
const NSString *kDefaultAPIBase = @"http://www.marshalls.co.uk/naturalstoneapp/api/";

const NSString *kDefaultAPIPassword = @"M4r5ha11s!";
const NSTimeInterval kDefaultBackgroundLongInactiveWarningTime = 7200;
const NSTimeInterval kDefaultBackgroundInactiveWarningTime = 600;
const NSTimeInterval kDefaultEnterBackgroundWarningTime = 2.0;

@implementation DMAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController; 

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //[TestFlight takeOff:@"95f8dc72501df0f188ae1dda876f41df_MTY2MTIzMjAxMi0xMi0xMyAxMTowMjozNC4xNDQ4MTY"];
    //[TestFlight takeOff:@"933dc7b0-1657-4be9-a6b0-e11501714f81"];
    //[TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
    // Initialize tracker. Replace with your tracking ID.
     
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults registerDefaults:@{kDefaultAPIBaseKey:kDefaultAPIBase}];
    [userDefaults setBool:YES forKey:@"WebKitStoreWebDataForBackup"];
    
    self.window = [[DMTapDetectingWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.viewController = [[DMNavigationController alloc] initWithHomeViewController];
    
    //self.viewController = [[UINavigationController alloc] initWithRootViewController:[[SSHomeVC alloc] initWithNibName:@"SSHomeVC" bundle:nil]];
    //[self.viewController setNavigationBarHidden:YES animated:YES];
    
    //ss_menu = [[SSMenuVC alloc] initWithNibName:@"SSMenuVC" bundle:nil];
    
    //[self.viewController.view addSubview:ss_menu.view];
    self.window.rootViewController = self.viewController;
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)changeVC:(NSString *)vcName{
    NSLog(@"changeVC to %@", vcName);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    if ([kDMNavigationControllerRef projectLibraryHasUnsyncedChanges]) {
        UILocalNotification *notif = [[UILocalNotification alloc] init];
        notif.fireDate = [NSDate dateWithTimeIntervalSinceNow:kDefaultEnterBackgroundWarningTime];
        notif.timeZone = [NSTimeZone defaultTimeZone];
        
        notif.alertBody = NSLocalizedString(@"ENTER_BACKGROUND_WARNING_BODY", @"");
        notif.alertAction = NSLocalizedString(@"ENTER_BACKGROUND_WARNING_ACTION", @"");
        notif.soundName = UILocalNotificationDefaultSoundName;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
         notif = nil;
        
        notif = [[UILocalNotification alloc] init];
        notif.fireDate = [NSDate dateWithTimeIntervalSinceNow:kDefaultBackgroundInactiveWarningTime];
        notif.timeZone = [NSTimeZone defaultTimeZone];
        
        notif.alertBody = NSLocalizedString(@"LOSE_BACKGROUND_WARNING_BODY", @"");
        notif.alertAction = NSLocalizedString(@"LOSE_BACKGROUND_WARNING_ACTION", @"");
        notif.soundName = UILocalNotificationDefaultSoundName;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
         notif = nil;

        notif = [[UILocalNotification alloc] init];
        notif.fireDate = [NSDate dateWithTimeIntervalSinceNow:kDefaultBackgroundLongInactiveWarningTime];
        notif.timeZone = [NSTimeZone defaultTimeZone];
        
        notif.alertBody = NSLocalizedString(@"LOSE_BACKGROUND_WARNING_BODY", @"");
        notif.alertAction = NSLocalizedString(@"LOSE_BACKGROUND_WARNING_ACTION", @"");
        notif.soundName = UILocalNotificationDefaultSoundName;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
         notif = nil;

        if (self.imageSyncObserver) {
            [[NSNotificationCenter defaultCenter] removeObserver:self.imageSyncObserver
                                                            name:kDMImageSyncCompletedNotification
                                                          object:nil];
        }
        [[NSNotificationCenter defaultCenter] addObserverForName:kDMImageSyncCompletedNotification
                                                          object:nil
                                                           queue:[NSOperationQueue mainQueue]
                                                      usingBlock:^(NSNotification *note) {
                                                          
                                                          if (![kDMNavigationControllerRef projectLibraryHasUnsyncedChanges]) {
                                                              [[UIApplication sharedApplication] cancelAllLocalNotifications];
                                                          }
                                                      }];
    }
    if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)]) { //Check if our iOS version supports multitasking I.E iOS 4
        if ([[UIDevice currentDevice] isMultitaskingSupported]) { //Check if device supports mulitasking
            UIApplication *application = [UIApplication sharedApplication]; //Get the shared application instance
            void (^backgroundBlock)(void) = nil;
            backgroundBlock = ^ {
                UIBackgroundTaskIdentifier backgroundTask = self.backgroundTask;
                self.backgroundTask = [application beginBackgroundTaskWithExpirationHandler:backgroundBlock];
                [application endBackgroundTask: backgroundTask]; //Tell the system that we are done with the tasks
                backgroundTask = UIBackgroundTaskInvalid; //Set the task to be invalid
                //System will be shutting down the app at any point in time now
                NSLog(@"\n\nBackground task finishing.\n\n");
            };
            self.backgroundTask = [application beginBackgroundTaskWithExpirationHandler:backgroundBlock];
            //Background tasks require you to use asyncrous tasks
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                //Perform your tasks that your application requires
                NSLog(@"\n\nRunning in the background!\n\n");
//                [application endBackgroundTask: self.backgroundTask]; //End the task so the system knows that you are done with what you need to perform
//                self.backgroundTask = UIBackgroundTaskInvalid; //Invalidate the background_task
            });
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    // Don't want to warn the user about running in the background when they no longer are
    [[UIApplication sharedApplication] cancelAllLocalNotifications];

    // Make sure we have a project library
    //[kDMNavigationControllerRef projectLibrary];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)applicationDidChangeStatusBarOrientation:(NSNotification *)notification
{
    // handling statusBar (iOS7)
    self.window.frame = [UIScreen mainScreen].applicationFrame;
}
@end
