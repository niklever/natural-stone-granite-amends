//
//  SSMenuViewCell.h
//  NaturalStone
//
//  Created by Nik Lever on 28/04/2014.
//
//

#import <UIKit/UIKit.h>

@interface SSMenuViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cell_lbl;
@property (weak, nonatomic) IBOutlet UIImageView *cell_img;
@property (weak, nonatomic) IBOutlet UIView *view;

-(void)setIcon:(UIImage*)icon andLabel:(NSString*)label;

@end
