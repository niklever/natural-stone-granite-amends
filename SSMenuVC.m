//
//  SSMenuVC.m
//  NaturalStone
//
//  Created by Nik Lever on 28/04/2014.
//
//
#import "DMAppDelegate.h"
#import "SSMenuVC.h"
#import "SSMenuViewCell.h"

@interface SSMenuVC ()

@end

@implementation SSMenuVC

@synthesize menu_vw;
@synthesize menu_btn;
@synthesize statusbar_bg;
@synthesize navController;
@synthesize dummy_menu_btn;
@synthesize hideAfterAnimation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    hidden = true;
    hideAfterAnimation = false;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addSwipeEvent:menu_vw ];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidAppear:(BOOL)animated{
    hidden = true;
    
    CGRect frame = menu_vw.frame;
    frame.origin.x = -frame.size.width;
    menu_vw.frame = frame;
    
    frame = menu_btn.frame;
    frame.origin.x -= menu_vw.frame.size.width;
    menu_btn.frame = frame;
    
    frame = statusbar_bg.frame;
    frame.origin.y = -frame.size.height;
    statusbar_bg.frame = frame;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SSMenuViewCell";
    
    SSMenuViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil){
        //NSLog(@"New Cell Made");
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SSMenuViewCell" owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[SSMenuViewCell class]])
            {
                cell = (SSMenuViewCell *)currentObject;
                break;
            }
        }
    }
    
    NSArray *labels = [[NSArray alloc] initWithObjects:@"Photo Gallery", @"Stonespar", @"Granite", @"Project Library", nil];
    [[cell cell_lbl] setText:[labels objectAtIndex:indexPath.row]];
    [[cell cell_img] setImage:[UIImage imageNamed:[NSString stringWithFormat:@"menu%d.png", (indexPath.row+1)]]];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:169.0/255.0 green:119.0/255.0 blue:68.0/255.0 alpha:1];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Row pressed!! %d", indexPath.row);
    
    switch(indexPath.row){
        case 0://Photo Gallery
            [navController showPhotoGallery];
            break;
        case 1://Stonespar
            [navController showStonespar];
            break;
        case 2://Granite
            [navController showGranite];
            break;
        case 3://Project Library
            [navController showProjectLibrary];
            break;
    }

    [self menuPressed:nil];
}

-(void)showMenu:(UIButton *)btn{
    self.view.hidden = NO;
    self.dummy_menu_btn = btn;
    self.dummy_menu_btn.hidden = YES;
    //hidden = false;
    CGRect frame = menu_vw.frame;
    CGRect frameb = statusbar_bg.frame;
    frame.origin.x = -frame.size.width;
    frameb.origin.y = -frame.size.height;
    menu_vw.frame = frame;
    statusbar_bg.frame = frameb;
    hideAfterAnimation  =   NO;
    [self menuPressed:nil];
}

-(void)closeMenu{
    self.view.hidden = NO;
    hidden = true;
    [self menuPressed:nil];
}

- (IBAction)menuPressed:(id)sender {
    hidden = !hidden;
    menu_btn.hidden = YES;
    
    CGRect frame = menu_vw.frame;
    CGRect frameb = statusbar_bg.frame;
    if (hidden){
        frame.origin.x = -frame.size.width;
        frameb.origin.y = -frame.size.height;
    }else{
        frame.origin.x = 0;
        frameb.origin.y = 0;
    }
    
    [UIView animateWithDuration: 0.4
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.menu_vw.frame = frame;
                         self.statusbar_bg.frame = frameb;
                     }
                     completion: ^(BOOL finished) {
                         CGRect frame = menu_btn.frame;
                         if (hidden){
                             frame.origin.x = 8;
                             [menu_btn setImage:[UIImage imageNamed:@"menu_btn"] forState:UIControlStateNormal];
                             if (hideAfterAnimation) self.view.hidden = YES;
                             self.view.hidden = YES;
                             if (self.dummy_menu_btn!=nil) self.dummy_menu_btn.hidden = NO;
                             menu_btn.hidden = NO;
                             menu_btn.frame = CGRectMake(8, menu_btn.frame.origin.y, menu_btn.frame.size.width, menu_btn.frame.size.height);
                         }else{
                             frame.origin.x = 258;
                             [menu_btn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
                             menu_btn.frame = CGRectMake(258, menu_btn.frame.origin.y, menu_btn.frame.size.width, menu_btn.frame.size.height);
                         }
                         menu_btn.hidden = NO;
                         
                     }];
    
}

-(void)addSwipeEvent:(UIView*)subView{
    UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
    leftRecognizer.direction=UISwipeGestureRecognizerDirectionLeft;
    leftRecognizer.numberOfTouchesRequired = 1;
    leftRecognizer.delegate = self;
    [subView addGestureRecognizer:leftRecognizer];
}

- (void) SwipeRecognizer:(UISwipeGestureRecognizer *)sender {
    if ( sender.direction == UISwipeGestureRecognizerDirectionLeft ){
        //NSLog(@" *** SWIPE LEFT ***");
        [self menuPressed:sender];
        
    }
    if ( sender.direction == UISwipeGestureRecognizerDirectionRight ){
        NSLog(@" *** SWIPE RIGHT ***");
        
    }
    if ( sender.direction== UISwipeGestureRecognizerDirectionUp ){
        NSLog(@" *** SWIPE UP ***");
        
    }
    if ( sender.direction == UISwipeGestureRecognizerDirectionDown ){
        NSLog(@" *** SWIPE DOWN ***");
        
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIView class]])
    {
        return YES;
    }
    return NO;
}
@end
