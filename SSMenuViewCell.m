//
//  SSMenuViewCell.m
//  NaturalStone
//
//  Created by Nik Lever on 28/04/2014.
//
//

#import "SSMenuViewCell.h"

@implementation SSMenuViewCell

@synthesize cell_lbl;
@synthesize cell_img;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
