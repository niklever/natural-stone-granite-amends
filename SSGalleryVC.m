//
//  SSGalleryVC.m
//  NaturalStone
//
//  Created by Nik Lever on 28/04/2014.
//
//

#import "SSGalleryVC.h"
#import "SSMenuVC.h"

@interface SSGalleryVC (){
    int count;
    int current;
    bool oneVisible;
}
@end

@implementation SSGalleryVC
@synthesize gallery1_img;
@synthesize gallery2_img;
@synthesize menu_btn;
@synthesize menu_vc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    current = 1;
    count = 11;
    oneVisible = true;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    gallery1_img.image = [UIImage imageNamed:[NSString stringWithFormat:@"gallery%d.jpg", current]];
    menu_btn.hidden = YES;
    
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(showMenuBtn:) userInfo:nil repeats:NO];
    [timer fire];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    CGRect frame = gallery2_img.frame;
    frame.origin.x = 1024;
    gallery2_img.frame = frame;
    
    [self addSwipeEvent:self.view];
}

-(void)showMenuBtn:(id)sender{
    menu_btn.hidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addSwipeEvent:(UIView*)subView{
    UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
    leftRecognizer.direction=UISwipeGestureRecognizerDirectionLeft;
    leftRecognizer.numberOfTouchesRequired = 1;
    leftRecognizer.delegate = self;
    [subView addGestureRecognizer:leftRecognizer];
    
    UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
    rightRecognizer.direction=UISwipeGestureRecognizerDirectionRight;
    rightRecognizer.numberOfTouchesRequired = 1;
    rightRecognizer.delegate = self;
    [subView addGestureRecognizer:rightRecognizer];
}

- (void) SwipeRecognizer:(UISwipeGestureRecognizer *)sender {
    bool swipe = false;
    CGRect frameOut;
    CGRect frameIn;
    CGRect frameInStart;
    
    frameIn = gallery1_img.frame;
    frameIn.origin.x = 0;
    frameInStart = frameOut = frameIn;
    
    if ( sender.direction == UISwipeGestureRecognizerDirectionRight ){
        NSLog(@"Swipe left %d", current);
        if (current>1){
            current--;
            oneVisible = !oneVisible;
            frameOut.origin.x = 1024;
            frameInStart.origin.x = -1024;
            swipe = true;
        }else{
            [menu_vc showMenu:menu_btn];
        }
    }
    
    if ( sender.direction == UISwipeGestureRecognizerDirectionLeft ){
        NSLog(@"Swipe right %d", current);
        if (current<count){
            current++;
            oneVisible = !oneVisible;
            frameOut.origin.x = -1024;
            frameInStart.origin.x = 1024;
            swipe = true;
        }
    }
    
    if (swipe){
        if (oneVisible){
            gallery1_img.image = [UIImage imageNamed:[NSString stringWithFormat:@"gallery%d.jpg", current]];
            gallery1_img.frame = frameInStart;
        }else{
            gallery2_img.image = [UIImage imageNamed:[NSString stringWithFormat:@"gallery%d.jpg", current]];
            gallery2_img.frame = frameInStart;
        }
        
        [UIView animateWithDuration: 0.5
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             if (oneVisible){
                                 gallery1_img.frame = frameIn;
                                 gallery2_img.frame = frameOut;
                             }else{
                                 gallery1_img.frame = frameOut;
                                 gallery2_img.frame = frameIn;
                             }
                         }
                         completion: ^(BOOL finished) {
                         }];

    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIView class]])
    {
        return YES;
    }
    return NO;
}

- (IBAction)menuPressed:(id)sender {
    //menu_btn.hidden = YES;
    [menu_vc showMenu:menu_btn];
}
@end
