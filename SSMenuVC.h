//
//  SSMenuVC.h
//  NaturalStone
//
//  Created by Nik Lever on 28/04/2014.
//
//

#import <UIKit/UIKit.h>

@interface SSMenuVC : UIViewController<UIGestureRecognizerDelegate>{
    bool hidden;
}

- (IBAction)menuPressed:(id)sender;
-(void)showMenu:(UIButton *)btn;
-(void)closeMenu;

@property (weak, nonatomic) IBOutlet UITableView *menu_vw;
@property (weak, nonatomic) IBOutlet UIButton *menu_btn;
@property (weak, nonatomic) IBOutlet UILabel *statusbar_bg;
@property (strong, nonatomic) IBOutlet DMNavigationController *navController;
@property (assign) bool hideAfterAnimation;
@property (strong, nonatomic) UIButton *dummy_menu_btn;

@end
